﻿using Lanchonha.Banco.BD.Cliente;
using Lanchonha.Banco.BD.Funcionarios;
using Lanchonha.Telas.Pedido;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Lanchonha.Telas.Cliente
{
    public partial class frmConsultarCliente : Form
    {
        public frmConsultarCliente()
        {
            InitializeComponent();
            CarregarPermissoes();
        }

        private void label5_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void label13_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void CarregarDados()
        {
            Business_Cliente business = new Business_Cliente();
            dgvcliente.AutoGenerateColumns = false;
            List<DTO_Cliente> dados = business.ConsultarporNome(txtnome.Text);
            dgvcliente.DataSource = dados;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            CarregarDados();
        }

        private void dgvcliente_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if(e.ColumnIndex==3)
            {
                DTO_Cliente dto = dgvcliente.CurrentRow.DataBoundItem as DTO_Cliente;
                int pk = dto.ID;
                int fk = dto.FKIDFuncionario;
                frmAlterarCliente alterarCliente = new frmAlterarCliente();
                alterarCliente.LoadScrenn(pk, fk);
                alterarCliente.Show();
                this.Close();
            }

            if (e.ColumnIndex==4)
            {
                DTO_Cliente dto = dgvcliente.CurrentRow.DataBoundItem as DTO_Cliente;
                frmCadastrarPedidos cadastrarPedidos = new frmCadastrarPedidos();
                int FKcliente;
                string nomecliente, cpfcliente, telefonecliente;
                FKcliente = dto.ID;
                nomecliente = dto.NomeCompleto;
                cpfcliente = dto.CPF;
                telefonecliente = dto.Celular;
                cadastrarPedidos.LoadScreen(nomecliente, cpfcliente, telefonecliente, FKcliente);
                cadastrarPedidos.Show();
                this.Close();
            }
        }

        private void CarregarPermissoes()
        {
            if (UserSession.UsuarioLogado.permissao_controle_cliente == false)
            {

                dgvcliente.Columns[3].Visible = false;
                //dgvcliente.Columns[4].Visible = false;
            }

        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            frmMenu menu = new frmMenu();
            menu.Show();
            this.Close();
        }
    }
}
