﻿using Lanchonha.Banco.BD.Cliente;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Lanchonha.Telas.Cliente
{
    public partial class frmAlterarCliente : Form
    {
        public frmAlterarCliente()
        {
            InitializeComponent();
        }

        private void label14_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void label13_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }
        int fkfuncionario;
        public void LoadScrenn(int pk, int fk)
        {
            lblid.Text = pk.ToString();
            fkfuncionario = fk;
        }

        private void AlterarDados()
        {
            Business_Cliente business = new Business_Cliente();
            DTO_Cliente dto = new DTO_Cliente();
            dto.NomeCompleto = txtnome.Text.Trim();
            dto.CPF = mktcepf.Text.Trim();
            dto.Celular = txttelefone.Text.Trim();
            dto.ID = int.Parse(lblid.Text);
            dto.FKIDFuncionario = fkfuncionario;
            business.Alterar(dto);
        }

        private void CarregarDados()
        {
            Business_Cliente cliente = new Business_Cliente();
            List<DTO_Cliente> lista = cliente.ConsultarporID(int.Parse(lblid.Text));
            DTO_Cliente dto = lista[0];
            txtnome.Text = dto.NomeCompleto;
            txttelefone.Text = dto.Celular;
            mktcepf.Text = dto.CPF;
           
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            { 
            AlterarDados();
            MessageBox.Show("Dados alterados com sucesso", "Alterado", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message, "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            CarregarDados();
        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            frmMenu menu = new frmMenu();
            menu.Show();
            this.Close();
        }
    }
}
