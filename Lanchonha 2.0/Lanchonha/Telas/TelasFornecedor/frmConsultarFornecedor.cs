﻿using Lanchonha.Banco.BD.Fornecedor;
using Lanchonha.Banco.BD.Funcionarios;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Lanchonha
{
    public partial class frmConsultarFornecedor : Form
    {
        public frmConsultarFornecedor()
        {
            InitializeComponent();
            CarregarPermissoes();
        }

        private void label5_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void label13_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void BuscarDados()
        {
            Business_Fornecedores business = new Business_Fornecedores();
            dgvfornecedor.AutoGenerateColumns = false;
            List<DTO_Fornecedores> dto = business.ConsultarporNome(txtfornecedor.Text);
            dgvfornecedor.DataSource = dto;
        }

        private void frmConsultarFornecedor_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            BuscarDados();
        }

        private void txtid_TextChanged(object sender, EventArgs e)
        {

        }

        private void dgvfornecedor_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if(e.ColumnIndex==3)
            {
                DTO_Fornecedores dto = dgvfornecedor.CurrentRow.DataBoundItem as DTO_Fornecedores;
                int pk = dto.IdFornecedor;
                frmAlterarDadosdeFornecedor alterarDadosdeFornecedor = new frmAlterarDadosdeFornecedor();
                alterarDadosdeFornecedor.LoadScreem(pk);
                alterarDadosdeFornecedor.Show();
                this.Close ();
            }

            if(e.ColumnIndex==4)
            {
                DTO_Fornecedores fornecedores = dgvfornecedor.CurrentRow.DataBoundItem as DTO_Fornecedores;
                int pk = fornecedores.IdFornecedor;
                Business_Fornecedores business = new Business_Fornecedores();
                business.Remover(pk);
                BuscarDados();
            }


        }

        private void CarregarPermissoes()
        {
            if (UserSession.UsuarioLogado.permissao_fornecedor_alterar == false)
            {

                dgvfornecedor.Columns[3].Visible = false;
                dgvfornecedor.Columns[4].Visible = false;
            }

        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            frmMenu menu = new frmMenu();
            menu.Show();
            this.Close();
        }
    }
}
