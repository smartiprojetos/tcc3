﻿using Lanchonha.Banco.BD.Fornecedor;
using Lanchonha.Banco.BD.Funcionarios;
using Lanchonha.Telas.TelasFornecedor;
using Lanchonha.Utilitarios;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Lanchonha
{
    public partial class frmcadastrofornecedor : Form
    {
        public frmcadastrofornecedor()
        {
            InitializeComponent();
        }

        private void CEPDADOS()
        {
            try { 
            APICorreios correios = new APICorreios();
            CorreiosDTO dto = correios.CEPDADOS(mktcep.Text);
            txtuf.Text = dto.UF;
            txtcidade.Text = dto.Cidade;
            txtbairro.Text = dto.Bairro;
            txtrua.Text = dto.Rua;
            }
            catch
            {
                MessageBox.Show("Por favor insira um CEP válido.", "Back's", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void SalvarDados()
        {
            int pk = UserSession.UsuarioLogado.Id_funcionario;
            Business_Fornecedores business = new Business_Fornecedores();
            DTO_Fornecedores dto = new DTO_Fornecedores();
            dto.NomeFornecedor = txtnome.Text.Trim();
            dto.CNPJ = mktcnpj.Text.Trim();
            dto.Rua = txtrua.Text.Trim();
            dto.Numerofornecedor = txtnumero.Text.Trim();
            dto.Cidade = txtcidade.Text.Trim();
            dto.UF = txtuf.Text.Trim();
            dto.Complemento = txtcomplemento.Text.Trim();
            dto.CEP = mktcep.Text.Trim();
            dto.Telefone = mkttelefone.Text.Trim();
            dto.Email = txtemail.Text.Trim();
            dto.Observacao = txtobservacoes.Text.Trim();
            dto.PrecoUnitario = decimal.Parse(txtpreçounitario.Text);
            dto.Bairro = txtbairro.Text.Trim();
            dto.DataCadastro = dtpdatacadastro.Value;
            dto.FKFuncionario = pk;
            business.Salvar(dto);
        }

        private void label14_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void label13_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
            
            SalvarDados();
            MessageBox.Show("Dados salvos com sucesso", "Salvo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message, "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void txtcidade_TextChanged(object sender, EventArgs e)
        {

        }

        private void button3_Click(object sender, EventArgs e)
        {
            CEPDADOS();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            frmProdutoFornecedor produtoFornecedor = new frmProdutoFornecedor();
            produtoFornecedor.Show();
            this.Close();
        }

        private void button2_Click(object sender, EventArgs e)
        {

        }

        private void txtpreçounitario_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && (e.KeyChar != ','))
            {
                e.Handled = true;
                MessageBox.Show("este campo aceita somente numero e virgula", "Back's", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            if ((e.KeyChar == ',') && ((sender as TextBox).Text.IndexOf('.') > -1))
            {
                e.Handled = true;
                MessageBox.Show("este campo aceita somente uma virgula", "Back's", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void txtnumero_TextChanged(object sender, EventArgs e)
        {

        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            frmMenu menu = new frmMenu();
            menu.Show();
            this.Close();
        }

        private void label15_Click(object sender, EventArgs e)
        {

        }

        private void label5_Click(object sender, EventArgs e)
        {

        }

        private void frmcadastrofornecedor_Load(object sender, EventArgs e)
        {

        }
    }
}
