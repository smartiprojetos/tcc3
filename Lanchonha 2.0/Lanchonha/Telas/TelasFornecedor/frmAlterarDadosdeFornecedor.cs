﻿using Lanchonha.Banco.BD.Fornecedor;
using Lanchonha.Banco.BD.Funcionarios;
using Lanchonha.Utilitarios;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Lanchonha
{
    public partial class frmAlterarDadosdeFornecedor : Form
    {
        public frmAlterarDadosdeFornecedor()
        {
            InitializeComponent();
        }
        int pk;

        private void label14_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void label13_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void label15_Click(object sender, EventArgs e)
        {

        }

        private void button4_Click(object sender, EventArgs e)
        {
            CarregarDados();
        }
        private void CEPDADOS()
        {
            try { 
            APICorreios correios = new APICorreios();
            CorreiosDTO dto = correios.CEPDADOS(mktcep.Text);
            txtuf.Text = dto.UF;
            txtcidade.Text = dto.Cidade;
            txtbairro.Text = dto.Bairro;
            txtrua.Text = dto.Rua;
            }
            catch
            {
                MessageBox.Show("Por favor insira um CEP válido.", "Back's", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void button5_Click(object sender, EventArgs e)
        {
            try
            { 
            CEPDADOS();
            }
            catch(Exception)
            {
                MessageBox.Show("Por Favor Insira o CEPPor favor insira um CEP válido.", "Back's", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void LoadScreem(int id)
        {
            lblid.Text = id.ToString();
        }

        private void CarregarDados()
        {
            Business_Fornecedores business = new Business_Fornecedores();
            List<DTO_Fornecedores> dtofornecedores = business.ConsultarporId(int.Parse(lblid.Text));
            DTO_Fornecedores fornecedores = dtofornecedores[0];
            txtnome.Text = fornecedores.NomeFornecedor;
            mktcnpj.Text = fornecedores.CNPJ;
            mktdatacadastro.Text = fornecedores.DataCadastro.ToString();
            mkttelefone.Text = fornecedores.Telefone;
            txtemail.Text = fornecedores.Email;
            txtprecounitario.Text = fornecedores.PrecoUnitario.ToString();
            txtcomplemento.Text = fornecedores.Complemento;
            txtuf.Text = fornecedores.UF;
            txtcidade.Text = fornecedores.Cidade;
            mktcep.Text = fornecedores.CEP;
            txtbairro.Text = fornecedores.Bairro;
            txtrua.Text = fornecedores.Rua;
            txtnumero.Text = fornecedores.Numerofornecedor; ;
            txtobservacoes.Text = fornecedores.Observacao;
            pk = fornecedores.FKFuncionario;
        }

        private void SalvarDados()
        {
            
            Business_Fornecedores fornecedores = new Business_Fornecedores();
            DTO_Fornecedores dto = new DTO_Fornecedores();
            dto.IdFornecedor = int.Parse(lblid.Text);
            dto.NomeFornecedor = txtnome.Text.Trim();
            dto.CNPJ = mktcnpj.Text.Trim();
            dto.Rua = txtrua.Text.Trim();
            dto.Numerofornecedor = txtnumero.Text.Trim();
            dto.Complemento = txtcomplemento.Text.Trim();
            dto.Bairro = txtbairro.Text.Trim();
            dto.Cidade = txtcidade.Text.Trim();
            dto.UF = txtuf.Text.Trim();
            dto.CEP = mktcep.Text.Trim();
            dto.Telefone = mkttelefone.Text.Trim();
            dto.Email = txtemail.Text.Trim();
            dto.PrecoUnitario = decimal.Parse(txtprecounitario.Text);
            dto.DataCadastro = DateTime.Parse(mktdatacadastro.Text);
            dto.Observacao = txtobservacoes.Text.Trim();
            dto.FKFuncionario = pk;
            fornecedores.Alterar(dto);
        }

        private void maskedTextBox1_MaskInputRejected(object sender, MaskInputRejectedEventArgs e)
        {

        }

        private void txtnumero_TextChanged(object sender, EventArgs e)
        {

        }

        private void frmAlterarDadosdeFornecedor_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {

                SalvarDados();
                MessageBox.Show("Dados alterados com sucesso", "Alterado", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch(Exception ex)
            {
                MessageBox.Show("Por Favor Insira os Dados correatamente", "Back's", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void txtprecounitario_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && (e.KeyChar != ','))
            {
                e.Handled = true;
                MessageBox.Show("este campo aceita somente numero e virgula", "Back's", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            if ((e.KeyChar == ',') && ((sender as TextBox).Text.IndexOf('.') > -1))
            {
                e.Handled = true;
                MessageBox.Show("este campo aceita somente uma virgula", "Back's", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void txtnumero_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && (e.KeyChar != ','))
            {
                e.Handled = true;
                MessageBox.Show("este campo aceita somente numero e virgula", "Back's", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            if ((e.KeyChar == ',') && ((sender as TextBox).Text.IndexOf('.') > -1))
            {
                e.Handled = true;
                MessageBox.Show("este campo aceita somente uma virgula", "Back's", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            frmMenu menu = new frmMenu();
            menu.Show();
            this.Close();
        }
    }
}
