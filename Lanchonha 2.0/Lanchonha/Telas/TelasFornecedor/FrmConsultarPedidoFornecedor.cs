﻿using Lanchonha.Banco.BD.Fornecedor;
using Lanchonha.Banco.BD.Fornecedor.PedidoFornecedor;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Lanchonha.Telas.TelasFornecedor
{
    public partial class FrmConsultarPedidoFornecedor : Form
    {
        public FrmConsultarPedidoFornecedor()
        {
            InitializeComponent();
            CarregarCombo();
        }

        private void label14_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void label13_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

       

        private void button1_Click(object sender, EventArgs e)
        {
            
        }

        private void CarregarCombo()
        {
            Business_Fornecedores fornecedores = new Business_Fornecedores();
            cbofornecedor.ValueMember = nameof(DTO_Fornecedores.IdFornecedor);
            cbofornecedor.DisplayMember = nameof(DTO_Fornecedores.NomeFornecedor);
            cbofornecedor.DataSource = fornecedores.Listar();
        }

        private void cbofornecedor_SelectedIndexChanged(object sender, EventArgs e)
        {
            PedidoFornecedorBusiness pedidoFornecedorBusiness = new PedidoFornecedorBusiness();
            DTO_Fornecedores dto = cbofornecedor.SelectedItem as DTO_Fornecedores;
            dgvpedidofornecedorconsulta.AutoGenerateColumns = false;
            dgvpedidofornecedorconsulta.DataSource = pedidoFornecedorBusiness.ConsultarporID(dto.IdFornecedor);
        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            frmMenu menu = new frmMenu();
            menu.Show();
            this.Close();
        }
    }
}
