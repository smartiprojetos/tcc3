﻿using Lanchonha.Banco.BD.CadastroProduto;
using Lanchonha.Banco.BD.Estoque;
using Lanchonha.Banco.BD.Fornecedor;
using Lanchonha.Banco.BD.Fornecedor.PedidoFornecedor;
using Lanchonha.Banco.BD.Funcionarios;
using Lanchonha.Banco.BD.ProdutoFornecedor;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Lanchonha.Telas.TelasFornecedor
{
    public partial class frmPedidoFornecedor : Form
    {

        
        public frmPedidoFornecedor()
        {
            InitializeComponent();
            CarregarComboFornecedor();
            Grid();
        }
        BindingList<PedidoparaFornecedorGridDTO> produtosfornecedor = new BindingList<PedidoparaFornecedorGridDTO>();
        List<DTO_Fornecedores> lista1 = new List<DTO_Fornecedores>();
        DTONomedeProdutoeFornecedor produtodto = new DTONomedeProdutoeFornecedor();
        DTO_Fornecedores fornecedoresdto = new DTO_Fornecedores();
        int pk;
        
        decimal preco,total;
        

        private void label11_Click(object sender, EventArgs e)
        {

        }

        private void label14_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void label13_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void CarregarComboFornecedor()
        {
            //carregando o combo de fornecedor
            Business_Fornecedores business2 = new Business_Fornecedores();
            lista1 =business2.Listar();
            cboFornecedor.DisplayMember = nameof(DTO_Fornecedores.NomeFornecedor);
            cboFornecedor.DataSource = lista1;
            

        }

        private void Conta()
        {

        }

        private void Grid()
        {
            dgvpedidofornecedor.AutoGenerateColumns = false;
            dgvpedidofornecedor.DataSource = produtosfornecedor;


        }
        

        private void cboFornecedor_SelectedIndexChanged(object sender, EventArgs e)
        {
            //pegando o id de fornecedor para fazer o select altomatico do produtos que 
            //ele fonece
            fornecedoresdto = cboFornecedor.SelectedItem as DTO_Fornecedores;
            pk = fornecedoresdto.IdFornecedor;

            //pegando os produtos ligados a fornecedor
            //e adicionando no combobox
            ProdutoFornecedorBusiness business = new ProdutoFornecedorBusiness();
            cboProduto.ValueMember = nameof(DTONomedeProdutoeFornecedor.IDprodutoFornecedor);
            cboProduto.DisplayMember = nameof(DTONomedeProdutoeFornecedor.NomeProduto);
            cboProduto.ValueMember = nameof(DTONomedeProdutoeFornecedor.PrecoProduto);
            cboProduto.ValueMember = nameof(DTONomedeProdutoeFornecedor.IdProduto);
            List<DTONomedeProdutoeFornecedor> lista = business.Consultar(pk);
            cboProduto.DataSource = lista; 
        }

        private void ColocarDadosNagrid()
        {
            //pegando os dados do produto para inserir na grid, do ComboBox
            produtodto = cboProduto.SelectedItem as DTONomedeProdutoeFornecedor;
            PedidoparaFornecedorGridDTO pedidofornecedor = new PedidoparaFornecedorGridDTO();

            if (txtquantidade.Text != null)
            {
                //Adcionando na grid com um DTO especifico para ela e para
                //o pedidofornecedor
                pedidofornecedor.NomeProduto = produtodto.NomeProduto;
                pedidofornecedor.NomeFornecedor = fornecedoresdto.NomeFornecedor;
                pedidofornecedor.FkProdutoFornecedor = produtodto.IDprodutoFornecedor;
                pedidofornecedor.IDProduto = produtodto.IdProduto;
                preco = produtodto.PrecoProduto;
                int quantidade = 0;

                quantidade = Convert.ToInt32((txtquantidade.Text));
                total = preco * quantidade;
                pedidofornecedor.Quantidade = int.Parse(txtquantidade.Text);
                pedidofornecedor.PrecoTotal = total;
                int fk = UserSession.UsuarioLogado.Id_funcionario;
                pedidofornecedor.FKFuncionario = fk;
                pedidofornecedor.DataCompra = dtpdatavenda.Value;

                produtosfornecedor.Add(pedidofornecedor);
            }
            else if (txtquantidade.Text == null)
            {
                MessageBox.Show("Por favor preencha o campo quantidade.", "Back's", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }

        }



        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void frmPedidoFornecedor_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtquantidade.Text == string.Empty)
                {
                    MessageBox.Show("Por favor preencha todos os campos.", "Back's", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    PedidoFornecedorBusiness fornecedorBusiness = new PedidoFornecedorBusiness();
                    fornecedorBusiness.Salvar(produtosfornecedor.ToList());

                    //vou dar um insert em estoque, para assim que o pedido fornecedor for feito já 
                    //eu já ter o estoque atualizado no sistema, dai criei outro salvar na Bussines de
                    //DTO estoque que salva uma lista, essa lista vem dessa tela com todos os itens nessarios
                    //para serem salvos lá, a lista que eu passo é a mesma da grid                 
                    Business_Estoque businessestoque = new Business_Estoque();
                    businessestoque.SalvarporLista(produtosfornecedor.ToList());
                    MessageBox.Show("Dados salvos com sucesso.", "Back's", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }

            }
            catch (Exception)
            {
                MessageBox.Show("Ouve algum erro por favor chamar um técnico.", "Back's", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }

        }

        private void button3_Click(object sender, EventArgs e)
        {
            try
            {
                ColocarDadosNagrid();

            }
            catch (Exception ex)
            {
                MessageBox.Show("Por favor preencher o campo quantidade.", "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }


        }

        private void button4_Click(object sender, EventArgs e)
        {
           
        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            frmMenu menu = new frmMenu();
            menu.Show();
            this.Close();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            try { 
            dgvpedidofornecedor.Rows.RemoveAt(dgvpedidofornecedor.CurrentRow.Index);
            }
            catch (Exception ex)
            {
                MessageBox.Show("A tabela precisa estar preenchida para que se torne possivel excluir o itens dela.", "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void txtquantidade_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && (e.KeyChar != ','))
            {
                e.Handled = true;
                MessageBox.Show("este campo aceita somente numero e virgula", "Back's", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            if ((e.KeyChar == ',') && ((sender as TextBox).Text.IndexOf('.') > -1))
            {
                e.Handled = true;
                MessageBox.Show("este campo aceita somente uma virgula", "Back's", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

      

      

       
    }
}
