﻿using Lanchonha.Banco.BD.CadastroProduto;
using Lanchonha.Banco.BD.Fornecedor;
using Lanchonha.Banco.BD.ProdutoFornecedor;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Lanchonha.Telas.TelasFornecedor
{
    public partial class frmProdutoFornecedor : Form
    {
        BindingList<DTONomedeProdutoeFornecedor> listadagrid = new BindingList<DTONomedeProdutoeFornecedor>();
        
        public frmProdutoFornecedor()
        {
            InitializeComponent();
            CarregarComboProduto();
            CarregarComboFornecedor();
            ConfigurarGrid();
        }

        private void label13_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void label14_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        void CarregarComboProduto()
        {
            Business_CadastroProduto cadastroProduto = new Business_CadastroProduto();
            List<DTO_CadastroProduto> dto = cadastroProduto.Listar();

            cboproduto.ValueMember = nameof(DTO_CadastroProduto.ID);
            cboproduto.DisplayMember = nameof(DTO_CadastroProduto.Produto);
            cboproduto.DataSource = dto;
        }

        void CarregarComboFornecedor()
        {
            Business_Fornecedores fornecedores = new Business_Fornecedores();
            List<DTO_Fornecedores> dto = fornecedores.Listar();

            cbofornecedor.ValueMember = nameof(DTO_Fornecedores.IdFornecedor);
            cbofornecedor.DisplayMember = nameof(DTO_Fornecedores.NomeFornecedor);

            cbofornecedor.DataSource = dto;
        }

        void ConfigurarGrid()
        {
            dgvCarrinho.AutoGenerateColumns = false;
            dgvCarrinho.DataSource = listadagrid;
        }

        private void AdicionaraGrid()
        {
            
        }

        private void button1_Click(object sender, EventArgs e)
        {
            DTO_CadastroProduto dto = cboproduto.SelectedItem as DTO_CadastroProduto;
            DTO_Fornecedores fornecedores = cbofornecedor.SelectedItem as DTO_Fornecedores;
            DTONomedeProdutoeFornecedor produtoeFornecedor = new DTONomedeProdutoeFornecedor();

            produtoeFornecedor.IdProduto = dto.ID;
            produtoeFornecedor.NomeProduto = dto.Produto;
            produtoeFornecedor.IdFornecedor = fornecedores.IdFornecedor;
            produtoeFornecedor.NomeFornecedor = fornecedores.NomeFornecedor;

            listadagrid.Add(produtoeFornecedor);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            try { 
            DTONomedeProdutoeFornecedor produtoeFornecedor = new DTONomedeProdutoeFornecedor();
            ProdutoFornecedorBusiness business = new ProdutoFornecedorBusiness();
            business.Salvar( listadagrid.ToList());
            MessageBox.Show("Dados salvos com sucesso.", "Back's", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch(Exception)
            {
                MessageBox.Show("Por favor inserir os dados corretamente.", "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            frmMenu menu = new frmMenu();
            menu.Show();
            this.Close();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            try
            {
                dgvCarrinho.Rows.RemoveAt(dgvCarrinho.CurrentRow.Index);
            }
            catch (Exception)
            {
                MessageBox.Show("Não existe mais itens para serem retirados.", "Back's", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
           
        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void cbofornecedor_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void cboproduto_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}
