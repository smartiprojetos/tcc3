﻿using Lanchonha.Banco.BD.CadastroProduto;
using Lanchonha.Banco.BD.Funcionarios;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Lanchonha.Telas.CadastroProduto
{
    public partial class frmCadastrarProdutos : Form
    {
        public frmCadastrarProdutos()
        {
            InitializeComponent();
        }

        private void SalvarDados()
        {
            int fk = UserSession.UsuarioLogado.Id_funcionario;
            DTO_CadastroProduto dto = new DTO_CadastroProduto();
            dto.Produto = txtproduto.Text.Trim();
            dto.Preco = decimal.Parse(txtpreco.Text);
            dto.Validade = dtpvalidade.Value;
            dto.FkFuncionario = fk;
            Business_CadastroProduto salva = new Business_CadastroProduto();
            salva.Salvar(dto);
        }

        private void LimparCampos()
        {
            txtproduto.Text = string.Empty;
            txtpreco.Text = string.Empty;
            dtpvalidade.Value = DateTime.Now;
        }

        private void label14_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void label13_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                SalvarDados();
                MessageBox.Show("Dados salvos com sucesso", "Salvo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }

            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            LimparCampos();
        }

        private void txtpreco_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && (e.KeyChar != ','))
            {
                e.Handled = true;
                MessageBox.Show("este campo aceita somente numero e virgula");
            }
            if ((e.KeyChar == ',') && ((sender as TextBox).Text.IndexOf('.') > -1))
            {
                e.Handled = true;
                MessageBox.Show("este campo aceita somente uma virgula", "Back's", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            frmMenu menu = new frmMenu();
            menu.Show();
            this.Close();
        }

        private void txtpreco_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
