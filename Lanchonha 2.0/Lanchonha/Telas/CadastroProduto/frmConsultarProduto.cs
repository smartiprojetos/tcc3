﻿using Lanchonha.Banco.BD.CadastroProduto;
using Lanchonha.Banco.BD.Funcionarios;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Lanchonha.Telas.CadastroProduto
{
    public partial class frmConsultarProduto : Form
    {
        public frmConsultarProduto()
        {
            InitializeComponent();
            CarregarPermissoes();
        }

        private void label5_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void label13_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void CarregarDados()
        {
            Business_CadastroProduto business = new Business_CadastroProduto();
            dgvproduto.AutoGenerateColumns = false;
            dgvproduto.DataSource = business.Consultar(txtproduto.Text);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            CarregarDados();
        }

        private void dgvproduto_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 4)
            { 
                DTO_CadastroProduto dto = dgvproduto.CurrentRow.DataBoundItem as DTO_CadastroProduto;
                FrmAlterarProduto alterarproduto = new FrmAlterarProduto();
                alterarproduto.LoadScreen(dto.ID);
                alterarproduto.Show();
                this.Close();
            }

            if(e.ColumnIndex == 5)
            {
                DTO_CadastroProduto dto = dgvproduto.CurrentRow.DataBoundItem as DTO_CadastroProduto;
                int pk = dto.ID;
                Business_CadastroProduto business = new Business_CadastroProduto();
                business.Remover(pk);
                CarregarDados();
            }

        }

        private void CarregarPermissoes()
        {
            if (UserSession.UsuarioLogado.permissao_produto_alterar == false)
            {

                dgvproduto.Columns[4].Visible = false;
                dgvproduto.Columns[5].Visible = false;
            }

        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            frmMenu menu = new frmMenu();
            menu.Show();
            this.Close();
        }
    }
}
