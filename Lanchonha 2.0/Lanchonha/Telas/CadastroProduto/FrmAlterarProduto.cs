﻿using Lanchonha.Banco.BD.CadastroProduto;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Lanchonha.Telas.CadastroProduto
{
    public partial class FrmAlterarProduto : Form
    {
        public FrmAlterarProduto()
        {
            InitializeComponent();
        }
        int pk;

        private void LimparDados()
        {
            lblId.Text = string.Empty;
            txtproduto.Text = string.Empty;
            txtpreco.Text = string.Empty;
            mktvalidade.Text = string.Empty;
        }
        private void CarregarDados()
        {
            Business_CadastroProduto business = new Business_CadastroProduto();
            List<DTO_CadastroProduto> lista = business.Buscar(int.Parse(lblId.Text));
            DTO_CadastroProduto dto = lista[0];
            
            txtproduto.Text = dto.Produto;
            mktvalidade.Text = dto.Validade.ToString();
            txtpreco.Text = dto.Preco.ToString();
            pk = dto.FkFuncionario;
        }

        private DTO_CadastroProduto AlterarDados()
        {
            DTO_CadastroProduto dto = new DTO_CadastroProduto();
            Business_CadastroProduto business = new Business_CadastroProduto();
            dto.ID = int.Parse(lblId.Text);
            dto.Produto = txtproduto.Text.Trim();
            dto.Validade = DateTime.Parse(mktvalidade.Text);
            dto.Preco = decimal.Parse(txtpreco.Text);
            dto.FkFuncionario = pk;
            business.Alterar(dto);
            return dto;
        }
        private void label14_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void label13_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void FrmAlterarProduto_Load(object sender, EventArgs e)
        {

        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void button4_Click(object sender, EventArgs e)
        {
            CarregarDados();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            LimparDados();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
            AlterarDados();
            MessageBox.Show("Dados alterados com sucesso", "Salvo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void LoadScreen(int Id)
        {
            lblId.Text = Id.ToString();
        }

        private void button3_Click(object sender, EventArgs e)
        {

        }

        private void txtpreco_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && (e.KeyChar != ','))
            {
                e.Handled = true;
                MessageBox.Show("este campo aceita somente numero e virgula");
            }
            if ((e.KeyChar == ',') && ((sender as TextBox).Text.IndexOf('.') > -1))
            {
                e.Handled = true;
                MessageBox.Show("este campo aceita somente uma virgula","Back's",MessageBoxButtons.OK,MessageBoxIcon.Information);
            }
        }

        private void txtproduto_KeyPress(object sender, KeyPressEventArgs e)
        {

        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            frmMenu menu = new frmMenu();
            menu.Show();
            this.Close();
        }
    }
}
