﻿using Lanchonha.Telas.TelasFuncionarios;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Lanchonha.Telas.Outros
{
    public partial class Splash : Form
    {
        public Splash()
        {
            InitializeComponent();

            //Começa a contagem para o início do SPLASH
            Task.Factory.StartNew(() =>
            {
                //Espera cerca de 5 segundos.
                System.Threading.Thread.Sleep(3000);

                Invoke(new Action(() =>
                {
                    frmLogin TELA = new frmLogin();
                    TELA.Show();
                    this.Hide();

                }));
            });
        }

        private void Splash_Load(object sender, EventArgs e)
        {
            label1.Parent = pictureBox1;
            label1.BackColor = Color.Transparent;
        }

        private void label1_Click(object sender, EventArgs e)
        {
          
        }
    }
}
