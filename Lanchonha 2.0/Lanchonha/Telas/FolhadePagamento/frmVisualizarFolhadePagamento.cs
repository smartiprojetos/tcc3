﻿using Lanchonha.Banco.BD.FolhaPgmt;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Lanchonha.Telas.FolhadePagamento
{
    public partial class frmVisualizarFolhadePagamento : Form
    {
        public frmVisualizarFolhadePagamento()
        {
            InitializeComponent();
        }
        public void CarregarCampos(int ID)
        {
            lblCod.Text = ID.ToString();

            FolhaPgmtBusiness business = new FolhaPgmtBusiness();
            List<view_func_folha> lista = business.ConsultaPorID(ID);
            view_func_folha folha = lista[0];

            lblNome.Text = folha.nm_NomeFunc;
            lblFuncao.Text = folha.ds_Cargo;
            lblMensagem.Text = folha.ds_Mensagem;
            lblDiasTrabalhados.Text = folha.ds_DiasTrabalhados.ToString();

            lblSalario.Text = $"R$ {folha.vl_mesSalario}";
            lblVencimentos.Text = $"R$ {folha.vl_Proventos}";
            lblDescontos.Text = $"R$ {folha.vl_Descontos}";
            lblReceber.Text = $"R$ {folha.vl_Liquido}";

            lblValeTransporte.Text = $"R$ {folha.vl_VT}";
            lblValeRefeicao.Text = $"R$ {folha.vl_VR}";
            lblINSS.Text = $"R$ {folha.vl_INSS}";
            lblHoraE50.Text = $"R$ {folha.vl_HoraE50}";
            lblHoraE100.Text = $"R$ {folha.vl_HoraE100}";
            lblDedIR.Text = $"R$ {folha.vl_DedIR}";
            lblIR.Text = $"R$ {folha.vl_IR}";
            lblValorIR.Text = $"R$ {folha.vl_ValorIR}";
            lblValorFGTS.Text = $"R$ {folha.vl_ValorFGTS}";

            lblSalarioBase.Text = $"R$ {folha.vl_SalarioBruto}";
            lblINSSBase.Text = $"R$ {folha.vl_BaseINSS}";
            lblFGTSBase.Text = $"R$ {folha.vl_BaseFGTS}";
            lblFGTSMes.Text = $"R$ {folha.vl_FGTSmes}";
            lblBaseIRRF.Text = $"R$ {folha.vl_BaseIRRF}";
            lblFaixaIRRF.Text = $"R$ {folha.vl_FaixaIRRF}";

        }

        private void label39_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void label6_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }
    }
}
