﻿using Lanchonha.Banco.BD.FolhaPgmt;
using Lanchonha.Banco.BD.Funcionarios;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Lanchonha.Telas.FolhadePagamento
{
    public partial class frmFolhadePagamento : Form
    {
        public frmFolhadePagamento()
        {
            InitializeComponent();
        }
        decimal liquido;
        private void frmFolhadePagamento_Load(object sender, EventArgs e)
        {

        }

        private void label11_Click(object sender, EventArgs e)
        {

        }

        private void BusCarCPF()
        {
            try { 
            Business_Funcionarios funcionarios = new Business_Funcionarios();
            List<FuncionarioDTO> lista = funcionarios.ConsultarCPF(mktCPF.Text);
            FuncionarioDTO dto = lista[0];
            txtNome.Text = dto.nomecompleto;
            txtCargo.Text = dto.cargo;
            txtid.Text = dto.Id_funcionario.ToString();
            }
            catch (Exception)
            {
                MessageBox.Show("Por favor colocar um CPF cadastrado no Sistema", "Back's", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            frmMenu menu = new frmMenu();
            menu.Show();
            this.Close();
        }

        private void GerarFolha()
        {
            try
            {
                FolhaPgmtDTO dto = new FolhaPgmtDTO();

                dto.ds_DiasTrabalhados = txtDias.Text == string.Empty ? 0 : Convert.ToInt32(txtDias.Text);
                dto.ds_HoraE100 = txtHorasE100.Text == string.Empty ? 0 : Convert.ToInt32(txtHorasE100.Text);
                dto.ds_HoraE50 = txtHorasE50.Text == string.Empty ? 0 : Convert.ToInt32(txtHorasE50.Text);
                dto.ds_Mensagem = txtMensagem.Text;
                dto.dt_Registro = mcdiapagamento.SelectionStart; ;
                dto.fk_FolhaPgmt_Func = int.Parse(txtid.Text);
                dto.vl_SalarioBruto = txtSalarioBruto.Text == string.Empty ? 0 : Convert.ToDecimal(txtSalarioBruto.Text);
                dto.FKEmpresa = 1;
                FolhaPgmtBusiness business = new FolhaPgmtBusiness();

                liquido= business.SalvarFolha(dto);
                lblSalario.Text = liquido.ToString();

                MessageBox.Show("Folha de Pagamento criada com sucesso!", "Back's", MessageBoxButtons.OK, MessageBoxIcon.Information);
                
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message, "Back's", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception ex)
            {
                if (ex.Message.Contains("cadeia de caracteres"))
                    MessageBox.Show("O salário não está num formato correto!");
                else
                    MessageBox.Show("Ocorreu um erro não identificado: " + ex.Message, "Back's", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void txtNome_KeyPress(object sender, KeyPressEventArgs e)
        {

        }

        private void txtSalarioBruto_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && (e.KeyChar != ','))
            {
                e.Handled = true;
                MessageBox.Show("Este campo aceita somente numero e virgula", "Back's", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            if ((e.KeyChar == ',') && ((sender as TextBox).Text.IndexOf('.') > -1))
            {
                e.Handled = true;
                MessageBox.Show("este campo aceita somente uma virgula", "Back's", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void txtDias_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && (e.KeyChar != ','))
            {
                e.Handled = true;
                MessageBox.Show("Este campo aceita somente numero e virgula", "Back's", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            if ((e.KeyChar == ',') && ((sender as TextBox).Text.IndexOf('.') > -1))
            {
                e.Handled = true;
                MessageBox.Show("este campo aceita somente uma virgula", "Back's", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void txtHorasE50_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && (e.KeyChar != ','))
            {
                e.Handled = true;
                MessageBox.Show("Este campo aceita somente numero e virgula", "Back's", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            if ((e.KeyChar == ',') && ((sender as TextBox).Text.IndexOf('.') > -1))
            {
                e.Handled = true;
                MessageBox.Show("este campo aceita somente uma virgula", "Back's", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void txtHorasE100_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && (e.KeyChar != ','))
            {
                e.Handled = true;
                MessageBox.Show("Este campo aceita somente numero e virgula", "Back's", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            if ((e.KeyChar == ',') && ((sender as TextBox).Text.IndexOf('.') > -1))
            {
                e.Handled = true;
                MessageBox.Show("este campo aceita somente uma virgula", "Back's", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void btnGerar_Click(object sender, EventArgs e)
        {
            GerarFolha();
        }

        private void Pesquisar_Click(object sender, EventArgs e)
        {
            BusCarCPF();
        }

        private void label5_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void label1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void txtDias_KeyPress_1(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && (e.KeyChar != ','))
            {
                e.Handled = true;
                MessageBox.Show("Este campo aceita somente numero e virgula","Back's",MessageBoxButtons.OK,MessageBoxIcon.Error);
            }
            if ((e.KeyChar == ',') && ((sender as TextBox).Text.IndexOf('.') > -1))
            {
                e.Handled = true;
                MessageBox.Show("este campo aceita somente uma virgula", "Back's", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void txtDias_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtHorasE50_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
    
    
