﻿using Lanchonha.Banco.BD.FolhaPgmt;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Lanchonha.Telas.FolhadePagamento
{
    public partial class frmConsultarFolhadePagamento : Form
    {
        public frmConsultarFolhadePagamento()
        {
            InitializeComponent();
            ListarDados();
        }

        private void txtCPF_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtCPF_MaskInputRejected(object sender, MaskInputRejectedEventArgs e)
        {

        }
        private void CarregarDados()
        {
            dgvFolha.AutoGenerateColumns = false;
            FolhaPgmtBusiness business = new FolhaPgmtBusiness();
            dgvFolha.DataSource = business.ConsultaPorCPF(txtCPF.Text); 
        }
        
        private void ListarDados()
        {
            dgvFolha.AutoGenerateColumns = false;
            FolhaPgmtBusiness business = new FolhaPgmtBusiness();
            dgvFolha.DataSource = business.Listar();
        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {
            CarregarDados();
        }

        private void label5_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void label13_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            frmMenu menu = new frmMenu();
            menu.Show();
            this.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            view_func_folha data = dgvFolha.CurrentRow.DataBoundItem as view_func_folha;

            frmVisualizarFolhadePagamento folhadePagamento = new frmVisualizarFolhadePagamento();
            folhadePagamento.CarregarCampos(data.id_Folha_Pgmt);
            folhadePagamento.Show();
        }
    }
}
