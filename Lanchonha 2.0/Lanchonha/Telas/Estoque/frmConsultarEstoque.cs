﻿using Lanchonha.Banco.BD.Estoque;
using Lanchonha.Banco.BD.Funcionarios;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Lanchonha.Telas.Estoque
{
    public partial class frmConsultarEstoque : Form
    {
        public frmConsultarEstoque()
        {
            InitializeComponent();
            CarregarPermissoes();
        }

        private void label5_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void label13_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void CarregarDados()
        {
            dgvestoque.AutoGenerateColumns = false;
            Business_Estoque estoque = new Business_Estoque();
            List<ViewConsultarEstoqueDTO> viewConsultars = estoque.ConsultarEstoqueView(txtproduto.Text);
            dgvestoque.DataSource = viewConsultars;
        }

        private void frmConsultarEstoque_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            CarregarDados();
        }

        private void dgvestoque_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if(e.ColumnIndex==9)
            {
                ViewConsultarEstoqueDTO estoque = dgvestoque.CurrentRow.DataBoundItem as ViewConsultarEstoqueDTO;
                int id = estoque.IDestoque;
                string nomeproduto = estoque.NomeProduto;
                frmAlterarEstoque AlterarDados = new frmAlterarEstoque();
                AlterarDados.LoadScreen(id, nomeproduto);
                AlterarDados.Show();
                this.Close();
            }

            if(e.ColumnIndex==10)
            {
                ViewConsultarEstoqueDTO estoque = dgvestoque.CurrentRow.DataBoundItem as ViewConsultarEstoqueDTO;
                int id = estoque.IDestoque;
                Business_Estoque business = new Business_Estoque();
                business.Remover(id);
                CarregarDados();
            }
        }

        private void CarregarPermissoes()
        {
            if (UserSession.UsuarioLogado.permissao_estoque_alterar == false)
            {
                dgvestoque.Columns[9].Visible = false;
                dgvestoque.Columns[10].Visible = false;
            }

        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            frmMenu menu = new frmMenu();
            menu.Show();
            this.Close();
        }
    }
}
