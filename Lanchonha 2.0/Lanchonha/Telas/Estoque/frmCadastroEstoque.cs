﻿using Lanchonha.Banco.BD.CadastroProduto;
using Lanchonha.Banco.BD.Estoque;
using Lanchonha.Banco.BD.Funcionarios;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Lanchonha.Telas.Estoque
{
    public partial class frmCadastroEstoque : Form
    {
        public frmCadastroEstoque()
        {
            InitializeComponent();
            CarregarComboProduto();
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }

        private void textBox6_TextChanged(object sender, EventArgs e)
        {

        }

        private void label14_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void label13_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void CarregarComboProduto()
        {
            Business_CadastroProduto cadastroProduto = new Business_CadastroProduto();
            List<DTO_CadastroProduto> lista = cadastroProduto.Listar();
            cboproduto.ValueMember = nameof(DTO_CadastroProduto.ID);
            cboproduto.DisplayMember = nameof(DTO_CadastroProduto.Produto);
            cboproduto.DataSource = lista;
        }

        private void SalvarDados()
        {
            int pk =UserSession.UsuarioLogado.Id_funcionario;
            DTO_CadastroProduto produto = cboproduto.SelectedItem as DTO_CadastroProduto;
            DTO_Estoque dto = new DTO_Estoque();
            Business_Estoque business = new Business_Estoque();
            dto.FKIdProduto =produto.ID;
            dto.IdFuncionarioParaEs = pk;

            dto.Quantidadeentrada = int.Parse(txtqntentrada.Text);
            dto.DataEntrada = dtpentrada.Value;
            dto.ValorTotal = int.Parse(txtvltotal.Text);
            dto.QuantidadeSaida = int.Parse(txtqntsaida.Text);
            dto.DataSaida = dtpsaida.Value;
            dto.RetornoValor=int.Parse(txtretorno.Text);
            dto.Condicao = txtcondicao.Text;
            business.Salvar(dto);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            { 
            SalvarDados();
            MessageBox.Show("Dados salvos com sucesso", "Salvo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch(Exception ex)
            {
            MessageBox.Show(ex.Message, "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            txtqntentrada.Text = string.Empty;
            txtqntsaida.Text= string.Empty;
            txtretorno.Text= string.Empty;
            txtvltotal.Text= string.Empty;
        }

        private void txtqntentrada_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && (e.KeyChar != ','))
            {
                e.Handled = true;
                MessageBox.Show("este campo aceita somente numero e virgula", "Back's", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            if ((e.KeyChar == ',') && ((sender as TextBox).Text.IndexOf('.') > -1))
            {
                e.Handled = true;
                MessageBox.Show("este campo aceita somente uma virgula", "Back's", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void txtvltotal_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && (e.KeyChar != ','))
            {
                e.Handled = true;
                MessageBox.Show("este campo aceita somente numero e virgula", "Back's", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            if ((e.KeyChar == ',') && ((sender as TextBox).Text.IndexOf('.') > -1))
            {
                e.Handled = true;
                MessageBox.Show("este campo aceita somente uma virgula", "Back's", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void txtqntsaida_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && (e.KeyChar != ','))
            {
                e.Handled = true;
                MessageBox.Show("este campo aceita somente numero e virgula", "Back's", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            if ((e.KeyChar == ',') && ((sender as TextBox).Text.IndexOf('.') > -1))
            {
                e.Handled = true;
                MessageBox.Show("este campo aceita somente uma virgula", "Back's", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void txtretorno_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && (e.KeyChar != ','))
            {
                e.Handled = true;
                MessageBox.Show("este campo aceita somente numero e virgula", "Back's", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            if ((e.KeyChar == ',') && ((sender as TextBox).Text.IndexOf('.') > -1))
            {
                e.Handled = true;
                MessageBox.Show("este campo aceita somente uma virgula", "Back's", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            frmMenu menu = new frmMenu();
            menu.Show();
            this.Close();
        }

        private void txtvltotal_TextChanged(object sender, EventArgs e)
        {

        }

        private void frmCadastroEstoque_Load(object sender, EventArgs e)
        {

        }
    }
}
