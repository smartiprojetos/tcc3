﻿namespace Lanchonha.Telas.Estoque
{
    partial class frmAlterarEstoque
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmAlterarEstoque));
            this.button1 = new System.Windows.Forms.Button();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.txtvlretono = new System.Windows.Forms.TextBox();
            this.txtvltotal = new System.Windows.Forms.TextBox();
            this.txtqntsaida = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.txtqntentrada = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.button4 = new System.Windows.Forms.Button();
            this.label7 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.lblidestoque = new System.Windows.Forms.Label();
            this.lblnomeproduto = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.mktdtsaida = new System.Windows.Forms.MaskedTextBox();
            this.mktdtentrada = new System.Windows.Forms.MaskedTextBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.txtcondicao = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.ForeColor = System.Drawing.Color.Firebrick;
            this.button1.Location = new System.Drawing.Point(312, 454);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(113, 50);
            this.button1.TabIndex = 9;
            this.button1.Text = "Alterar";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.BackColor = System.Drawing.Color.Transparent;
            this.label13.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.ForeColor = System.Drawing.Color.Firebrick;
            this.label13.Location = new System.Drawing.Point(380, 6);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(18, 21);
            this.label13.TabIndex = 73;
            this.label13.Text = "_";
            this.label13.Click += new System.EventHandler(this.label13_Click);
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.BackColor = System.Drawing.Color.Transparent;
            this.label14.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.Color.Firebrick;
            this.label14.Location = new System.Drawing.Point(399, 6);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(19, 21);
            this.label14.TabIndex = 72;
            this.label14.Text = "X";
            this.label14.Click += new System.EventHandler(this.label14_Click);
            // 
            // txtvlretono
            // 
            this.txtvlretono.Location = new System.Drawing.Point(12, 412);
            this.txtvlretono.Multiline = true;
            this.txtvlretono.Name = "txtvlretono";
            this.txtvlretono.Size = new System.Drawing.Size(244, 19);
            this.txtvlretono.TabIndex = 6;
            this.txtvlretono.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtvlretono_KeyPress);
            // 
            // txtvltotal
            // 
            this.txtvltotal.Location = new System.Drawing.Point(12, 254);
            this.txtvltotal.Multiline = true;
            this.txtvltotal.Name = "txtvltotal";
            this.txtvltotal.Size = new System.Drawing.Size(244, 19);
            this.txtvltotal.TabIndex = 3;
            this.txtvltotal.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtvltotal_KeyPress);
            // 
            // txtqntsaida
            // 
            this.txtqntsaida.Location = new System.Drawing.Point(12, 308);
            this.txtqntsaida.Multiline = true;
            this.txtqntsaida.Name = "txtqntsaida";
            this.txtqntsaida.Size = new System.Drawing.Size(244, 19);
            this.txtqntsaida.TabIndex = 4;
            this.txtqntsaida.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtqntsaida_KeyPress);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.Firebrick;
            this.label11.Location = new System.Drawing.Point(12, 184);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(145, 21);
            this.label11.TabIndex = 66;
            this.label11.Text = "Data da Entrada";
            this.label11.Click += new System.EventHandler(this.label11_Click);
            // 
            // txtqntentrada
            // 
            this.txtqntentrada.Location = new System.Drawing.Point(12, 147);
            this.txtqntentrada.Multiline = true;
            this.txtqntentrada.Name = "txtqntentrada";
            this.txtqntentrada.Size = new System.Drawing.Size(244, 19);
            this.txtqntentrada.TabIndex = 1;
            this.txtqntentrada.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtqntentrada_KeyPress);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Firebrick;
            this.label6.Location = new System.Drawing.Point(12, 391);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(116, 21);
            this.label6.TabIndex = 64;
            this.label6.Text = "Valor Retorno";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Firebrick;
            this.label4.Location = new System.Drawing.Point(12, 233);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(93, 21);
            this.label4.TabIndex = 63;
            this.label4.Text = "Valor Total";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Firebrick;
            this.label3.Location = new System.Drawing.Point(12, 339);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(124, 21);
            this.label3.TabIndex = 62;
            this.label3.Text = "Data de Saida";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Firebrick;
            this.label2.Location = new System.Drawing.Point(9, 287);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(156, 21);
            this.label2.TabIndex = 61;
            this.label2.Text = "Quantidade Saida";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Firebrick;
            this.label1.Location = new System.Drawing.Point(12, 126);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(201, 21);
            this.label1.TabIndex = 60;
            this.label1.Text = "Quantidade de Entrada";
            // 
            // button4
            // 
            this.button4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button4.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button4.ForeColor = System.Drawing.Color.Firebrick;
            this.button4.Location = new System.Drawing.Point(312, 398);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(113, 50);
            this.button4.TabIndex = 8;
            this.button4.Text = "Carregar Dados";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.Firebrick;
            this.label7.Location = new System.Drawing.Point(-2, 6);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(201, 19);
            this.label7.TabIndex = 81;
            this.label7.Text = "Alterar dados do Estoque";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.Firebrick;
            this.label9.Location = new System.Drawing.Point(6, 40);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(119, 21);
            this.label9.TabIndex = 83;
            this.label9.Text = "ID do Estoque";
            // 
            // lblidestoque
            // 
            this.lblidestoque.AutoSize = true;
            this.lblidestoque.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblidestoque.ForeColor = System.Drawing.Color.Firebrick;
            this.lblidestoque.Location = new System.Drawing.Point(131, 40);
            this.lblidestoque.Name = "lblidestoque";
            this.lblidestoque.Size = new System.Drawing.Size(19, 21);
            this.lblidestoque.TabIndex = 84;
            this.lblidestoque.Text = "0";
            // 
            // lblnomeproduto
            // 
            this.lblnomeproduto.AutoSize = true;
            this.lblnomeproduto.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblnomeproduto.ForeColor = System.Drawing.Color.Firebrick;
            this.lblnomeproduto.Location = new System.Drawing.Point(12, 98);
            this.lblnomeproduto.Name = "lblnomeproduto";
            this.lblnomeproduto.Size = new System.Drawing.Size(72, 21);
            this.lblnomeproduto.TabIndex = 86;
            this.lblnomeproduto.Text = "Produto";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.Firebrick;
            this.label8.Location = new System.Drawing.Point(9, 71);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(148, 21);
            this.label8.TabIndex = 85;
            this.label8.Text = "Nome do Produto";
            // 
            // mktdtsaida
            // 
            this.mktdtsaida.Location = new System.Drawing.Point(12, 368);
            this.mktdtsaida.Mask = "00/00/0000";
            this.mktdtsaida.Name = "mktdtsaida";
            this.mktdtsaida.Size = new System.Drawing.Size(244, 20);
            this.mktdtsaida.TabIndex = 5;
            this.mktdtsaida.ValidatingType = typeof(System.DateTime);
            // 
            // mktdtentrada
            // 
            this.mktdtentrada.Location = new System.Drawing.Point(15, 210);
            this.mktdtentrada.Mask = "00/00/0000";
            this.mktdtentrada.Name = "mktdtentrada";
            this.mktdtentrada.Size = new System.Drawing.Size(241, 20);
            this.mktdtentrada.TabIndex = 2;
            this.mktdtentrada.ValidatingType = typeof(System.DateTime);
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = global::Lanchonha.Properties.Resources.IconesBaita_ver_02;
            this.pictureBox2.Location = new System.Drawing.Point(378, 328);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(47, 50);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox2.TabIndex = 112;
            this.pictureBox2.TabStop = false;
            this.pictureBox2.Click += new System.EventHandler(this.pictureBox2_Click);
            // 
            // txtcondicao
            // 
            this.txtcondicao.Location = new System.Drawing.Point(15, 470);
            this.txtcondicao.MaxLength = 45;
            this.txtcondicao.Multiline = true;
            this.txtcondicao.Name = "txtcondicao";
            this.txtcondicao.Size = new System.Drawing.Size(244, 19);
            this.txtcondicao.TabIndex = 7;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Firebrick;
            this.label5.Location = new System.Drawing.Point(9, 449);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(88, 21);
            this.label5.TabIndex = 113;
            this.label5.Text = "Condição";
            // 
            // frmAlterarEstoque
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(443, 507);
            this.Controls.Add(this.txtcondicao);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.mktdtentrada);
            this.Controls.Add(this.mktdtsaida);
            this.Controls.Add(this.lblnomeproduto);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.lblidestoque);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.txtvlretono);
            this.Controls.Add(this.txtvltotal);
            this.Controls.Add(this.txtqntsaida);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.txtqntentrada);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frmAlterarEstoque";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "frmAlterarEstoque";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox txtvlretono;
        private System.Windows.Forms.TextBox txtvltotal;
        private System.Windows.Forms.TextBox txtqntsaida;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox txtqntentrada;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label lblidestoque;
        private System.Windows.Forms.Label lblnomeproduto;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.MaskedTextBox mktdtsaida;
        private System.Windows.Forms.MaskedTextBox mktdtentrada;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.TextBox txtcondicao;
        private System.Windows.Forms.Label label5;
    }
}