﻿using Lanchonha.Banco.BD.Estoque;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Lanchonha.Telas.Estoque
{
    public partial class frmAlterarEstoque : Form
    {
        public frmAlterarEstoque()
        {
            InitializeComponent();
        }

        private void label11_Click(object sender, EventArgs e)
        {

        }

        private void label14_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void label13_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        public void LoadScreen(int idestoque, string nomeproduto)
        {
            lblidestoque.Text = idestoque.ToString();
            lblnomeproduto.Text = nomeproduto;
        }
        int idproduto, idfuncionario;

        private void CarregarDados()
        {
            Business_Estoque business = new Business_Estoque();
            List<DTO_Estoque> listar = business.Consultarporid(int.Parse(lblidestoque.Text));
            DTO_Estoque dto = listar[0];
            txtqntentrada.Text = dto.Quantidadeentrada.ToString();
            mktdtentrada.Text = dto.DataEntrada.ToString();
            txtvltotal.Text = dto.ValorTotal.ToString();
            txtqntsaida.Text = dto.QuantidadeSaida.ToString();
            mktdtsaida.Text = dto.DataSaida.ToString();
            txtvlretono.Text = dto.RetornoValor.ToString();
            idproduto = dto.FKIdProduto;
            idfuncionario = dto.IdFuncionarioParaEs;
            txtcondicao.Text = dto.Condicao;
        }

        private void button4_Click(object sender, EventArgs e)
        {
            CarregarDados();
        }

        private void SalvarDados()
        {
            Business_Estoque estoque = new Business_Estoque();
            DTO_Estoque dto = new DTO_Estoque();
            dto.IDestoque = int.Parse(lblidestoque.Text);
            dto.Quantidadeentrada = int.Parse(txtqntentrada.Text);
            dto.DataEntrada = DateTime.Parse(mktdtentrada.Text);
            dto.ValorTotal = decimal.Parse(txtvltotal.Text);
            dto.QuantidadeSaida = int.Parse(txtqntsaida.Text);
            dto.DataSaida = DateTime.Parse(mktdtsaida.Text);
            dto.RetornoValor = decimal.Parse(txtvlretono.Text);
            dto.FKIdProduto = idproduto;
            dto.IdFuncionarioParaEs = idfuncionario;
            dto.Condicao = txtcondicao.Text;
            estoque.Alterar(dto);
        }
        
        private void txtqntentrada_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && (e.KeyChar != ','))
            {
                e.Handled = true;
                MessageBox.Show("este campo aceita somente numero e virgula", "Back's", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            if ((e.KeyChar == ',') && ((sender as TextBox).Text.IndexOf('.') > -1))
            {
                e.Handled = true;
                MessageBox.Show("este campo aceita somente uma virgula", "Back's", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void txtvltotal_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && (e.KeyChar != ','))
            {
                e.Handled = true;
                MessageBox.Show("este campo aceita somente numero e virgula", "Back's", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            if ((e.KeyChar == ',') && ((sender as TextBox).Text.IndexOf('.') > -1))
            {
                e.Handled = true;
                MessageBox.Show("este campo aceita somente uma virgula", "Back's", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void txtqntsaida_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && (e.KeyChar != ','))
            {
                e.Handled = true;
                MessageBox.Show("este campo aceita somente numero e virgula", "Back's", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            if ((e.KeyChar == ',') && ((sender as TextBox).Text.IndexOf('.') > -1))
            {
                e.Handled = true;
                MessageBox.Show("este campo aceita somente uma virgula", "Back's", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void txtvlretono_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && (e.KeyChar != ','))
            {
                e.Handled = true;
                MessageBox.Show("este campo aceita somente numero e virgula", "Back's", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            if ((e.KeyChar == ',') && ((sender as TextBox).Text.IndexOf('.') > -1))
            {
                e.Handled = true;
                MessageBox.Show("este campo aceita somente uma virgula", "Back's", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            frmMenu menu = new frmMenu();
            menu.Show();
            this.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                SalvarDados();
                MessageBox.Show("Dados salvos com sucesso", "Salvo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }

            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
