﻿using Lanchonha.Banco.BD.Funcionarios;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Lanchonha.Telas.TelasFuncionarios
{
    public partial class frmConsultarFuncionario : Form
    {
        public frmConsultarFuncionario()
        {
            InitializeComponent();
            CarregarPermissoes();
        }
       

        private void label5_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void label13_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            CarregarGrid();
        }
        private void CarregarGrid()
        {
            Business_Funcionarios business = new Business_Funcionarios();
            List<FuncionarioDTO> dto = business.Consultar(txtnome.Text);
            dgvfuncionario.AutoGenerateColumns = false;
            dgvfuncionario.DataSource = dto;
        }

     

        private void dgvfuncionario_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if(e.ColumnIndex == 3)
            {
                FuncionarioDTO dto= dgvfuncionario.CurrentRow.DataBoundItem as FuncionarioDTO;
                
                frmAlterarFuncionario telaalterar = new frmAlterarFuncionario();
                int id = dto.Id_funcionario;
                telaalterar.LoadScreen(id);
                telaalterar.Show();
                this.Close();
            }

            if(e.ColumnIndex == 4)
            {
                FuncionarioDTO dto = dgvfuncionario.CurrentRow.DataBoundItem as FuncionarioDTO;
                int pk = dto.Id_funcionario;
                Business_Funcionarios business = new Business_Funcionarios();
                business.Remover(pk);
                CarregarGrid();

            }
            
       

        }


        private void CarregarPermissoes()
        {
           if(UserSession.UsuarioLogado.permissao_funcionario_alterar==false)
           {
                
                dgvfuncionario.Columns[3].Visible = false;
                dgvfuncionario.Columns[4].Visible = false;
           }

        }

        private void frmConsultarFuncionario_Load(object sender, EventArgs e)
        {
            label1.Parent = pictureBox1;
            label1.BackColor = Color.Transparent;
            label2.Parent = pictureBox1;
            label2.BackColor = Color.Transparent;
            label13.Parent = pictureBox1;
            label13.BackColor = Color.Transparent;
            label5.Parent = pictureBox1;
            label5.BackColor = Color.Transparent;
        }

        private void txtnome_TextChanged(object sender, EventArgs e)
        {

        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            frmMenu menu = new frmMenu();
            menu.Show();
            this.Close();
        }
    }
}
