﻿using Lanchonha.Banco.BD.Funcionarios;
using Lanchonha.Utilitarios;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Lanchonha.Telas.TelasFuncionarios
{
    public partial class frmAlterarSenha : Form
    {
        public frmAlterarSenha()
        {
            InitializeComponent();
        }
        CriacaodeCodigo cod;
        string codigo;
        int idusuario;
        string novasenha;
        Business_Funcionarios bus = new Business_Funcionarios();
        private void EnviarConfirmacao()
        {

            cod = new CriacaodeCodigo();
            codigo = cod.Codigo4();

            string para = txtemail.Text;
            string copiaoculta = "smarti7empresa@gmail.com";
            string copia = "smarti7empresa@gmail.com";
            string assunto = "Modificação de senha";
            bool comhtml = false;
            string mensagem =
                "Olá, informamos que o usuario "
                + txtnomeusuario.Text
                + " está realizando o pedido para efetuar a "
                + "troca de senha, caso não seja você, ignore esta mensagem "
                + "caso seja envie o codigo abaixo "
                + codigo;

            Email email = new Email();
            email.EnviarGmail(para, copia, copiaoculta, assunto, mensagem, comhtml);
            MessageBox.Show("Código enviado com sucesso.","Back's",MessageBoxButtons.OK,MessageBoxIcon.Information);
        }

        private bool ConferirCOdigo()
        {
            bool codcorreto;
            string codinformado = txtcod.Text;
            if (codigo == codinformado)
            {
                MessageBox.Show("Código correto",
                                "Back's",
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Information);
                codcorreto = true;

            }
            else
            {
                DialogResult re = MessageBox.Show("Codigo incorreto.",
                                   "Back's",
                                   MessageBoxButtons.OK,
                                   MessageBoxIcon.Information);

                codcorreto = false;
            }
            return codcorreto;
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
           
            FuncionarioDTO dto = new FuncionarioDTO();
            try
            {
             List<FuncionarioDTO> lista = bus.ConsultarporUsuario(txtnomeusuario.Text);
            dto = lista[0];
            idusuario = dto.Id_funcionario;
            novasenha = txtconfirmarsenha.Text;
                if (txtnovasenha.Text == txtconfirmarsenha.Text)
                {
                    EnviarConfirmacao();
                }
                else
                {
                    MessageBox.Show("Senhas não conincidem, por favor re-escreva.",
                                "Back's",
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Information);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Por favor insira um usuário cadastrado no sistema.", "Back's", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
           
        }

        private void button2_Click_1(object sender, EventArgs e)
        {
           
            
            bool cod = ConferirCOdigo();
            
            if (cod == true)
            {
                try
                {
                    bus.AlterarSenha(idusuario, novasenha);

                    MessageBox.Show("Senha alterada com sucesso",
                                    "Back's",
                                    MessageBoxButtons.OK,
                                    MessageBoxIcon.Information);
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "Back's", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }

            }
        }

        private void label13_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void label6_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            frmLogin login = new frmLogin();
            login.Show();
            this.Close();
        }
    }

}
