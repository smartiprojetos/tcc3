﻿namespace Lanchonha.Telas.TelasFuncionarios
{
    partial class frmCadastroFuncionario
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmCadastroFuncionario));
            this.label12 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.txtconfirmarsenha = new System.Windows.Forms.TextBox();
            this.label21 = new System.Windows.Forms.Label();
            this.txtbairro = new System.Windows.Forms.TextBox();
            this.txtsenha = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtusuario = new System.Windows.Forms.TextBox();
            this.mktdatanascimento = new System.Windows.Forms.MaskedTextBox();
            this.txtcidade = new System.Windows.Forms.TextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.txtuf = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.txtnumerocasa = new System.Windows.Forms.TextBox();
            this.txtrua = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.button3 = new System.Windows.Forms.Button();
            this.mktcep = new System.Windows.Forms.MaskedTextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.mktcelular = new System.Windows.Forms.MaskedTextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.mkttelefone = new System.Windows.Forms.MaskedTextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.cbosexo = new System.Windows.Forms.ComboBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.mktrg = new System.Windows.Forms.MaskedTextBox();
            this.mktcpf = new System.Windows.Forms.MaskedTextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.dtpdatacadastro = new System.Windows.Forms.DateTimePicker();
            this.button2 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.txtcargo = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.txtnome = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.panel16 = new System.Windows.Forms.Panel();
            this.panel15 = new System.Windows.Forms.Panel();
            this.panel14 = new System.Windows.Forms.Panel();
            this.panel13 = new System.Windows.Forms.Panel();
            this.panel12 = new System.Windows.Forms.Panel();
            this.panel11 = new System.Windows.Forms.Panel();
            this.panel10 = new System.Windows.Forms.Panel();
            this.panel17 = new System.Windows.Forms.Panel();
            this.panel9 = new System.Windows.Forms.Panel();
            this.panel8 = new System.Windows.Forms.Panel();
            this.panel7 = new System.Windows.Forms.Panel();
            this.panel6 = new System.Windows.Forms.Panel();
            this.panel5 = new System.Windows.Forms.Panel();
            this.panel4 = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.chkfolhaalterar = new System.Windows.Forms.CheckBox();
            this.chkfolhaconsultar = new System.Windows.Forms.CheckBox();
            this.chkfolhacadastrar = new System.Windows.Forms.CheckBox();
            this.chkprodutoalterar = new System.Windows.Forms.CheckBox();
            this.chkfolhadepagamento = new System.Windows.Forms.CheckBox();
            this.label37 = new System.Windows.Forms.Label();
            this.chkfluxodecaixacadastrar = new System.Windows.Forms.CheckBox();
            this.chkfornecedor = new System.Windows.Forms.CheckBox();
            this.chkpedido = new System.Windows.Forms.CheckBox();
            this.chkfluxodecaixa = new System.Windows.Forms.CheckBox();
            this.chkcardapio = new System.Windows.Forms.CheckBox();
            this.chkcliente = new System.Windows.Forms.CheckBox();
            this.chkestoque = new System.Windows.Forms.CheckBox();
            this.chkfuncionarios = new System.Windows.Forms.CheckBox();
            this.chkproduto = new System.Windows.Forms.CheckBox();
            this.label36 = new System.Windows.Forms.Label();
            this.chkcardapiovinculardados = new System.Windows.Forms.CheckBox();
            this.chkfornecedorvinculardados = new System.Windows.Forms.CheckBox();
            this.chkpedidosparafornecedor = new System.Windows.Forms.CheckBox();
            this.label35 = new System.Windows.Forms.Label();
            this.label34 = new System.Windows.Forms.Label();
            this.chkfornecedorcadastrar = new System.Windows.Forms.CheckBox();
            this.chkfornecedoralterar = new System.Windows.Forms.CheckBox();
            this.chkfornecedorconsultar = new System.Windows.Forms.CheckBox();
            this.chkfuncionariosalterar = new System.Windows.Forms.CheckBox();
            this.chkfuncionariosconsultar = new System.Windows.Forms.CheckBox();
            this.chkpedidocadastrar = new System.Windows.Forms.CheckBox();
            this.chkpedidoconsultar = new System.Windows.Forms.CheckBox();
            this.chkfluxodecaixaconsultar = new System.Windows.Forms.CheckBox();
            this.chkprodutoconsultar = new System.Windows.Forms.CheckBox();
            this.chkcardapiocadastrar = new System.Windows.Forms.CheckBox();
            this.chkcardapioalterar = new System.Windows.Forms.CheckBox();
            this.chkcardapioconsultar = new System.Windows.Forms.CheckBox();
            this.chkclientealterar = new System.Windows.Forms.CheckBox();
            this.chkclienteconsultar = new System.Windows.Forms.CheckBox();
            this.chkestoquecadastrar = new System.Windows.Forms.CheckBox();
            this.chkestoquealterar = new System.Windows.Forms.CheckBox();
            this.chkestoqueconsultar = new System.Windows.Forms.CheckBox();
            this.chkfuncionarioscadastrar = new System.Windows.Forms.CheckBox();
            this.chkprodutocadastrar = new System.Windows.Forms.CheckBox();
            this.label33 = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.panel10.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.SuspendLayout();
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Century Gothic", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.Color.Firebrick;
            this.label12.Location = new System.Drawing.Point(287, 9);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(241, 24);
            this.label12.TabIndex = 12;
            this.label12.Text = "Cadastrar Funcionários";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Font = new System.Drawing.Font("Comic Sans MS", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Firebrick;
            this.label5.Location = new System.Drawing.Point(768, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(26, 27);
            this.label5.TabIndex = 27;
            this.label5.Text = "X";
            this.label5.Click += new System.EventHandler(this.label5_Click);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.BackColor = System.Drawing.Color.Transparent;
            this.label13.Font = new System.Drawing.Font("Comic Sans MS", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.ForeColor = System.Drawing.Color.Firebrick;
            this.label13.Location = new System.Drawing.Point(746, -10);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(24, 27);
            this.label13.TabIndex = 28;
            this.label13.Text = "_";
            this.label13.Click += new System.EventHandler(this.label13_Click);
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabControl1.Location = new System.Drawing.Point(-4, 30);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(817, 470);
            this.tabControl1.TabIndex = 29;
            // 
            // tabPage1
            // 
            this.tabPage1.BackColor = System.Drawing.Color.White;
            this.tabPage1.Controls.Add(this.txtconfirmarsenha);
            this.tabPage1.Controls.Add(this.label21);
            this.tabPage1.Controls.Add(this.txtbairro);
            this.tabPage1.Controls.Add(this.txtsenha);
            this.tabPage1.Controls.Add(this.label3);
            this.tabPage1.Controls.Add(this.label2);
            this.tabPage1.Controls.Add(this.txtusuario);
            this.tabPage1.Controls.Add(this.mktdatanascimento);
            this.tabPage1.Controls.Add(this.txtcidade);
            this.tabPage1.Controls.Add(this.label20);
            this.tabPage1.Controls.Add(this.txtuf);
            this.tabPage1.Controls.Add(this.label19);
            this.tabPage1.Controls.Add(this.txtnumerocasa);
            this.tabPage1.Controls.Add(this.txtrua);
            this.tabPage1.Controls.Add(this.label18);
            this.tabPage1.Controls.Add(this.button3);
            this.tabPage1.Controls.Add(this.mktcep);
            this.tabPage1.Controls.Add(this.label17);
            this.tabPage1.Controls.Add(this.label16);
            this.tabPage1.Controls.Add(this.label15);
            this.tabPage1.Controls.Add(this.mktcelular);
            this.tabPage1.Controls.Add(this.label14);
            this.tabPage1.Controls.Add(this.mkttelefone);
            this.tabPage1.Controls.Add(this.label10);
            this.tabPage1.Controls.Add(this.cbosexo);
            this.tabPage1.Controls.Add(this.label9);
            this.tabPage1.Controls.Add(this.label8);
            this.tabPage1.Controls.Add(this.mktrg);
            this.tabPage1.Controls.Add(this.mktcpf);
            this.tabPage1.Controls.Add(this.label7);
            this.tabPage1.Controls.Add(this.label6);
            this.tabPage1.Controls.Add(this.dtpdatacadastro);
            this.tabPage1.Controls.Add(this.button2);
            this.tabPage1.Controls.Add(this.button1);
            this.tabPage1.Controls.Add(this.txtcargo);
            this.tabPage1.Controls.Add(this.label11);
            this.tabPage1.Controls.Add(this.txtnome);
            this.tabPage1.Controls.Add(this.label4);
            this.tabPage1.Controls.Add(this.label1);
            this.tabPage1.ForeColor = System.Drawing.Color.Firebrick;
            this.tabPage1.Location = new System.Drawing.Point(4, 25);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(809, 441);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Dados";
            // 
            // txtconfirmarsenha
            // 
            this.txtconfirmarsenha.Location = new System.Drawing.Point(176, 211);
            this.txtconfirmarsenha.Name = "txtconfirmarsenha";
            this.txtconfirmarsenha.Size = new System.Drawing.Size(183, 21);
            this.txtconfirmarsenha.TabIndex = 7;
            this.txtconfirmarsenha.UseSystemPasswordChar = true;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Baskerville Old Face", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.ForeColor = System.Drawing.Color.Firebrick;
            this.label21.Location = new System.Drawing.Point(53, 211);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(117, 18);
            this.label21.TabIndex = 113;
            this.label21.Text = "Confirmar Senha";
            // 
            // txtbairro
            // 
            this.txtbairro.Location = new System.Drawing.Point(512, 210);
            this.txtbairro.MaxLength = 70;
            this.txtbairro.Name = "txtbairro";
            this.txtbairro.Size = new System.Drawing.Size(145, 21);
            this.txtbairro.TabIndex = 17;
            // 
            // txtsenha
            // 
            this.txtsenha.Location = new System.Drawing.Point(118, 177);
            this.txtsenha.MaxLength = 500;
            this.txtsenha.Name = "txtsenha";
            this.txtsenha.Size = new System.Drawing.Size(241, 21);
            this.txtsenha.TabIndex = 6;
            this.txtsenha.UseSystemPasswordChar = true;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Baskerville Old Face", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Firebrick;
            this.label3.Location = new System.Drawing.Point(53, 178);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(46, 18);
            this.label3.TabIndex = 109;
            this.label3.Text = "Senha";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Baskerville Old Face", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Firebrick;
            this.label2.Location = new System.Drawing.Point(53, 143);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(59, 18);
            this.label2.TabIndex = 108;
            this.label2.Text = "Usuario";
            // 
            // txtusuario
            // 
            this.txtusuario.Location = new System.Drawing.Point(118, 143);
            this.txtusuario.MaxLength = 60;
            this.txtusuario.Multiline = true;
            this.txtusuario.Name = "txtusuario";
            this.txtusuario.Size = new System.Drawing.Size(241, 19);
            this.txtusuario.TabIndex = 5;
            // 
            // mktdatanascimento
            // 
            this.mktdatanascimento.Location = new System.Drawing.Point(177, 279);
            this.mktdatanascimento.Mask = "00/00/0000";
            this.mktdatanascimento.Name = "mktdatanascimento";
            this.mktdatanascimento.Size = new System.Drawing.Size(182, 21);
            this.mktdatanascimento.TabIndex = 9;
            this.mktdatanascimento.ValidatingType = typeof(System.DateTime);
            // 
            // txtcidade
            // 
            this.txtcidade.Location = new System.Drawing.Point(512, 176);
            this.txtcidade.MaxLength = 100;
            this.txtcidade.Multiline = true;
            this.txtcidade.Name = "txtcidade";
            this.txtcidade.Size = new System.Drawing.Size(145, 19);
            this.txtcidade.TabIndex = 16;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Baskerville Old Face", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.ForeColor = System.Drawing.Color.Firebrick;
            this.label20.Location = new System.Drawing.Point(442, 176);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(53, 18);
            this.label20.TabIndex = 107;
            this.label20.Text = "Cidade";
            // 
            // txtuf
            // 
            this.txtuf.Location = new System.Drawing.Point(512, 141);
            this.txtuf.MaxLength = 2;
            this.txtuf.Multiline = true;
            this.txtuf.Name = "txtuf";
            this.txtuf.Size = new System.Drawing.Size(145, 19);
            this.txtuf.TabIndex = 15;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Baskerville Old Face", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.ForeColor = System.Drawing.Color.Firebrick;
            this.label19.Location = new System.Drawing.Point(442, 142);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(51, 18);
            this.label19.TabIndex = 106;
            this.label19.Text = "Estado";
            // 
            // txtnumerocasa
            // 
            this.txtnumerocasa.Location = new System.Drawing.Point(512, 279);
            this.txtnumerocasa.Multiline = true;
            this.txtnumerocasa.Name = "txtnumerocasa";
            this.txtnumerocasa.Size = new System.Drawing.Size(145, 19);
            this.txtnumerocasa.TabIndex = 19;
            // 
            // txtrua
            // 
            this.txtrua.Location = new System.Drawing.Point(512, 244);
            this.txtrua.MaxLength = 100;
            this.txtrua.Multiline = true;
            this.txtrua.Name = "txtrua";
            this.txtrua.Size = new System.Drawing.Size(145, 19);
            this.txtrua.TabIndex = 18;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Baskerville Old Face", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.ForeColor = System.Drawing.Color.Firebrick;
            this.label18.Location = new System.Drawing.Point(442, 278);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(24, 18);
            this.label18.TabIndex = 105;
            this.label18.Text = "N°";
            // 
            // button3
            // 
            this.button3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button3.ForeColor = System.Drawing.Color.Firebrick;
            this.button3.Location = new System.Drawing.Point(663, 102);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(54, 32);
            this.button3.TabIndex = 14;
            this.button3.Text = "Buscar";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click_1);
            // 
            // mktcep
            // 
            this.mktcep.Location = new System.Drawing.Point(512, 107);
            this.mktcep.Mask = "00000-000";
            this.mktcep.Name = "mktcep";
            this.mktcep.Size = new System.Drawing.Size(145, 21);
            this.mktcep.TabIndex = 13;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Baskerville Old Face", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.ForeColor = System.Drawing.Color.Firebrick;
            this.label17.Location = new System.Drawing.Point(442, 210);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(50, 18);
            this.label17.TabIndex = 104;
            this.label17.Text = "Bairro";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Baskerville Old Face", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.ForeColor = System.Drawing.Color.Firebrick;
            this.label16.Location = new System.Drawing.Point(442, 244);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(33, 18);
            this.label16.TabIndex = 103;
            this.label16.Text = "Rua";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Baskerville Old Face", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.ForeColor = System.Drawing.Color.Firebrick;
            this.label15.Location = new System.Drawing.Point(442, 108);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(38, 18);
            this.label15.TabIndex = 102;
            this.label15.Text = "CEP";
            // 
            // mktcelular
            // 
            this.mktcelular.Location = new System.Drawing.Point(512, 72);
            this.mktcelular.Mask = "(99) 99999-9999";
            this.mktcelular.Name = "mktcelular";
            this.mktcelular.Size = new System.Drawing.Size(145, 21);
            this.mktcelular.TabIndex = 12;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Baskerville Old Face", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.Color.Firebrick;
            this.label14.Location = new System.Drawing.Point(442, 72);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(55, 18);
            this.label14.TabIndex = 101;
            this.label14.Text = "Celular";
            // 
            // mkttelefone
            // 
            this.mkttelefone.Location = new System.Drawing.Point(512, 33);
            this.mkttelefone.Mask = "(00) 0000-0000";
            this.mkttelefone.Name = "mkttelefone";
            this.mkttelefone.Size = new System.Drawing.Size(145, 21);
            this.mkttelefone.TabIndex = 11;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Baskerville Old Face", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.Firebrick;
            this.label10.Location = new System.Drawing.Point(442, 33);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(64, 18);
            this.label10.TabIndex = 100;
            this.label10.Text = "Telefone";
            // 
            // cbosexo
            // 
            this.cbosexo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbosexo.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbosexo.FormattingEnabled = true;
            this.cbosexo.Items.AddRange(new object[] {
            "Masculino",
            "Feminino"});
            this.cbosexo.Location = new System.Drawing.Point(97, 311);
            this.cbosexo.Name = "cbosexo";
            this.cbosexo.Size = new System.Drawing.Size(262, 24);
            this.cbosexo.TabIndex = 10;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Baskerville Old Face", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.Firebrick;
            this.label9.Location = new System.Drawing.Point(53, 317);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(38, 18);
            this.label9.TabIndex = 99;
            this.label9.Text = "Sexo";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Baskerville Old Face", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.Firebrick;
            this.label8.Location = new System.Drawing.Point(53, 279);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(118, 18);
            this.label8.TabIndex = 98;
            this.label8.Text = "Data Nascimento";
            // 
            // mktrg
            // 
            this.mktrg.Location = new System.Drawing.Point(254, 73);
            this.mktrg.Mask = "00.000.000-0";
            this.mktrg.Name = "mktrg";
            this.mktrg.Size = new System.Drawing.Size(105, 21);
            this.mktrg.TabIndex = 3;
            // 
            // mktcpf
            // 
            this.mktcpf.Location = new System.Drawing.Point(118, 73);
            this.mktcpf.Mask = "000.000.000-00";
            this.mktcpf.Name = "mktcpf";
            this.mktcpf.Size = new System.Drawing.Size(84, 21);
            this.mktcpf.TabIndex = 2;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Baskerville Old Face", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.Firebrick;
            this.label7.Location = new System.Drawing.Point(218, 74);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(30, 18);
            this.label7.TabIndex = 97;
            this.label7.Text = "RG";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Baskerville Old Face", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Firebrick;
            this.label6.Location = new System.Drawing.Point(53, 244);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(118, 18);
            this.label6.TabIndex = 96;
            this.label6.Text = "Data de Cadastro";
            // 
            // dtpdatacadastro
            // 
            this.dtpdatacadastro.Location = new System.Drawing.Point(177, 242);
            this.dtpdatacadastro.Name = "dtpdatacadastro";
            this.dtpdatacadastro.Size = new System.Drawing.Size(182, 21);
            this.dtpdatacadastro.TabIndex = 8;
            // 
            // button2
            // 
            this.button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button2.ForeColor = System.Drawing.Color.Firebrick;
            this.button2.Location = new System.Drawing.Point(555, 317);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(102, 50);
            this.button2.TabIndex = 21;
            this.button2.Text = "Cancelar";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click_1);
            // 
            // button1
            // 
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.ForeColor = System.Drawing.Color.Firebrick;
            this.button1.Location = new System.Drawing.Point(445, 317);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(104, 50);
            this.button1.TabIndex = 20;
            this.button1.Text = "Salvar";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click_1);
            // 
            // txtcargo
            // 
            this.txtcargo.Location = new System.Drawing.Point(118, 109);
            this.txtcargo.MaxLength = 60;
            this.txtcargo.Multiline = true;
            this.txtcargo.Name = "txtcargo";
            this.txtcargo.Size = new System.Drawing.Size(241, 19);
            this.txtcargo.TabIndex = 4;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Baskerville Old Face", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.Firebrick;
            this.label11.Location = new System.Drawing.Point(53, 73);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(37, 18);
            this.label11.TabIndex = 81;
            this.label11.Text = "CPF";
            // 
            // txtnome
            // 
            this.txtnome.Location = new System.Drawing.Point(118, 33);
            this.txtnome.MaxLength = 50;
            this.txtnome.Multiline = true;
            this.txtnome.Name = "txtnome";
            this.txtnome.Size = new System.Drawing.Size(241, 19);
            this.txtnome.TabIndex = 1;
            this.txtnome.TextChanged += new System.EventHandler(this.txtnome_TextChanged);
            this.txtnome.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtnome_KeyDown);
            this.txtnome.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtnome_KeyPress);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Baskerville Old Face", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Firebrick;
            this.label4.Location = new System.Drawing.Point(53, 109);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(47, 18);
            this.label4.TabIndex = 71;
            this.label4.Text = "Cargo";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Baskerville Old Face", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Firebrick;
            this.label1.Location = new System.Drawing.Point(53, 33);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(52, 18);
            this.label1.TabIndex = 68;
            this.label1.Text = "Nome ";
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.panel16);
            this.tabPage2.Controls.Add(this.panel15);
            this.tabPage2.Controls.Add(this.panel14);
            this.tabPage2.Controls.Add(this.panel13);
            this.tabPage2.Controls.Add(this.panel12);
            this.tabPage2.Controls.Add(this.panel11);
            this.tabPage2.Controls.Add(this.panel10);
            this.tabPage2.Controls.Add(this.panel9);
            this.tabPage2.Controls.Add(this.panel8);
            this.tabPage2.Controls.Add(this.panel7);
            this.tabPage2.Controls.Add(this.panel6);
            this.tabPage2.Controls.Add(this.panel5);
            this.tabPage2.Controls.Add(this.panel4);
            this.tabPage2.Controls.Add(this.panel3);
            this.tabPage2.Controls.Add(this.panel2);
            this.tabPage2.Controls.Add(this.panel1);
            this.tabPage2.Controls.Add(this.chkfolhaalterar);
            this.tabPage2.Controls.Add(this.chkfolhaconsultar);
            this.tabPage2.Controls.Add(this.chkfolhacadastrar);
            this.tabPage2.Controls.Add(this.chkprodutoalterar);
            this.tabPage2.Controls.Add(this.chkfolhadepagamento);
            this.tabPage2.Controls.Add(this.label37);
            this.tabPage2.Controls.Add(this.chkfluxodecaixacadastrar);
            this.tabPage2.Controls.Add(this.chkfornecedor);
            this.tabPage2.Controls.Add(this.chkpedido);
            this.tabPage2.Controls.Add(this.chkfluxodecaixa);
            this.tabPage2.Controls.Add(this.chkcardapio);
            this.tabPage2.Controls.Add(this.chkcliente);
            this.tabPage2.Controls.Add(this.chkestoque);
            this.tabPage2.Controls.Add(this.chkfuncionarios);
            this.tabPage2.Controls.Add(this.chkproduto);
            this.tabPage2.Controls.Add(this.label36);
            this.tabPage2.Controls.Add(this.chkcardapiovinculardados);
            this.tabPage2.Controls.Add(this.chkfornecedorvinculardados);
            this.tabPage2.Controls.Add(this.chkpedidosparafornecedor);
            this.tabPage2.Controls.Add(this.label35);
            this.tabPage2.Controls.Add(this.label34);
            this.tabPage2.Controls.Add(this.chkfornecedorcadastrar);
            this.tabPage2.Controls.Add(this.chkfornecedoralterar);
            this.tabPage2.Controls.Add(this.chkfornecedorconsultar);
            this.tabPage2.Controls.Add(this.chkfuncionariosalterar);
            this.tabPage2.Controls.Add(this.chkfuncionariosconsultar);
            this.tabPage2.Controls.Add(this.chkpedidocadastrar);
            this.tabPage2.Controls.Add(this.chkpedidoconsultar);
            this.tabPage2.Controls.Add(this.chkfluxodecaixaconsultar);
            this.tabPage2.Controls.Add(this.chkprodutoconsultar);
            this.tabPage2.Controls.Add(this.chkcardapiocadastrar);
            this.tabPage2.Controls.Add(this.chkcardapioalterar);
            this.tabPage2.Controls.Add(this.chkcardapioconsultar);
            this.tabPage2.Controls.Add(this.chkclientealterar);
            this.tabPage2.Controls.Add(this.chkclienteconsultar);
            this.tabPage2.Controls.Add(this.chkestoquecadastrar);
            this.tabPage2.Controls.Add(this.chkestoquealterar);
            this.tabPage2.Controls.Add(this.chkestoqueconsultar);
            this.tabPage2.Controls.Add(this.chkfuncionarioscadastrar);
            this.tabPage2.Controls.Add(this.chkprodutocadastrar);
            this.tabPage2.Controls.Add(this.label33);
            this.tabPage2.Controls.Add(this.label32);
            this.tabPage2.Controls.Add(this.label31);
            this.tabPage2.Controls.Add(this.label30);
            this.tabPage2.Controls.Add(this.label29);
            this.tabPage2.Controls.Add(this.label28);
            this.tabPage2.Controls.Add(this.label27);
            this.tabPage2.Controls.Add(this.label26);
            this.tabPage2.Controls.Add(this.label25);
            this.tabPage2.Controls.Add(this.label24);
            this.tabPage2.Controls.Add(this.label23);
            this.tabPage2.Controls.Add(this.label22);
            this.tabPage2.Location = new System.Drawing.Point(4, 25);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(809, 441);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Permissões";
            this.tabPage2.UseVisualStyleBackColor = true;
            this.tabPage2.Click += new System.EventHandler(this.tabPage2_Click);
            // 
            // panel16
            // 
            this.panel16.BackColor = System.Drawing.Color.Firebrick;
            this.panel16.Location = new System.Drawing.Point(0, 17);
            this.panel16.Name = "panel16";
            this.panel16.Size = new System.Drawing.Size(819, 10);
            this.panel16.TabIndex = 59;
            // 
            // panel15
            // 
            this.panel15.BackColor = System.Drawing.Color.Firebrick;
            this.panel15.Location = new System.Drawing.Point(3, 359);
            this.panel15.Name = "panel15";
            this.panel15.Size = new System.Drawing.Size(819, 10);
            this.panel15.TabIndex = 60;
            // 
            // panel14
            // 
            this.panel14.BackColor = System.Drawing.Color.Firebrick;
            this.panel14.Location = new System.Drawing.Point(684, 18);
            this.panel14.Name = "panel14";
            this.panel14.Size = new System.Drawing.Size(10, 349);
            this.panel14.TabIndex = 60;
            // 
            // panel13
            // 
            this.panel13.BackColor = System.Drawing.Color.Firebrick;
            this.panel13.Location = new System.Drawing.Point(552, 18);
            this.panel13.Name = "panel13";
            this.panel13.Size = new System.Drawing.Size(10, 348);
            this.panel13.TabIndex = 60;
            // 
            // panel12
            // 
            this.panel12.BackColor = System.Drawing.Color.Firebrick;
            this.panel12.Location = new System.Drawing.Point(477, 16);
            this.panel12.Name = "panel12";
            this.panel12.Size = new System.Drawing.Size(10, 352);
            this.panel12.TabIndex = 60;
            // 
            // panel11
            // 
            this.panel11.BackColor = System.Drawing.Color.Firebrick;
            this.panel11.Location = new System.Drawing.Point(378, 17);
            this.panel11.Name = "panel11";
            this.panel11.Size = new System.Drawing.Size(10, 352);
            this.panel11.TabIndex = 60;
            // 
            // panel10
            // 
            this.panel10.BackColor = System.Drawing.Color.Firebrick;
            this.panel10.Controls.Add(this.panel17);
            this.panel10.Location = new System.Drawing.Point(268, 15);
            this.panel10.Name = "panel10";
            this.panel10.Size = new System.Drawing.Size(10, 353);
            this.panel10.TabIndex = 59;
            // 
            // panel17
            // 
            this.panel17.BackColor = System.Drawing.Color.Firebrick;
            this.panel17.Location = new System.Drawing.Point(0, 0);
            this.panel17.Name = "panel17";
            this.panel17.Size = new System.Drawing.Size(10, 353);
            this.panel17.TabIndex = 60;
            // 
            // panel9
            // 
            this.panel9.BackColor = System.Drawing.Color.Firebrick;
            this.panel9.Location = new System.Drawing.Point(3, 328);
            this.panel9.Name = "panel9";
            this.panel9.Size = new System.Drawing.Size(819, 10);
            this.panel9.TabIndex = 59;
            // 
            // panel8
            // 
            this.panel8.BackColor = System.Drawing.Color.Firebrick;
            this.panel8.Location = new System.Drawing.Point(3, 295);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(819, 10);
            this.panel8.TabIndex = 59;
            // 
            // panel7
            // 
            this.panel7.BackColor = System.Drawing.Color.Firebrick;
            this.panel7.Location = new System.Drawing.Point(0, 257);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(819, 10);
            this.panel7.TabIndex = 59;
            // 
            // panel6
            // 
            this.panel6.BackColor = System.Drawing.Color.Firebrick;
            this.panel6.Location = new System.Drawing.Point(0, 224);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(819, 10);
            this.panel6.TabIndex = 59;
            // 
            // panel5
            // 
            this.panel5.BackColor = System.Drawing.Color.Firebrick;
            this.panel5.Location = new System.Drawing.Point(0, 191);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(819, 10);
            this.panel5.TabIndex = 59;
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.Firebrick;
            this.panel4.Location = new System.Drawing.Point(0, 155);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(819, 10);
            this.panel4.TabIndex = 59;
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.Firebrick;
            this.panel3.Location = new System.Drawing.Point(0, 121);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(819, 10);
            this.panel3.TabIndex = 59;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.Firebrick;
            this.panel2.Location = new System.Drawing.Point(0, 85);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(819, 10);
            this.panel2.TabIndex = 59;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Firebrick;
            this.panel1.Location = new System.Drawing.Point(-2, 52);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(819, 10);
            this.panel1.TabIndex = 58;
            // 
            // chkfolhaalterar
            // 
            this.chkfolhaalterar.AutoSize = true;
            this.chkfolhaalterar.Location = new System.Drawing.Point(509, 277);
            this.chkfolhaalterar.Name = "chkfolhaalterar";
            this.chkfolhaalterar.Size = new System.Drawing.Size(15, 14);
            this.chkfolhaalterar.TabIndex = 57;
            this.chkfolhaalterar.UseVisualStyleBackColor = true;
            // 
            // chkfolhaconsultar
            // 
            this.chkfolhaconsultar.AutoSize = true;
            this.chkfolhaconsultar.Location = new System.Drawing.Point(323, 277);
            this.chkfolhaconsultar.Name = "chkfolhaconsultar";
            this.chkfolhaconsultar.Size = new System.Drawing.Size(15, 14);
            this.chkfolhaconsultar.TabIndex = 56;
            this.chkfolhaconsultar.UseVisualStyleBackColor = true;
            // 
            // chkfolhacadastrar
            // 
            this.chkfolhacadastrar.AutoSize = true;
            this.chkfolhacadastrar.Location = new System.Drawing.Point(419, 277);
            this.chkfolhacadastrar.Name = "chkfolhacadastrar";
            this.chkfolhacadastrar.Size = new System.Drawing.Size(15, 14);
            this.chkfolhacadastrar.TabIndex = 55;
            this.chkfolhacadastrar.UseVisualStyleBackColor = true;
            this.chkfolhacadastrar.CheckedChanged += new System.EventHandler(this.chkfolhacadastrar_CheckedChanged);
            // 
            // chkprodutoalterar
            // 
            this.chkprodutoalterar.AutoSize = true;
            this.chkprodutoalterar.Location = new System.Drawing.Point(509, 207);
            this.chkprodutoalterar.Name = "chkprodutoalterar";
            this.chkprodutoalterar.Size = new System.Drawing.Size(15, 14);
            this.chkprodutoalterar.TabIndex = 54;
            this.chkprodutoalterar.UseVisualStyleBackColor = true;
            // 
            // chkfolhadepagamento
            // 
            this.chkfolhadepagamento.AutoSize = true;
            this.chkfolhadepagamento.Location = new System.Drawing.Point(201, 277);
            this.chkfolhadepagamento.Name = "chkfolhadepagamento";
            this.chkfolhadepagamento.Size = new System.Drawing.Size(15, 14);
            this.chkfolhadepagamento.TabIndex = 53;
            this.chkfolhadepagamento.UseVisualStyleBackColor = true;
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label37.ForeColor = System.Drawing.Color.Firebrick;
            this.label37.Location = new System.Drawing.Point(23, 270);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(175, 21);
            this.label37.TabIndex = 52;
            this.label37.Text = "Folha de Pagamento";
            // 
            // chkfluxodecaixacadastrar
            // 
            this.chkfluxodecaixacadastrar.AutoSize = true;
            this.chkfluxodecaixacadastrar.Location = new System.Drawing.Point(419, 311);
            this.chkfluxodecaixacadastrar.Name = "chkfluxodecaixacadastrar";
            this.chkfluxodecaixacadastrar.Size = new System.Drawing.Size(15, 14);
            this.chkfluxodecaixacadastrar.TabIndex = 51;
            this.chkfluxodecaixacadastrar.UseVisualStyleBackColor = true;
            this.chkfluxodecaixacadastrar.CheckedChanged += new System.EventHandler(this.chkfluxodecaixacadastrar_CheckedChanged);
            // 
            // chkfornecedor
            // 
            this.chkfornecedor.AutoSize = true;
            this.chkfornecedor.Location = new System.Drawing.Point(201, 68);
            this.chkfornecedor.Name = "chkfornecedor";
            this.chkfornecedor.Size = new System.Drawing.Size(15, 14);
            this.chkfornecedor.TabIndex = 50;
            this.chkfornecedor.UseVisualStyleBackColor = true;
            // 
            // chkpedido
            // 
            this.chkpedido.AutoSize = true;
            this.chkpedido.Location = new System.Drawing.Point(201, 241);
            this.chkpedido.Name = "chkpedido";
            this.chkpedido.Size = new System.Drawing.Size(15, 14);
            this.chkpedido.TabIndex = 49;
            this.chkpedido.UseVisualStyleBackColor = true;
            // 
            // chkfluxodecaixa
            // 
            this.chkfluxodecaixa.AutoSize = true;
            this.chkfluxodecaixa.Location = new System.Drawing.Point(201, 311);
            this.chkfluxodecaixa.Name = "chkfluxodecaixa";
            this.chkfluxodecaixa.Size = new System.Drawing.Size(15, 14);
            this.chkfluxodecaixa.TabIndex = 48;
            this.chkfluxodecaixa.UseVisualStyleBackColor = true;
            // 
            // chkcardapio
            // 
            this.chkcardapio.AutoSize = true;
            this.chkcardapio.Location = new System.Drawing.Point(201, 101);
            this.chkcardapio.Name = "chkcardapio";
            this.chkcardapio.Size = new System.Drawing.Size(15, 14);
            this.chkcardapio.TabIndex = 47;
            this.chkcardapio.UseVisualStyleBackColor = true;
            // 
            // chkcliente
            // 
            this.chkcliente.AutoSize = true;
            this.chkcliente.Location = new System.Drawing.Point(201, 342);
            this.chkcliente.Name = "chkcliente";
            this.chkcliente.Size = new System.Drawing.Size(15, 14);
            this.chkcliente.TabIndex = 46;
            this.chkcliente.UseVisualStyleBackColor = true;
            // 
            // chkestoque
            // 
            this.chkestoque.AutoSize = true;
            this.chkestoque.Location = new System.Drawing.Point(201, 138);
            this.chkestoque.Name = "chkestoque";
            this.chkestoque.Size = new System.Drawing.Size(15, 14);
            this.chkestoque.TabIndex = 45;
            this.chkestoque.UseVisualStyleBackColor = true;
            // 
            // chkfuncionarios
            // 
            this.chkfuncionarios.AutoSize = true;
            this.chkfuncionarios.Location = new System.Drawing.Point(201, 176);
            this.chkfuncionarios.Name = "chkfuncionarios";
            this.chkfuncionarios.Size = new System.Drawing.Size(15, 14);
            this.chkfuncionarios.TabIndex = 44;
            this.chkfuncionarios.UseVisualStyleBackColor = true;
            // 
            // chkproduto
            // 
            this.chkproduto.AutoSize = true;
            this.chkproduto.Location = new System.Drawing.Point(201, 207);
            this.chkproduto.Name = "chkproduto";
            this.chkproduto.Size = new System.Drawing.Size(15, 14);
            this.chkproduto.TabIndex = 43;
            this.chkproduto.UseVisualStyleBackColor = true;
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label36.ForeColor = System.Drawing.Color.Firebrick;
            this.label36.Location = new System.Drawing.Point(170, 30);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(78, 19);
            this.label36.TabIndex = 42;
            this.label36.Text = "Botão de";
            // 
            // chkcardapiovinculardados
            // 
            this.chkcardapiovinculardados.AutoSize = true;
            this.chkcardapiovinculardados.Location = new System.Drawing.Point(615, 101);
            this.chkcardapiovinculardados.Name = "chkcardapiovinculardados";
            this.chkcardapiovinculardados.Size = new System.Drawing.Size(15, 14);
            this.chkcardapiovinculardados.TabIndex = 41;
            this.chkcardapiovinculardados.UseVisualStyleBackColor = true;
            // 
            // chkfornecedorvinculardados
            // 
            this.chkfornecedorvinculardados.AutoSize = true;
            this.chkfornecedorvinculardados.Location = new System.Drawing.Point(615, 68);
            this.chkfornecedorvinculardados.Name = "chkfornecedorvinculardados";
            this.chkfornecedorvinculardados.Size = new System.Drawing.Size(15, 14);
            this.chkfornecedorvinculardados.TabIndex = 40;
            this.chkfornecedorvinculardados.UseVisualStyleBackColor = true;
            // 
            // chkpedidosparafornecedor
            // 
            this.chkpedidosparafornecedor.AutoSize = true;
            this.chkpedidosparafornecedor.Location = new System.Drawing.Point(751, 68);
            this.chkpedidosparafornecedor.Name = "chkpedidosparafornecedor";
            this.chkpedidosparafornecedor.Size = new System.Drawing.Size(15, 14);
            this.chkpedidosparafornecedor.TabIndex = 39;
            this.chkpedidosparafornecedor.UseVisualStyleBackColor = true;
            this.chkpedidosparafornecedor.CheckedChanged += new System.EventHandler(this.chkpedidosparafornecedor_CheckedChanged);
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label35.ForeColor = System.Drawing.Color.Firebrick;
            this.label35.Location = new System.Drawing.Point(692, 30);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(112, 19);
            this.label35.TabIndex = 38;
            this.label35.Text = "Pedidos para";
            this.label35.Click += new System.EventHandler(this.label35_Click);
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label34.ForeColor = System.Drawing.Color.Firebrick;
            this.label34.Location = new System.Drawing.Point(559, 30);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(127, 19);
            this.label34.TabIndex = 37;
            this.label34.Text = "Vincular Dados";
            // 
            // chkfornecedorcadastrar
            // 
            this.chkfornecedorcadastrar.AutoSize = true;
            this.chkfornecedorcadastrar.Location = new System.Drawing.Point(419, 68);
            this.chkfornecedorcadastrar.Name = "chkfornecedorcadastrar";
            this.chkfornecedorcadastrar.Size = new System.Drawing.Size(15, 14);
            this.chkfornecedorcadastrar.TabIndex = 36;
            this.chkfornecedorcadastrar.UseVisualStyleBackColor = true;
            this.chkfornecedorcadastrar.CheckedChanged += new System.EventHandler(this.chkfornecedorcadastrar_CheckedChanged);
            // 
            // chkfornecedoralterar
            // 
            this.chkfornecedoralterar.AutoSize = true;
            this.chkfornecedoralterar.Location = new System.Drawing.Point(509, 68);
            this.chkfornecedoralterar.Name = "chkfornecedoralterar";
            this.chkfornecedoralterar.Size = new System.Drawing.Size(15, 14);
            this.chkfornecedoralterar.TabIndex = 35;
            this.chkfornecedoralterar.UseVisualStyleBackColor = true;
            // 
            // chkfornecedorconsultar
            // 
            this.chkfornecedorconsultar.AutoSize = true;
            this.chkfornecedorconsultar.Location = new System.Drawing.Point(323, 68);
            this.chkfornecedorconsultar.Name = "chkfornecedorconsultar";
            this.chkfornecedorconsultar.Size = new System.Drawing.Size(15, 14);
            this.chkfornecedorconsultar.TabIndex = 34;
            this.chkfornecedorconsultar.UseVisualStyleBackColor = true;
            // 
            // chkfuncionariosalterar
            // 
            this.chkfuncionariosalterar.AutoSize = true;
            this.chkfuncionariosalterar.Location = new System.Drawing.Point(509, 176);
            this.chkfuncionariosalterar.Name = "chkfuncionariosalterar";
            this.chkfuncionariosalterar.Size = new System.Drawing.Size(15, 14);
            this.chkfuncionariosalterar.TabIndex = 33;
            this.chkfuncionariosalterar.UseVisualStyleBackColor = true;
            // 
            // chkfuncionariosconsultar
            // 
            this.chkfuncionariosconsultar.AutoSize = true;
            this.chkfuncionariosconsultar.Location = new System.Drawing.Point(323, 176);
            this.chkfuncionariosconsultar.Name = "chkfuncionariosconsultar";
            this.chkfuncionariosconsultar.Size = new System.Drawing.Size(15, 14);
            this.chkfuncionariosconsultar.TabIndex = 32;
            this.chkfuncionariosconsultar.UseVisualStyleBackColor = true;
            // 
            // chkpedidocadastrar
            // 
            this.chkpedidocadastrar.AutoSize = true;
            this.chkpedidocadastrar.Location = new System.Drawing.Point(419, 241);
            this.chkpedidocadastrar.Name = "chkpedidocadastrar";
            this.chkpedidocadastrar.Size = new System.Drawing.Size(15, 14);
            this.chkpedidocadastrar.TabIndex = 30;
            this.chkpedidocadastrar.UseVisualStyleBackColor = true;
            this.chkpedidocadastrar.CheckedChanged += new System.EventHandler(this.chkpedidocadastrar_CheckedChanged);
            // 
            // chkpedidoconsultar
            // 
            this.chkpedidoconsultar.AutoSize = true;
            this.chkpedidoconsultar.Location = new System.Drawing.Point(323, 241);
            this.chkpedidoconsultar.Name = "chkpedidoconsultar";
            this.chkpedidoconsultar.Size = new System.Drawing.Size(15, 14);
            this.chkpedidoconsultar.TabIndex = 29;
            this.chkpedidoconsultar.UseVisualStyleBackColor = true;
            // 
            // chkfluxodecaixaconsultar
            // 
            this.chkfluxodecaixaconsultar.AutoSize = true;
            this.chkfluxodecaixaconsultar.Location = new System.Drawing.Point(323, 311);
            this.chkfluxodecaixaconsultar.Name = "chkfluxodecaixaconsultar";
            this.chkfluxodecaixaconsultar.Size = new System.Drawing.Size(15, 14);
            this.chkfluxodecaixaconsultar.TabIndex = 27;
            this.chkfluxodecaixaconsultar.UseVisualStyleBackColor = true;
            // 
            // chkprodutoconsultar
            // 
            this.chkprodutoconsultar.AutoSize = true;
            this.chkprodutoconsultar.Location = new System.Drawing.Point(323, 207);
            this.chkprodutoconsultar.Name = "chkprodutoconsultar";
            this.chkprodutoconsultar.Size = new System.Drawing.Size(15, 14);
            this.chkprodutoconsultar.TabIndex = 25;
            this.chkprodutoconsultar.UseVisualStyleBackColor = true;
            // 
            // chkcardapiocadastrar
            // 
            this.chkcardapiocadastrar.AutoSize = true;
            this.chkcardapiocadastrar.Location = new System.Drawing.Point(419, 101);
            this.chkcardapiocadastrar.Name = "chkcardapiocadastrar";
            this.chkcardapiocadastrar.Size = new System.Drawing.Size(15, 14);
            this.chkcardapiocadastrar.TabIndex = 24;
            this.chkcardapiocadastrar.UseVisualStyleBackColor = true;
            this.chkcardapiocadastrar.CheckedChanged += new System.EventHandler(this.chkcardapiocadastrar_CheckedChanged);
            // 
            // chkcardapioalterar
            // 
            this.chkcardapioalterar.AutoSize = true;
            this.chkcardapioalterar.Location = new System.Drawing.Point(509, 101);
            this.chkcardapioalterar.Name = "chkcardapioalterar";
            this.chkcardapioalterar.Size = new System.Drawing.Size(15, 14);
            this.chkcardapioalterar.TabIndex = 23;
            this.chkcardapioalterar.UseVisualStyleBackColor = true;
            // 
            // chkcardapioconsultar
            // 
            this.chkcardapioconsultar.AutoSize = true;
            this.chkcardapioconsultar.Location = new System.Drawing.Point(323, 101);
            this.chkcardapioconsultar.Name = "chkcardapioconsultar";
            this.chkcardapioconsultar.Size = new System.Drawing.Size(15, 14);
            this.chkcardapioconsultar.TabIndex = 22;
            this.chkcardapioconsultar.UseVisualStyleBackColor = true;
            // 
            // chkclientealterar
            // 
            this.chkclientealterar.AutoSize = true;
            this.chkclientealterar.Location = new System.Drawing.Point(509, 342);
            this.chkclientealterar.Name = "chkclientealterar";
            this.chkclientealterar.Size = new System.Drawing.Size(15, 14);
            this.chkclientealterar.TabIndex = 20;
            this.chkclientealterar.UseVisualStyleBackColor = true;
            // 
            // chkclienteconsultar
            // 
            this.chkclienteconsultar.AutoSize = true;
            this.chkclienteconsultar.Location = new System.Drawing.Point(323, 342);
            this.chkclienteconsultar.Name = "chkclienteconsultar";
            this.chkclienteconsultar.Size = new System.Drawing.Size(15, 14);
            this.chkclienteconsultar.TabIndex = 19;
            this.chkclienteconsultar.UseVisualStyleBackColor = true;
            // 
            // chkestoquecadastrar
            // 
            this.chkestoquecadastrar.AutoSize = true;
            this.chkestoquecadastrar.Location = new System.Drawing.Point(419, 138);
            this.chkestoquecadastrar.Name = "chkestoquecadastrar";
            this.chkestoquecadastrar.Size = new System.Drawing.Size(15, 14);
            this.chkestoquecadastrar.TabIndex = 18;
            this.chkestoquecadastrar.UseVisualStyleBackColor = true;
            this.chkestoquecadastrar.CheckedChanged += new System.EventHandler(this.chkestoquecadastrar_CheckedChanged);
            // 
            // chkestoquealterar
            // 
            this.chkestoquealterar.AutoSize = true;
            this.chkestoquealterar.Location = new System.Drawing.Point(509, 138);
            this.chkestoquealterar.Name = "chkestoquealterar";
            this.chkestoquealterar.Size = new System.Drawing.Size(15, 14);
            this.chkestoquealterar.TabIndex = 17;
            this.chkestoquealterar.UseVisualStyleBackColor = true;
            // 
            // chkestoqueconsultar
            // 
            this.chkestoqueconsultar.AutoSize = true;
            this.chkestoqueconsultar.Location = new System.Drawing.Point(323, 138);
            this.chkestoqueconsultar.Name = "chkestoqueconsultar";
            this.chkestoqueconsultar.Size = new System.Drawing.Size(15, 14);
            this.chkestoqueconsultar.TabIndex = 16;
            this.chkestoqueconsultar.UseVisualStyleBackColor = true;
            // 
            // chkfuncionarioscadastrar
            // 
            this.chkfuncionarioscadastrar.AutoSize = true;
            this.chkfuncionarioscadastrar.Location = new System.Drawing.Point(419, 176);
            this.chkfuncionarioscadastrar.Name = "chkfuncionarioscadastrar";
            this.chkfuncionarioscadastrar.Size = new System.Drawing.Size(15, 14);
            this.chkfuncionarioscadastrar.TabIndex = 13;
            this.chkfuncionarioscadastrar.UseVisualStyleBackColor = true;
            this.chkfuncionarioscadastrar.CheckedChanged += new System.EventHandler(this.chkfuncionarioscadastrar_CheckedChanged);
            // 
            // chkprodutocadastrar
            // 
            this.chkprodutocadastrar.AutoSize = true;
            this.chkprodutocadastrar.Location = new System.Drawing.Point(419, 207);
            this.chkprodutocadastrar.Name = "chkprodutocadastrar";
            this.chkprodutocadastrar.Size = new System.Drawing.Size(15, 14);
            this.chkprodutocadastrar.TabIndex = 12;
            this.chkprodutocadastrar.UseVisualStyleBackColor = true;
            this.chkprodutocadastrar.CheckedChanged += new System.EventHandler(this.chkprodutocadastrar_CheckedChanged);
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label33.ForeColor = System.Drawing.Color.Firebrick;
            this.label33.Location = new System.Drawing.Point(284, 30);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(81, 19);
            this.label33.TabIndex = 11;
            this.label33.Text = "Consultar";
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label32.ForeColor = System.Drawing.Color.Firebrick;
            this.label32.Location = new System.Drawing.Point(493, 30);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(60, 19);
            this.label32.TabIndex = 10;
            this.label32.Text = "Alterar";
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label31.ForeColor = System.Drawing.Color.Firebrick;
            this.label31.Location = new System.Drawing.Point(394, 30);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(85, 19);
            this.label31.TabIndex = 9;
            this.label31.Text = "Cadastrar";
            this.label31.Click += new System.EventHandler(this.label31_Click);
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label30.ForeColor = System.Drawing.Color.Firebrick;
            this.label30.Location = new System.Drawing.Point(23, 30);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(74, 19);
            this.label30.TabIndex = 8;
            this.label30.Text = "Módulos";
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label29.ForeColor = System.Drawing.Color.Firebrick;
            this.label29.Location = new System.Drawing.Point(23, 94);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(85, 21);
            this.label29.TabIndex = 7;
            this.label29.Text = "Cardápio";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.ForeColor = System.Drawing.Color.Firebrick;
            this.label28.Location = new System.Drawing.Point(23, 335);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(66, 21);
            this.label28.TabIndex = 6;
            this.label28.Text = "Cliente";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.ForeColor = System.Drawing.Color.Firebrick;
            this.label27.Location = new System.Drawing.Point(23, 131);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(73, 21);
            this.label27.TabIndex = 5;
            this.label27.Text = "Estoque";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.ForeColor = System.Drawing.Color.Firebrick;
            this.label26.Location = new System.Drawing.Point(23, 234);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(64, 21);
            this.label26.TabIndex = 4;
            this.label26.Text = "Pedido";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.ForeColor = System.Drawing.Color.Firebrick;
            this.label25.Location = new System.Drawing.Point(23, 169);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(106, 21);
            this.label25.TabIndex = 3;
            this.label25.Text = "Funcionários";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.ForeColor = System.Drawing.Color.Firebrick;
            this.label24.Location = new System.Drawing.Point(23, 304);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(124, 21);
            this.label24.TabIndex = 2;
            this.label24.Text = "Fluxo de Caixa";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.ForeColor = System.Drawing.Color.Firebrick;
            this.label23.Location = new System.Drawing.Point(23, 61);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(99, 21);
            this.label23.TabIndex = 1;
            this.label23.Text = "Fornecedor";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.ForeColor = System.Drawing.Color.Firebrick;
            this.label22.Location = new System.Drawing.Point(23, 200);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(72, 21);
            this.label22.TabIndex = 0;
            this.label22.Text = "Produto";
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = global::Lanchonha.Properties.Resources.IconesBaita_ver_02;
            this.pictureBox2.Location = new System.Drawing.Point(700, -1);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(40, 34);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox2.TabIndex = 130;
            this.pictureBox2.TabStop = false;
            this.pictureBox2.Click += new System.EventHandler(this.pictureBox2_Click);
            // 
            // frmCadastroFuncionario
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(806, 425);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label12);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frmCadastroFuncionario";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "_";
            this.Load += new System.EventHandler(this.frmCadastroFuncionario_Load);
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            this.panel10.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TextBox txtbairro;
        private System.Windows.Forms.TextBox txtsenha;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtusuario;
        private System.Windows.Forms.MaskedTextBox mktdatanascimento;
        private System.Windows.Forms.TextBox txtcidade;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.TextBox txtuf;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TextBox txtnumerocasa;
        private System.Windows.Forms.TextBox txtrua;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.MaskedTextBox mktcep;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.MaskedTextBox mktcelular;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.MaskedTextBox mkttelefone;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.ComboBox cbosexo;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.MaskedTextBox mktrg;
        private System.Windows.Forms.MaskedTextBox mktcpf;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.DateTimePicker dtpdatacadastro;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox txtcargo;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox txtnome;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.CheckBox chkfornecedorvinculardados;
        private System.Windows.Forms.CheckBox chkpedidosparafornecedor;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.CheckBox chkfornecedorcadastrar;
        private System.Windows.Forms.CheckBox chkfornecedoralterar;
        private System.Windows.Forms.CheckBox chkfornecedorconsultar;
        private System.Windows.Forms.CheckBox chkfuncionariosalterar;
        private System.Windows.Forms.CheckBox chkfuncionariosconsultar;
        private System.Windows.Forms.CheckBox chkpedidocadastrar;
        private System.Windows.Forms.CheckBox chkpedidoconsultar;
        private System.Windows.Forms.CheckBox chkprodutoconsultar;
        private System.Windows.Forms.CheckBox chkcardapiocadastrar;
        private System.Windows.Forms.CheckBox chkcardapioalterar;
        private System.Windows.Forms.CheckBox chkcardapioconsultar;
        private System.Windows.Forms.CheckBox chkclientealterar;
        private System.Windows.Forms.CheckBox chkclienteconsultar;
        private System.Windows.Forms.CheckBox chkestoquecadastrar;
        private System.Windows.Forms.CheckBox chkestoquealterar;
        private System.Windows.Forms.CheckBox chkestoqueconsultar;
        private System.Windows.Forms.CheckBox chkfuncionarioscadastrar;
        private System.Windows.Forms.CheckBox chkprodutocadastrar;
        private System.Windows.Forms.CheckBox chkcardapiovinculardados;
        private System.Windows.Forms.CheckBox chkfornecedor;
        private System.Windows.Forms.CheckBox chkpedido;
        private System.Windows.Forms.CheckBox chkfluxodecaixa;
        private System.Windows.Forms.CheckBox chkcardapio;
        private System.Windows.Forms.CheckBox chkcliente;
        private System.Windows.Forms.CheckBox chkestoque;
        private System.Windows.Forms.CheckBox chkfuncionarios;
        private System.Windows.Forms.CheckBox chkproduto;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.CheckBox chkfluxodecaixaconsultar;
        private System.Windows.Forms.CheckBox chkfluxodecaixacadastrar;
        private System.Windows.Forms.CheckBox chkfolhadepagamento;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.CheckBox chkprodutoalterar;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.TextBox txtconfirmarsenha;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.CheckBox chkfolhaalterar;
        private System.Windows.Forms.CheckBox chkfolhaconsultar;
        private System.Windows.Forms.CheckBox chkfolhacadastrar;
        private System.Windows.Forms.Panel panel16;
        private System.Windows.Forms.Panel panel15;
        private System.Windows.Forms.Panel panel14;
        private System.Windows.Forms.Panel panel13;
        private System.Windows.Forms.Panel panel12;
        private System.Windows.Forms.Panel panel11;
        private System.Windows.Forms.Panel panel10;
        private System.Windows.Forms.Panel panel17;
        private System.Windows.Forms.Panel panel9;
        private System.Windows.Forms.Panel panel8;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel1;
    }
}