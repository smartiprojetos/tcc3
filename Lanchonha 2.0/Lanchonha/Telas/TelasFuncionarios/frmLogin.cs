﻿using Lanchonha.Banco.BD.Funcionarios;
using Lanchonha.Utilitarios;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace Lanchonha.Telas.TelasFuncionarios
{
    public partial class frmLogin : Form
    {
        public frmLogin()
        {
            InitializeComponent();
           

        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Logar();
        }

        private void label4_Click(object sender, EventArgs e)
        {
            frmAlterarSenha alterarSenha = new frmAlterarSenha();
            alterarSenha.Show();
            this.Close();
        }

        private void Logar()
        {
            SHA256Cript crip = new SHA256Cript();
            Business_Funcionarios business = new Business_Funcionarios();
            FuncionarioDTO funcionario = business.Logar(txtusuario.Text, crip.Criptografar(txtsenha.Text));
            if (funcionario != null)
            {

                UserSession.UsuarioLogado = funcionario;
                frmMenu menu = new frmMenu();
                menu.Show();
                this.Hide();
            }
            else
            {
                MessageBox.Show("Dados inválidos", "Erro", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void txtsenha_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            { 

                this.Logar();
            }
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }

        private void label3_Click(object sender, EventArgs e)
        {

            
       
        }

        private void pictureBox1_Click_1(object sender, EventArgs e)
        {

        }

        private void frmLogin_Load(object sender, EventArgs e)
        {
            button1.Parent = pictureBox1;
            button1.BackColor = Color.Transparent;
            button2.Parent = pictureBox1;
            button2.BackColor = Color.Transparent;
            label1.Parent = pictureBox1;
            label1.BackColor = Color.Transparent;
            label2.Parent = pictureBox1;
            label2.BackColor = Color.Transparent;
            label3.Parent = pictureBox1;
            label3.BackColor = Color.Transparent;
            label4.Parent = pictureBox1;
            label4.BackColor = Color.Transparent;

        }
    }
}
