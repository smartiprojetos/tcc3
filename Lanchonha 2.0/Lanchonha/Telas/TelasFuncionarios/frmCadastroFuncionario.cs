﻿using Correios;
using Lanchonha.Banco.BD.Funcionarios;
using Lanchonha.Utilitarios;
using System;
using System.Drawing;
using System.Media;
using System.Windows.Forms;

namespace Lanchonha.Telas.TelasFuncionarios
{
    public partial class frmCadastroFuncionario : Form
    {
        public frmCadastroFuncionario()
        {
            InitializeComponent();
        }


        FuncionarioDTO dto = new FuncionarioDTO();
        private FuncionarioDTO DadosParaSalvar()
        {
            dto.nomecompleto = txtnome.Text.Trim(); 
            dto.cargo          = txtcargo.Text.Trim();
            dto.RG             = mktrg.Text.Trim();
            dto.Nascimento     = Convert.ToDateTime(mktdatanascimento.Text.Trim());
            dto.cpf            = mktcpf.Text.Trim();
            dto.usuario        = txtusuario.Text.Trim();
            if(txtsenha.Text == txtconfirmarsenha.Text)
            { 
              dto.senha = txtsenha.Text;
            }
            dto.Sexo           = cbosexo.SelectedItem.ToString();
            dto.Telefone       = mkttelefone.Text.Trim();
            dto.Celular        = mktcelular.Text.Trim();
            dto.CEP            = mktcep.Text.Trim();
            dto.Rua            = txtrua.Text.Trim();
            dto.Bairro         = txtbairro.Text.Trim();
            dto.numerocasa     = txtnumerocasa.Text.Trim();
            dto.Cidade         = txtcidade.Text.Trim();
            dto.Estado         = txtuf.Text.Trim();
            dto.cadastro       = dtpdatacadastro.Value;
            SalvarPermissoes();           
           return dto;
        }



        private void SalvarPermissoes()
        {
            dto.permissao_estoque = chkestoque.Checked;
            dto.permissao_atualizar_estoque = chkestoquecadastrar.Checked;
            dto.permissao_consultar_estoque = chkestoqueconsultar.Checked;
            dto.permissao_estoque_alterar = chkestoquealterar.Checked;

            dto.permissao_funcionarios = chkfuncionarios.Checked;
            dto.permissao_cadastrar_funcionarios = chkfuncionarioscadastrar.Checked;
            dto.permissao_consultar_funcionarios = chkfuncionariosconsultar.Checked;
            dto.permissao_funcionario_alterar = chkfuncionariosalterar.Checked;

            dto.permissao_fluxo_de_caixa = chkfluxodecaixa.Checked;
            dto.permissao_fluxo_de_caixa_bt = chkfluxodecaixacadastrar.Checked;
            dto.permissao_fluxo_de_caixa_consultar = chkfluxodecaixaconsultar.Checked;

            dto.permissao_folha_de_pagamento = chkfolhadepagamento.Checked;
            dto.permissao_folha_de_pagamento_gerar = chkfolhacadastrar.Checked;
            dto.permissao_folha_de_pagamento_consultar = chkfolhaconsultar.Checked;
            dto.permissao_folha_de_pagamento_alterar = chkfolhaalterar.Checked;

            dto.permissao_produtos = chkproduto.Checked;
            dto.permissao_cadastrar_novo_produto = chkprodutocadastrar.Checked;
            dto.consultar_produtos = chkprodutoconsultar.Checked;
            dto.permissao_produto_alterar = chkprodutoalterar.Checked;

            dto.permissao_pedido = chkpedido.Checked;
            dto.permissao_pedido_cadastrar = chkpedidocadastrar.Checked;
            dto.permissao_pedido_consultar = chkpedidoconsultar.Checked;

            dto.permissao_cliente = chkcliente.Checked;
            dto.permissao_controle_cliente = chkclientealterar.Checked;
            dto.permissao_consultar_cliente = chkclienteconsultar.Checked;

            dto.permissao_fornecedor = chkfornecedor.Checked;
            dto.permissao_novo_fornecedor = chkfornecedorcadastrar.Checked;
            dto.permissao_consultar_fornecedor = chkfornecedorconsultar.Checked;
            dto.permissao_pedido_fornecedor = chkpedidosparafornecedor.Checked;
            dto.permissao_fornecedor_alterar = chkfornecedoralterar.Checked;

            dto.permissao_cardapio = chkcardapio.Checked;
            dto.permissao_consultar_cardapio = chkcardapioconsultar.Checked; 
            dto.permissao_cadastro_novo_cardapio = chkcardapiocadastrar.Checked;
            dto.permissao_cardapio_alterar = chkcardapioalterar.Checked; ;
            dto.permissao_vinculardadosdecardapio_e_produto = chkcardapiovinculardados.Checked;
        }


        private void CEPDADOS()
        {
            try
            {
                APICorreios correios = new APICorreios();
                CorreiosDTO dto = correios.CEPDADOS(mktcep.Text);
                txtuf.Text = dto.UF;
                txtcidade.Text = dto.Cidade;
                txtbairro.Text = dto.Bairro;
                txtrua.Text = dto.Rua;

            }
            catch
            {
                MessageBox.Show("Por favor insira um CEP válido.", "Back's", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void label10_Click(object sender, EventArgs e)
        {

        }

        private void label9_Click(object sender, EventArgs e)
        {

        }

        private void label8_Click(object sender, EventArgs e)
        {

        }

        private void label5_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void label13_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void frmCadastroFuncionario_Load(object sender, EventArgs e)
        {
            //label1.Parent = pictureBox1;
            //label1.BackColor = Color.Transparent;
            //label1.Parent = pictureBox1;
            //label11.BackColor = Color.Transparent;
            //label4.Parent = pictureBox1;
            //label4.BackColor = Color.Transparent;
            //label2.Parent = pictureBox1;
            //label2.BackColor = Color.Transparent;
            //label3.Parent = pictureBox1;
            //label3.BackColor = Color.Transparent;
            //label21.Parent = pictureBox1;
            //label21.BackColor = Color.Transparent;
            //label6.Parent = pictureBox1;
            //label6.BackColor = Color.Transparent;
            //label8.Parent = pictureBox1;
            //label8.BackColor = Color.Transparent;
            //label9.Parent = pictureBox1;
            //label9.BackColor = Color.Transparent;
            //label10.Parent = pictureBox1;
            //label10.BackColor = Color.Transparent;
            //label14.Parent = pictureBox1;
            //label14.BackColor = Color.Transparent;
            //label15.Parent = pictureBox1;
            //label15.BackColor = Color.Transparent;
            //label19.Parent = pictureBox1;
            //label19.BackColor = Color.Transparent;
            //label20.Parent = pictureBox1;
            //label20.BackColor = Color.Transparent;
            //label17.Parent = pictureBox1;
            //label17.BackColor = Color.Transparent;
            //label16.Parent = pictureBox1;
            //label16.BackColor = Color.Transparent;
            //label18.Parent = pictureBox1;
            //label18.BackColor = Color.Transparent;


        }     

        private void rdnRH_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void rdnADM_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void txtnumerocasa_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && (e.KeyChar != ','))
            {
                e.Handled = true;
                MessageBox.Show("este campo aceita somente numero e virgula", "Back's", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            if ((e.KeyChar == ',') && ((sender as TextBox).Text.IndexOf('.') > -1))
            {
                e.Handled = true;
                MessageBox.Show("este campo aceita somente uma virgula", "Back's", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void label35_Click(object sender, EventArgs e)
        {

        }

        private void chkpedidosparafornecedor_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void button3_Click_1(object sender, EventArgs e)
        {
            CEPDADOS();
        }

        private void button1_Click_1(object sender, EventArgs e)
        {

            try
            {

                Business_Funcionarios business = new Business_Funcionarios();
                business.Salvar(DadosParaSalvar());
                MessageBox.Show("Dados salvos com sucesso", "Back's", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Back's", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void button2_Click_1(object sender, EventArgs e)
        {
            Close();
        }

        private void txtnome_TextChanged(object sender, EventArgs e)
        {
        }

        private void txtnome_KeyPress(object sender, KeyPressEventArgs e)
        {            
        }

        private void txtnome_KeyDown(object sender, KeyEventArgs e)
        {
        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            frmMenu menu = new frmMenu();
            menu.Show();
            this.Close();
        }

        private void label31_Click(object sender, EventArgs e)
        {

        }

        private void chkfluxodecaixacadastrar_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void chkfornecedorcadastrar_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void chkpedidocadastrar_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void chkcardapiocadastrar_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void chkestoquecadastrar_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void chkfuncionarioscadastrar_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void chkprodutocadastrar_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void chkfolhacadastrar_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void tabPage2_Click(object sender, EventArgs e)
        {

        }
    }
}
