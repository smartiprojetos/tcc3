﻿using Correios;
using Lanchonha.Banco.BD.Funcionarios;
using Lanchonha.Utilitarios;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Lanchonha.Telas.TelasFuncionarios
{
    public partial class frmAlterarFuncionario : Form
    {
        public frmAlterarFuncionario()
        {
            InitializeComponent();
            txtsenha.Enabled = false;
        }
        FuncionarioDTO dto = new FuncionarioDTO();
        private void CarregarDados()
        {
            Business_Funcionarios business = new Business_Funcionarios();
            List<FuncionarioDTO> dto1 = business.Buscar(int.Parse(txtid2.Text));
           

                FuncionarioDTO posisao = dto1[0];
                txtid2.Text = posisao.Id_funcionario.ToString();
                txtnome.Text = posisao.nomecompleto;
                mktcpf.Text = posisao.cpf;
                mktrg.Text = posisao.RG;
                txtcargo.Text = posisao.cargo;
                mktdiacadastro.Text = posisao.cadastro.ToString();
                mktdatanascimento.Text = posisao.Nascimento.ToString();
                txtusuario.Text = posisao.usuario;
                txtsenha.Text = posisao.senha;
                mkttelefone.Text = posisao.Telefone;
                mktcelular.Text = posisao.Celular;
                mktcep.Text = posisao.CEP;
                txtestado.Text = posisao.Estado;
                txtcidade.Text = posisao.Cidade;
                txtbairro.Text = posisao.Bairro;
                txtrua.Text = posisao.Rua;
                txtnumerocasa.Text = posisao.numerocasa;
        }
        public void LoadScreen(int id)
        {

            txtid2.Text = id.ToString();
        }

        private void CEPDADOS()
        {
            try
            {
                APICorreios correios = new APICorreios();
            CorreiosDTO dto = correios.CEPDADOS(mktcep.Text);
            txtestado.Text = dto.UF;
            txtcidade.Text = dto.Cidade;
            txtbairro.Text = dto.Bairro;
            txtrua.Text = dto.Rua;
            }
            catch
            {
                MessageBox.Show("Por favor insira um CEP válido.", "Back's", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void AlterarDados()
        {
          
             Business_Funcionarios business = new Business_Funcionarios();
        
            dto.Id_funcionario = int.Parse(txtid2.Text);
            dto.nomecompleto = txtnome.Text.Trim();
            dto.cpf  = mktcpf.Text.Trim();           
            dto.RG  = mktrg.Text.Trim();           
            dto.cargo = txtcargo.Text.Trim();          
            dto.cadastro=Convert.ToDateTime( mktdiacadastro.Text.Trim());    
            dto.Nascimento= Convert.ToDateTime(mktdatanascimento.Text.Trim()); 
            dto.usuario = txtusuario.Text.Trim();       
            dto.senha = txtsenha.Text.Trim();          
            dto.Telefone   = mkttelefone.Text.Trim();       
            dto.Celular    = mktcelular.Text.Trim();        
            dto.CEP        = mktcep.Text.Trim();            
            dto.Estado     = txtestado.Text.Trim();         
            dto.Cidade     = txtcidade.Text.Trim();        
            dto.Bairro    = txtbairro.Text.Trim();        
            dto.Rua        = txtrua.Text.Trim();       
            dto.numerocasa = txtnumerocasa.Text.Trim();
            dto.Sexo = cbosexo.SelectedItem.ToString();
            SalvarPermissoes();
            business.Alterar(dto);
        }

        private void label5_Click(object sender, EventArgs e)
        {
            this.Close();
        }


        private void label13_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

  

        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            
        }

        private void label21_Click(object sender, EventArgs e)
        {


        }
        private void frmAlterarFuncionario_Load(object sender, EventArgs e)
        {

        }

        private void txtnumerocasa_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && (e.KeyChar != ','))
            {
                e.Handled = true;
                MessageBox.Show("este campo aceita somente numero e virgula", "Back's", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            if ((e.KeyChar == ',') && ((sender as TextBox).Text.IndexOf('.') > -1))
            {
                e.Handled = true;
                MessageBox.Show("este campo aceita somente uma virgula", "Back's", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void button5_Click_1(object sender, EventArgs e)
        {
            CEPDADOS();
        }

        private void button4_Click_1(object sender, EventArgs e)
        {
            try
            {
                CarregarDados();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Back's", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void button1_Click_1(object sender, EventArgs e)
        {

            try
            {
                AlterarDados();
                MessageBox.Show("Dados alterados com sucesso", "Alterado", MessageBoxButtons.OK, MessageBoxIcon.Information);

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void SalvarPermissoes()
        {
            dto.permissao_estoque = chkestoque.Checked;
            dto.permissao_atualizar_estoque = chkestoquecadastrar.Checked;
            dto.permissao_consultar_estoque = chkestoqueconsultar.Checked;
            dto.permissao_estoque_alterar = chkestoquealterar.Checked;

            dto.permissao_funcionarios = chkfuncionarios.Checked;
            dto.permissao_cadastrar_funcionarios = chkfuncionarioscadastrar.Checked;
            dto.permissao_consultar_funcionarios = chkfuncionariosconsultar.Checked;
            dto.permissao_funcionario_alterar = chkfuncionariosalterar.Checked;

            dto.permissao_fluxo_de_caixa = chkfluxodecaixa.Checked;
            dto.permissao_fluxo_de_caixa_bt = chkfluxodecaixacadastrar.Checked;
            dto.permissao_fluxo_de_caixa_consultar = chkfluxodecaixaconsultar.Checked;

            dto.permissao_folha_de_pagamento = chkfolhadepagamento.Checked;
            dto.permissao_folha_de_pagamento_gerar = chkfolhacadastrar.Checked;
            dto.permissao_folha_de_pagamento_consultar = chkfolhaconsultar.Checked;
            dto.permissao_folha_de_pagamento_alterar = chkfolhaalterar.Checked;

            dto.permissao_produtos = chkproduto.Checked;
            dto.permissao_cadastrar_novo_produto = chkprodutocadastrar.Checked;
            dto.consultar_produtos = chkprodutoconsultar.Checked;
            dto.permissao_produto_alterar = chkprodutoalterar.Checked;

            dto.permissao_pedido = chkpedido.Checked;
            dto.permissao_pedido_cadastrar = chkpedidocadastrar.Checked;
            dto.permissao_pedido_consultar = chkpedidoconsultar.Checked;

            dto.permissao_cliente = chkcliente.Checked;
            dto.permissao_controle_cliente = chkclientealterar.Checked;
            dto.permissao_consultar_cliente = chkclienteconsultar.Checked;

            dto.permissao_fornecedor = chkfornecedor.Checked;
            dto.permissao_novo_fornecedor = chkfornecedorcadastrar.Checked;
            dto.permissao_consultar_fornecedor = chkfornecedorconsultar.Checked;
            dto.permissao_pedido_fornecedor = chkpedidosparafornecedor.Checked;
            dto.permissao_fornecedor_alterar = chkfornecedoralterar.Checked;

            dto.permissao_cardapio = chkcardapio.Checked;
            dto.permissao_consultar_cardapio = chkcardapioconsultar.Checked;
            dto.permissao_cadastro_novo_cardapio = chkcardapiocadastrar.Checked;
            dto.permissao_cardapio_alterar = chkcardapioalterar.Checked; ;
            dto.permissao_vinculardadosdecardapio_e_produto = chkcardapiovinculardados.Checked;
        }

        private void chkpedidosparafornecedor_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void label35_Click(object sender, EventArgs e)
        {

        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            frmMenu menu = new frmMenu();
            menu.Show();
            this.Close();
        }
    }
}
