﻿namespace Lanchonha.Telas.TelasFuncionarios
{
    partial class frmAlterarFuncionario
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmAlterarFuncionario));
            this.label12 = new System.Windows.Forms.Label();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.txtbairro = new System.Windows.Forms.TextBox();
            this.txtid2 = new System.Windows.Forms.TextBox();
            this.txtsenha = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtusuario = new System.Windows.Forms.TextBox();
            this.mktdiacadastro = new System.Windows.Forms.MaskedTextBox();
            this.ID = new System.Windows.Forms.Label();
            this.mktdatanascimento = new System.Windows.Forms.MaskedTextBox();
            this.txtcidade = new System.Windows.Forms.TextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.txtestado = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.txtnumerocasa = new System.Windows.Forms.TextBox();
            this.txtrua = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.button5 = new System.Windows.Forms.Button();
            this.mktcep = new System.Windows.Forms.MaskedTextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.mktcelular = new System.Windows.Forms.MaskedTextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.mkttelefone = new System.Windows.Forms.MaskedTextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.cbosexo = new System.Windows.Forms.ComboBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.mktrg = new System.Windows.Forms.MaskedTextBox();
            this.mktcpf = new System.Windows.Forms.MaskedTextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.txtcargo = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.txtnome = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.button4 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.panel10 = new System.Windows.Forms.Panel();
            this.panel9 = new System.Windows.Forms.Panel();
            this.panel8 = new System.Windows.Forms.Panel();
            this.panel7 = new System.Windows.Forms.Panel();
            this.panel6 = new System.Windows.Forms.Panel();
            this.panel5 = new System.Windows.Forms.Panel();
            this.panel4 = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.chkfolhaalterar = new System.Windows.Forms.CheckBox();
            this.chkfolhaconsultar = new System.Windows.Forms.CheckBox();
            this.chkfolhacadastrar = new System.Windows.Forms.CheckBox();
            this.chkprodutoalterar = new System.Windows.Forms.CheckBox();
            this.chkfolhadepagamento = new System.Windows.Forms.CheckBox();
            this.label37 = new System.Windows.Forms.Label();
            this.chkfluxodecaixacadastrar = new System.Windows.Forms.CheckBox();
            this.chkfornecedor = new System.Windows.Forms.CheckBox();
            this.chkpedido = new System.Windows.Forms.CheckBox();
            this.chkfluxodecaixa = new System.Windows.Forms.CheckBox();
            this.chkcardapio = new System.Windows.Forms.CheckBox();
            this.chkcliente = new System.Windows.Forms.CheckBox();
            this.chkestoque = new System.Windows.Forms.CheckBox();
            this.chkfuncionarios = new System.Windows.Forms.CheckBox();
            this.chkproduto = new System.Windows.Forms.CheckBox();
            this.label36 = new System.Windows.Forms.Label();
            this.chkcardapiovinculardados = new System.Windows.Forms.CheckBox();
            this.chkfornecedorvinculardados = new System.Windows.Forms.CheckBox();
            this.chkpedidosparafornecedor = new System.Windows.Forms.CheckBox();
            this.label35 = new System.Windows.Forms.Label();
            this.label34 = new System.Windows.Forms.Label();
            this.chkfornecedorcadastrar = new System.Windows.Forms.CheckBox();
            this.chkfornecedoralterar = new System.Windows.Forms.CheckBox();
            this.chkfornecedorconsultar = new System.Windows.Forms.CheckBox();
            this.chkfuncionariosalterar = new System.Windows.Forms.CheckBox();
            this.chkfuncionariosconsultar = new System.Windows.Forms.CheckBox();
            this.chkpedidocadastrar = new System.Windows.Forms.CheckBox();
            this.chkpedidoconsultar = new System.Windows.Forms.CheckBox();
            this.chkfluxodecaixaconsultar = new System.Windows.Forms.CheckBox();
            this.chkprodutoconsultar = new System.Windows.Forms.CheckBox();
            this.chkcardapiocadastrar = new System.Windows.Forms.CheckBox();
            this.chkcardapioalterar = new System.Windows.Forms.CheckBox();
            this.chkcardapioconsultar = new System.Windows.Forms.CheckBox();
            this.chkclientealterar = new System.Windows.Forms.CheckBox();
            this.chkclienteconsultar = new System.Windows.Forms.CheckBox();
            this.chkestoquecadastrar = new System.Windows.Forms.CheckBox();
            this.chkestoquealterar = new System.Windows.Forms.CheckBox();
            this.chkestoqueconsultar = new System.Windows.Forms.CheckBox();
            this.chkfuncionarioscadastrar = new System.Windows.Forms.CheckBox();
            this.chkprodutocadastrar = new System.Windows.Forms.CheckBox();
            this.label33 = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.SuspendLayout();
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.Color.Firebrick;
            this.label12.Location = new System.Drawing.Point(3, 7);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(248, 21);
            this.label12.TabIndex = 41;
            this.label12.Text = "Alterar dados dos Funcionarios";
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Location = new System.Drawing.Point(-3, 28);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(777, 482);
            this.tabControl1.TabIndex = 55;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.txtbairro);
            this.tabPage1.Controls.Add(this.txtid2);
            this.tabPage1.Controls.Add(this.txtsenha);
            this.tabPage1.Controls.Add(this.label3);
            this.tabPage1.Controls.Add(this.label2);
            this.tabPage1.Controls.Add(this.txtusuario);
            this.tabPage1.Controls.Add(this.mktdiacadastro);
            this.tabPage1.Controls.Add(this.ID);
            this.tabPage1.Controls.Add(this.mktdatanascimento);
            this.tabPage1.Controls.Add(this.txtcidade);
            this.tabPage1.Controls.Add(this.label20);
            this.tabPage1.Controls.Add(this.txtestado);
            this.tabPage1.Controls.Add(this.label19);
            this.tabPage1.Controls.Add(this.txtnumerocasa);
            this.tabPage1.Controls.Add(this.txtrua);
            this.tabPage1.Controls.Add(this.label18);
            this.tabPage1.Controls.Add(this.label17);
            this.tabPage1.Controls.Add(this.label16);
            this.tabPage1.Controls.Add(this.button5);
            this.tabPage1.Controls.Add(this.mktcep);
            this.tabPage1.Controls.Add(this.label15);
            this.tabPage1.Controls.Add(this.mktcelular);
            this.tabPage1.Controls.Add(this.label14);
            this.tabPage1.Controls.Add(this.mkttelefone);
            this.tabPage1.Controls.Add(this.label10);
            this.tabPage1.Controls.Add(this.cbosexo);
            this.tabPage1.Controls.Add(this.label9);
            this.tabPage1.Controls.Add(this.label8);
            this.tabPage1.Controls.Add(this.mktrg);
            this.tabPage1.Controls.Add(this.mktcpf);
            this.tabPage1.Controls.Add(this.label7);
            this.tabPage1.Controls.Add(this.label6);
            this.tabPage1.Controls.Add(this.txtcargo);
            this.tabPage1.Controls.Add(this.label11);
            this.tabPage1.Controls.Add(this.txtnome);
            this.tabPage1.Controls.Add(this.label4);
            this.tabPage1.Controls.Add(this.label1);
            this.tabPage1.Controls.Add(this.button4);
            this.tabPage1.Controls.Add(this.button1);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(769, 456);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Dados";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // txtbairro
            // 
            this.txtbairro.Location = new System.Drawing.Point(443, 250);
            this.txtbairro.MaxLength = 70;
            this.txtbairro.Name = "txtbairro";
            this.txtbairro.Size = new System.Drawing.Size(145, 20);
            this.txtbairro.TabIndex = 164;
            // 
            // txtid2
            // 
            this.txtid2.Location = new System.Drawing.Point(80, 12);
            this.txtid2.Multiline = true;
            this.txtid2.Name = "txtid2";
            this.txtid2.Size = new System.Drawing.Size(95, 19);
            this.txtid2.TabIndex = 162;
            // 
            // txtsenha
            // 
            this.txtsenha.Location = new System.Drawing.Point(95, 212);
            this.txtsenha.MaxLength = 500;
            this.txtsenha.Name = "txtsenha";
            this.txtsenha.Size = new System.Drawing.Size(239, 20);
            this.txtsenha.TabIndex = 123;
            this.txtsenha.UseSystemPasswordChar = true;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Firebrick;
            this.label3.Location = new System.Drawing.Point(30, 212);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(47, 17);
            this.label3.TabIndex = 155;
            this.label3.Text = "Senha";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Firebrick;
            this.label2.Location = new System.Drawing.Point(30, 176);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(54, 17);
            this.label2.TabIndex = 154;
            this.label2.Text = "Usuario";
            // 
            // txtusuario
            // 
            this.txtusuario.Location = new System.Drawing.Point(95, 175);
            this.txtusuario.MaxLength = 30;
            this.txtusuario.Multiline = true;
            this.txtusuario.Name = "txtusuario";
            this.txtusuario.Size = new System.Drawing.Size(239, 19);
            this.txtusuario.TabIndex = 122;
            // 
            // mktdiacadastro
            // 
            this.mktdiacadastro.Location = new System.Drawing.Point(154, 250);
            this.mktdiacadastro.Mask = "00/00/0000";
            this.mktdiacadastro.Name = "mktdiacadastro";
            this.mktdiacadastro.Size = new System.Drawing.Size(180, 20);
            this.mktdiacadastro.TabIndex = 124;
            this.mktdiacadastro.ValidatingType = typeof(System.DateTime);
            // 
            // ID
            // 
            this.ID.AutoSize = true;
            this.ID.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ID.ForeColor = System.Drawing.Color.Firebrick;
            this.ID.Location = new System.Drawing.Point(41, 13);
            this.ID.Name = "ID";
            this.ID.Size = new System.Drawing.Size(21, 17);
            this.ID.TabIndex = 153;
            this.ID.Text = "ID";
            // 
            // mktdatanascimento
            // 
            this.mktdatanascimento.Location = new System.Drawing.Point(154, 301);
            this.mktdatanascimento.Mask = "00/00/0000";
            this.mktdatanascimento.Name = "mktdatanascimento";
            this.mktdatanascimento.Size = new System.Drawing.Size(180, 20);
            this.mktdatanascimento.TabIndex = 125;
            this.mktdatanascimento.ValidatingType = typeof(System.DateTime);
            // 
            // txtcidade
            // 
            this.txtcidade.Location = new System.Drawing.Point(443, 213);
            this.txtcidade.MaxLength = 100;
            this.txtcidade.Multiline = true;
            this.txtcidade.Name = "txtcidade";
            this.txtcidade.Size = new System.Drawing.Size(145, 19);
            this.txtcidade.TabIndex = 132;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.ForeColor = System.Drawing.Color.Firebrick;
            this.label20.Location = new System.Drawing.Point(373, 213);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(57, 17);
            this.label20.TabIndex = 152;
            this.label20.Text = "Cidade";
            // 
            // txtestado
            // 
            this.txtestado.Location = new System.Drawing.Point(443, 174);
            this.txtestado.MaxLength = 2;
            this.txtestado.Multiline = true;
            this.txtestado.Name = "txtestado";
            this.txtestado.Size = new System.Drawing.Size(145, 19);
            this.txtestado.TabIndex = 131;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.ForeColor = System.Drawing.Color.Firebrick;
            this.label19.Location = new System.Drawing.Point(373, 175);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(52, 17);
            this.label19.TabIndex = 151;
            this.label19.Text = "Estado";
            // 
            // txtnumerocasa
            // 
            this.txtnumerocasa.Location = new System.Drawing.Point(443, 343);
            this.txtnumerocasa.Multiline = true;
            this.txtnumerocasa.Name = "txtnumerocasa";
            this.txtnumerocasa.Size = new System.Drawing.Size(145, 19);
            this.txtnumerocasa.TabIndex = 134;
            // 
            // txtrua
            // 
            this.txtrua.Location = new System.Drawing.Point(443, 300);
            this.txtrua.MaxLength = 100;
            this.txtrua.Multiline = true;
            this.txtrua.Name = "txtrua";
            this.txtrua.Size = new System.Drawing.Size(145, 19);
            this.txtrua.TabIndex = 133;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.ForeColor = System.Drawing.Color.Firebrick;
            this.label18.Location = new System.Drawing.Point(373, 344);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(23, 17);
            this.label18.TabIndex = 150;
            this.label18.Text = "N°";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.ForeColor = System.Drawing.Color.Firebrick;
            this.label17.Location = new System.Drawing.Point(373, 252);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(44, 17);
            this.label17.TabIndex = 149;
            this.label17.Text = "Bairro";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.ForeColor = System.Drawing.Color.Firebrick;
            this.label16.Location = new System.Drawing.Point(373, 301);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(33, 17);
            this.label16.TabIndex = 148;
            this.label16.Text = "Rua";
            // 
            // button5
            // 
            this.button5.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button5.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button5.ForeColor = System.Drawing.Color.Firebrick;
            this.button5.Location = new System.Drawing.Point(547, 123);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(56, 34);
            this.button5.TabIndex = 130;
            this.button5.Text = "Buscar";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.button5_Click_1);
            // 
            // mktcep
            // 
            this.mktcep.ForeColor = System.Drawing.Color.Firebrick;
            this.mktcep.Location = new System.Drawing.Point(443, 131);
            this.mktcep.Mask = "00000-000";
            this.mktcep.Name = "mktcep";
            this.mktcep.Size = new System.Drawing.Size(98, 20);
            this.mktcep.TabIndex = 129;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.ForeColor = System.Drawing.Color.Firebrick;
            this.label15.Location = new System.Drawing.Point(373, 132);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(34, 17);
            this.label15.TabIndex = 147;
            this.label15.Text = "CEP";
            // 
            // mktcelular
            // 
            this.mktcelular.ForeColor = System.Drawing.Color.Firebrick;
            this.mktcelular.Location = new System.Drawing.Point(443, 82);
            this.mktcelular.Mask = "(99) 99999-9999";
            this.mktcelular.Name = "mktcelular";
            this.mktcelular.Size = new System.Drawing.Size(145, 20);
            this.mktcelular.TabIndex = 128;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.Color.Firebrick;
            this.label14.Location = new System.Drawing.Point(373, 82);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(54, 17);
            this.label14.TabIndex = 146;
            this.label14.Text = "Celular";
            // 
            // mkttelefone
            // 
            this.mkttelefone.ForeColor = System.Drawing.Color.Firebrick;
            this.mkttelefone.Location = new System.Drawing.Point(443, 46);
            this.mkttelefone.Mask = "(00) 0000-0000";
            this.mkttelefone.Name = "mkttelefone";
            this.mkttelefone.Size = new System.Drawing.Size(145, 20);
            this.mkttelefone.TabIndex = 127;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.Firebrick;
            this.label10.Location = new System.Drawing.Point(373, 47);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(61, 17);
            this.label10.TabIndex = 145;
            this.label10.Text = "Telefone";
            // 
            // cbosexo
            // 
            this.cbosexo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbosexo.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbosexo.FormattingEnabled = true;
            this.cbosexo.Items.AddRange(new object[] {
            "Masculino",
            "Feminino"});
            this.cbosexo.Location = new System.Drawing.Point(79, 341);
            this.cbosexo.Name = "cbosexo";
            this.cbosexo.Size = new System.Drawing.Size(260, 21);
            this.cbosexo.TabIndex = 126;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.Firebrick;
            this.label9.Location = new System.Drawing.Point(30, 344);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(37, 17);
            this.label9.TabIndex = 144;
            this.label9.Text = "Sexo";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.Firebrick;
            this.label8.Location = new System.Drawing.Point(30, 301);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(123, 17);
            this.label8.TabIndex = 143;
            this.label8.Text = "Data Nascimento";
            // 
            // mktrg
            // 
            this.mktrg.ForeColor = System.Drawing.Color.Firebrick;
            this.mktrg.Location = new System.Drawing.Point(227, 80);
            this.mktrg.Mask = "00.000.000-0";
            this.mktrg.Name = "mktrg";
            this.mktrg.Size = new System.Drawing.Size(104, 20);
            this.mktrg.TabIndex = 120;
            // 
            // mktcpf
            // 
            this.mktcpf.ForeColor = System.Drawing.Color.Firebrick;
            this.mktcpf.Location = new System.Drawing.Point(88, 82);
            this.mktcpf.Mask = "000.000.000-00";
            this.mktcpf.Name = "mktcpf";
            this.mktcpf.Size = new System.Drawing.Size(94, 20);
            this.mktcpf.TabIndex = 119;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.Firebrick;
            this.label7.Location = new System.Drawing.Point(190, 82);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(27, 17);
            this.label7.TabIndex = 142;
            this.label7.Text = "RG";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Firebrick;
            this.label6.Location = new System.Drawing.Point(30, 252);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(127, 17);
            this.label6.TabIndex = 141;
            this.label6.Text = "Data de Cadastro";
            // 
            // txtcargo
            // 
            this.txtcargo.Location = new System.Drawing.Point(83, 132);
            this.txtcargo.MaxLength = 60;
            this.txtcargo.Multiline = true;
            this.txtcargo.Name = "txtcargo";
            this.txtcargo.Size = new System.Drawing.Size(251, 19);
            this.txtcargo.TabIndex = 121;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.Firebrick;
            this.label11.Location = new System.Drawing.Point(41, 84);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(33, 17);
            this.label11.TabIndex = 140;
            this.label11.Text = "CPF";
            // 
            // txtnome
            // 
            this.txtnome.Location = new System.Drawing.Point(88, 47);
            this.txtnome.MaxLength = 50;
            this.txtnome.Multiline = true;
            this.txtnome.Name = "txtnome";
            this.txtnome.Size = new System.Drawing.Size(243, 19);
            this.txtnome.TabIndex = 118;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Firebrick;
            this.label4.Location = new System.Drawing.Point(30, 133);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(50, 17);
            this.label4.TabIndex = 139;
            this.label4.Text = "Cargo";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Firebrick;
            this.label1.Location = new System.Drawing.Point(41, 48);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(52, 17);
            this.label1.TabIndex = 138;
            this.label1.Text = "Nome ";
            // 
            // button4
            // 
            this.button4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button4.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button4.ForeColor = System.Drawing.Color.Firebrick;
            this.button4.Location = new System.Drawing.Point(33, 403);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(113, 50);
            this.button4.TabIndex = 135;
            this.button4.Text = "Carregar Dados";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click_1);
            // 
            // button1
            // 
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.ForeColor = System.Drawing.Color.Firebrick;
            this.button1.Location = new System.Drawing.Point(475, 403);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(113, 50);
            this.button1.TabIndex = 136;
            this.button1.Text = "Alterar";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click_1);
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.panel10);
            this.tabPage2.Controls.Add(this.panel9);
            this.tabPage2.Controls.Add(this.panel8);
            this.tabPage2.Controls.Add(this.panel7);
            this.tabPage2.Controls.Add(this.panel6);
            this.tabPage2.Controls.Add(this.panel5);
            this.tabPage2.Controls.Add(this.panel4);
            this.tabPage2.Controls.Add(this.panel3);
            this.tabPage2.Controls.Add(this.panel2);
            this.tabPage2.Controls.Add(this.panel1);
            this.tabPage2.Controls.Add(this.chkfolhaalterar);
            this.tabPage2.Controls.Add(this.chkfolhaconsultar);
            this.tabPage2.Controls.Add(this.chkfolhacadastrar);
            this.tabPage2.Controls.Add(this.chkprodutoalterar);
            this.tabPage2.Controls.Add(this.chkfolhadepagamento);
            this.tabPage2.Controls.Add(this.label37);
            this.tabPage2.Controls.Add(this.chkfluxodecaixacadastrar);
            this.tabPage2.Controls.Add(this.chkfornecedor);
            this.tabPage2.Controls.Add(this.chkpedido);
            this.tabPage2.Controls.Add(this.chkfluxodecaixa);
            this.tabPage2.Controls.Add(this.chkcardapio);
            this.tabPage2.Controls.Add(this.chkcliente);
            this.tabPage2.Controls.Add(this.chkestoque);
            this.tabPage2.Controls.Add(this.chkfuncionarios);
            this.tabPage2.Controls.Add(this.chkproduto);
            this.tabPage2.Controls.Add(this.label36);
            this.tabPage2.Controls.Add(this.chkcardapiovinculardados);
            this.tabPage2.Controls.Add(this.chkfornecedorvinculardados);
            this.tabPage2.Controls.Add(this.chkpedidosparafornecedor);
            this.tabPage2.Controls.Add(this.label35);
            this.tabPage2.Controls.Add(this.label34);
            this.tabPage2.Controls.Add(this.chkfornecedorcadastrar);
            this.tabPage2.Controls.Add(this.chkfornecedoralterar);
            this.tabPage2.Controls.Add(this.chkfornecedorconsultar);
            this.tabPage2.Controls.Add(this.chkfuncionariosalterar);
            this.tabPage2.Controls.Add(this.chkfuncionariosconsultar);
            this.tabPage2.Controls.Add(this.chkpedidocadastrar);
            this.tabPage2.Controls.Add(this.chkpedidoconsultar);
            this.tabPage2.Controls.Add(this.chkfluxodecaixaconsultar);
            this.tabPage2.Controls.Add(this.chkprodutoconsultar);
            this.tabPage2.Controls.Add(this.chkcardapiocadastrar);
            this.tabPage2.Controls.Add(this.chkcardapioalterar);
            this.tabPage2.Controls.Add(this.chkcardapioconsultar);
            this.tabPage2.Controls.Add(this.chkclientealterar);
            this.tabPage2.Controls.Add(this.chkclienteconsultar);
            this.tabPage2.Controls.Add(this.chkestoquecadastrar);
            this.tabPage2.Controls.Add(this.chkestoquealterar);
            this.tabPage2.Controls.Add(this.chkestoqueconsultar);
            this.tabPage2.Controls.Add(this.chkfuncionarioscadastrar);
            this.tabPage2.Controls.Add(this.chkprodutocadastrar);
            this.tabPage2.Controls.Add(this.label33);
            this.tabPage2.Controls.Add(this.label32);
            this.tabPage2.Controls.Add(this.label31);
            this.tabPage2.Controls.Add(this.label30);
            this.tabPage2.Controls.Add(this.label29);
            this.tabPage2.Controls.Add(this.label28);
            this.tabPage2.Controls.Add(this.label27);
            this.tabPage2.Controls.Add(this.label26);
            this.tabPage2.Controls.Add(this.label25);
            this.tabPage2.Controls.Add(this.label24);
            this.tabPage2.Controls.Add(this.label23);
            this.tabPage2.Controls.Add(this.label21);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(769, 456);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Permissões";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // panel10
            // 
            this.panel10.BackColor = System.Drawing.Color.Firebrick;
            this.panel10.ForeColor = System.Drawing.Color.Firebrick;
            this.panel10.Location = new System.Drawing.Point(0, 76);
            this.panel10.Name = "panel10";
            this.panel10.Size = new System.Drawing.Size(781, 10);
            this.panel10.TabIndex = 111;
            // 
            // panel9
            // 
            this.panel9.BackColor = System.Drawing.Color.Firebrick;
            this.panel9.ForeColor = System.Drawing.Color.Firebrick;
            this.panel9.Location = new System.Drawing.Point(3, 368);
            this.panel9.Name = "panel9";
            this.panel9.Size = new System.Drawing.Size(781, 10);
            this.panel9.TabIndex = 109;
            // 
            // panel8
            // 
            this.panel8.BackColor = System.Drawing.Color.Firebrick;
            this.panel8.ForeColor = System.Drawing.Color.Firebrick;
            this.panel8.Location = new System.Drawing.Point(3, 332);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(781, 10);
            this.panel8.TabIndex = 109;
            // 
            // panel7
            // 
            this.panel7.BackColor = System.Drawing.Color.Firebrick;
            this.panel7.ForeColor = System.Drawing.Color.Firebrick;
            this.panel7.Location = new System.Drawing.Point(3, 297);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(781, 10);
            this.panel7.TabIndex = 109;
            // 
            // panel6
            // 
            this.panel6.BackColor = System.Drawing.Color.Firebrick;
            this.panel6.ForeColor = System.Drawing.Color.Firebrick;
            this.panel6.Location = new System.Drawing.Point(3, 263);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(781, 10);
            this.panel6.TabIndex = 109;
            // 
            // panel5
            // 
            this.panel5.BackColor = System.Drawing.Color.Firebrick;
            this.panel5.ForeColor = System.Drawing.Color.Firebrick;
            this.panel5.Location = new System.Drawing.Point(0, 237);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(781, 10);
            this.panel5.TabIndex = 109;
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.Firebrick;
            this.panel4.ForeColor = System.Drawing.Color.Firebrick;
            this.panel4.Location = new System.Drawing.Point(0, 204);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(781, 10);
            this.panel4.TabIndex = 110;
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.Firebrick;
            this.panel3.ForeColor = System.Drawing.Color.Firebrick;
            this.panel3.Location = new System.Drawing.Point(0, 175);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(781, 10);
            this.panel3.TabIndex = 109;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.Firebrick;
            this.panel2.ForeColor = System.Drawing.Color.Firebrick;
            this.panel2.Location = new System.Drawing.Point(3, 142);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(781, 10);
            this.panel2.TabIndex = 108;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Firebrick;
            this.panel1.ForeColor = System.Drawing.Color.Firebrick;
            this.panel1.Location = new System.Drawing.Point(0, 108);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(781, 10);
            this.panel1.TabIndex = 107;
            // 
            // chkfolhaalterar
            // 
            this.chkfolhaalterar.AutoSize = true;
            this.chkfolhaalterar.Location = new System.Drawing.Point(381, 279);
            this.chkfolhaalterar.Name = "chkfolhaalterar";
            this.chkfolhaalterar.Size = new System.Drawing.Size(15, 14);
            this.chkfolhaalterar.TabIndex = 106;
            this.chkfolhaalterar.UseVisualStyleBackColor = true;
            // 
            // chkfolhaconsultar
            // 
            this.chkfolhaconsultar.AutoSize = true;
            this.chkfolhaconsultar.Location = new System.Drawing.Point(269, 279);
            this.chkfolhaconsultar.Name = "chkfolhaconsultar";
            this.chkfolhaconsultar.Size = new System.Drawing.Size(15, 14);
            this.chkfolhaconsultar.TabIndex = 105;
            this.chkfolhaconsultar.UseVisualStyleBackColor = true;
            // 
            // chkfolhacadastrar
            // 
            this.chkfolhacadastrar.AutoSize = true;
            this.chkfolhacadastrar.Location = new System.Drawing.Point(480, 280);
            this.chkfolhacadastrar.Name = "chkfolhacadastrar";
            this.chkfolhacadastrar.Size = new System.Drawing.Size(15, 14);
            this.chkfolhacadastrar.TabIndex = 104;
            this.chkfolhacadastrar.UseVisualStyleBackColor = true;
            // 
            // chkprodutoalterar
            // 
            this.chkprodutoalterar.AutoSize = true;
            this.chkprodutoalterar.Location = new System.Drawing.Point(381, 219);
            this.chkprodutoalterar.Name = "chkprodutoalterar";
            this.chkprodutoalterar.Size = new System.Drawing.Size(15, 14);
            this.chkprodutoalterar.TabIndex = 103;
            this.chkprodutoalterar.UseVisualStyleBackColor = true;
            // 
            // chkfolhadepagamento
            // 
            this.chkfolhadepagamento.AutoSize = true;
            this.chkfolhadepagamento.Location = new System.Drawing.Point(172, 279);
            this.chkfolhadepagamento.Name = "chkfolhadepagamento";
            this.chkfolhadepagamento.Size = new System.Drawing.Size(15, 14);
            this.chkfolhadepagamento.TabIndex = 102;
            this.chkfolhadepagamento.UseVisualStyleBackColor = true;
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label37.ForeColor = System.Drawing.Color.Firebrick;
            this.label37.Location = new System.Drawing.Point(12, 277);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(146, 17);
            this.label37.TabIndex = 101;
            this.label37.Text = "Folha de Pagamento";
            // 
            // chkfluxodecaixacadastrar
            // 
            this.chkfluxodecaixacadastrar.AutoSize = true;
            this.chkfluxodecaixacadastrar.Location = new System.Drawing.Point(480, 315);
            this.chkfluxodecaixacadastrar.Name = "chkfluxodecaixacadastrar";
            this.chkfluxodecaixacadastrar.Size = new System.Drawing.Size(15, 14);
            this.chkfluxodecaixacadastrar.TabIndex = 100;
            this.chkfluxodecaixacadastrar.UseVisualStyleBackColor = true;
            // 
            // chkfornecedor
            // 
            this.chkfornecedor.AutoSize = true;
            this.chkfornecedor.Location = new System.Drawing.Point(171, 91);
            this.chkfornecedor.Name = "chkfornecedor";
            this.chkfornecedor.Size = new System.Drawing.Size(15, 14);
            this.chkfornecedor.TabIndex = 99;
            this.chkfornecedor.UseVisualStyleBackColor = true;
            // 
            // chkpedido
            // 
            this.chkpedido.AutoSize = true;
            this.chkpedido.Location = new System.Drawing.Point(171, 250);
            this.chkpedido.Name = "chkpedido";
            this.chkpedido.Size = new System.Drawing.Size(15, 14);
            this.chkpedido.TabIndex = 98;
            this.chkpedido.UseVisualStyleBackColor = true;
            // 
            // chkfluxodecaixa
            // 
            this.chkfluxodecaixa.AutoSize = true;
            this.chkfluxodecaixa.Location = new System.Drawing.Point(172, 314);
            this.chkfluxodecaixa.Name = "chkfluxodecaixa";
            this.chkfluxodecaixa.Size = new System.Drawing.Size(15, 14);
            this.chkfluxodecaixa.TabIndex = 97;
            this.chkfluxodecaixa.UseVisualStyleBackColor = true;
            // 
            // chkcardapio
            // 
            this.chkcardapio.AutoSize = true;
            this.chkcardapio.Location = new System.Drawing.Point(171, 124);
            this.chkcardapio.Name = "chkcardapio";
            this.chkcardapio.Size = new System.Drawing.Size(15, 14);
            this.chkcardapio.TabIndex = 96;
            this.chkcardapio.UseVisualStyleBackColor = true;
            // 
            // chkcliente
            // 
            this.chkcliente.AutoSize = true;
            this.chkcliente.Location = new System.Drawing.Point(172, 351);
            this.chkcliente.Name = "chkcliente";
            this.chkcliente.Size = new System.Drawing.Size(15, 14);
            this.chkcliente.TabIndex = 95;
            this.chkcliente.UseVisualStyleBackColor = true;
            // 
            // chkestoque
            // 
            this.chkestoque.AutoSize = true;
            this.chkestoque.Location = new System.Drawing.Point(170, 158);
            this.chkestoque.Name = "chkestoque";
            this.chkestoque.Size = new System.Drawing.Size(15, 14);
            this.chkestoque.TabIndex = 94;
            this.chkestoque.UseVisualStyleBackColor = true;
            // 
            // chkfuncionarios
            // 
            this.chkfuncionarios.AutoSize = true;
            this.chkfuncionarios.Location = new System.Drawing.Point(171, 191);
            this.chkfuncionarios.Name = "chkfuncionarios";
            this.chkfuncionarios.Size = new System.Drawing.Size(15, 14);
            this.chkfuncionarios.TabIndex = 93;
            this.chkfuncionarios.UseVisualStyleBackColor = true;
            // 
            // chkproduto
            // 
            this.chkproduto.AutoSize = true;
            this.chkproduto.Location = new System.Drawing.Point(172, 219);
            this.chkproduto.Name = "chkproduto";
            this.chkproduto.Size = new System.Drawing.Size(15, 14);
            this.chkproduto.TabIndex = 92;
            this.chkproduto.UseVisualStyleBackColor = true;
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label36.ForeColor = System.Drawing.Color.Firebrick;
            this.label36.Location = new System.Drawing.Point(135, 50);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(78, 19);
            this.label36.TabIndex = 91;
            this.label36.Text = "Botão de";
            // 
            // chkcardapiovinculardados
            // 
            this.chkcardapiovinculardados.AutoSize = true;
            this.chkcardapiovinculardados.Location = new System.Drawing.Point(593, 122);
            this.chkcardapiovinculardados.Name = "chkcardapiovinculardados";
            this.chkcardapiovinculardados.Size = new System.Drawing.Size(15, 14);
            this.chkcardapiovinculardados.TabIndex = 90;
            this.chkcardapiovinculardados.UseVisualStyleBackColor = true;
            // 
            // chkfornecedorvinculardados
            // 
            this.chkfornecedorvinculardados.AutoSize = true;
            this.chkfornecedorvinculardados.Location = new System.Drawing.Point(593, 90);
            this.chkfornecedorvinculardados.Name = "chkfornecedorvinculardados";
            this.chkfornecedorvinculardados.Size = new System.Drawing.Size(15, 14);
            this.chkfornecedorvinculardados.TabIndex = 89;
            this.chkfornecedorvinculardados.UseVisualStyleBackColor = true;
            // 
            // chkpedidosparafornecedor
            // 
            this.chkpedidosparafornecedor.AutoSize = true;
            this.chkpedidosparafornecedor.Location = new System.Drawing.Point(706, 90);
            this.chkpedidosparafornecedor.Name = "chkpedidosparafornecedor";
            this.chkpedidosparafornecedor.Size = new System.Drawing.Size(15, 14);
            this.chkpedidosparafornecedor.TabIndex = 88;
            this.chkpedidosparafornecedor.UseVisualStyleBackColor = true;
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label35.ForeColor = System.Drawing.Color.Firebrick;
            this.label35.Location = new System.Drawing.Point(657, 50);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(112, 19);
            this.label35.TabIndex = 87;
            this.label35.Text = "Pedidos para";
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label34.ForeColor = System.Drawing.Color.Firebrick;
            this.label34.Location = new System.Drawing.Point(530, 50);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(127, 19);
            this.label34.TabIndex = 86;
            this.label34.Text = "Vincular Dados";
            // 
            // chkfornecedorcadastrar
            // 
            this.chkfornecedorcadastrar.AutoSize = true;
            this.chkfornecedorcadastrar.Location = new System.Drawing.Point(479, 91);
            this.chkfornecedorcadastrar.Name = "chkfornecedorcadastrar";
            this.chkfornecedorcadastrar.Size = new System.Drawing.Size(15, 14);
            this.chkfornecedorcadastrar.TabIndex = 85;
            this.chkfornecedorcadastrar.UseVisualStyleBackColor = true;
            // 
            // chkfornecedoralterar
            // 
            this.chkfornecedoralterar.AutoSize = true;
            this.chkfornecedoralterar.Location = new System.Drawing.Point(380, 91);
            this.chkfornecedoralterar.Name = "chkfornecedoralterar";
            this.chkfornecedoralterar.Size = new System.Drawing.Size(15, 14);
            this.chkfornecedoralterar.TabIndex = 84;
            this.chkfornecedoralterar.UseVisualStyleBackColor = true;
            // 
            // chkfornecedorconsultar
            // 
            this.chkfornecedorconsultar.AutoSize = true;
            this.chkfornecedorconsultar.Location = new System.Drawing.Point(267, 91);
            this.chkfornecedorconsultar.Name = "chkfornecedorconsultar";
            this.chkfornecedorconsultar.Size = new System.Drawing.Size(15, 14);
            this.chkfornecedorconsultar.TabIndex = 83;
            this.chkfornecedorconsultar.UseVisualStyleBackColor = true;
            // 
            // chkfuncionariosalterar
            // 
            this.chkfuncionariosalterar.AutoSize = true;
            this.chkfuncionariosalterar.Location = new System.Drawing.Point(380, 190);
            this.chkfuncionariosalterar.Name = "chkfuncionariosalterar";
            this.chkfuncionariosalterar.Size = new System.Drawing.Size(15, 14);
            this.chkfuncionariosalterar.TabIndex = 82;
            this.chkfuncionariosalterar.UseVisualStyleBackColor = true;
            // 
            // chkfuncionariosconsultar
            // 
            this.chkfuncionariosconsultar.AutoSize = true;
            this.chkfuncionariosconsultar.Location = new System.Drawing.Point(267, 190);
            this.chkfuncionariosconsultar.Name = "chkfuncionariosconsultar";
            this.chkfuncionariosconsultar.Size = new System.Drawing.Size(15, 14);
            this.chkfuncionariosconsultar.TabIndex = 81;
            this.chkfuncionariosconsultar.UseVisualStyleBackColor = true;
            // 
            // chkpedidocadastrar
            // 
            this.chkpedidocadastrar.AutoSize = true;
            this.chkpedidocadastrar.Location = new System.Drawing.Point(480, 248);
            this.chkpedidocadastrar.Name = "chkpedidocadastrar";
            this.chkpedidocadastrar.Size = new System.Drawing.Size(15, 14);
            this.chkpedidocadastrar.TabIndex = 80;
            this.chkpedidocadastrar.UseVisualStyleBackColor = true;
            // 
            // chkpedidoconsultar
            // 
            this.chkpedidoconsultar.AutoSize = true;
            this.chkpedidoconsultar.Location = new System.Drawing.Point(268, 250);
            this.chkpedidoconsultar.Name = "chkpedidoconsultar";
            this.chkpedidoconsultar.Size = new System.Drawing.Size(15, 14);
            this.chkpedidoconsultar.TabIndex = 79;
            this.chkpedidoconsultar.UseVisualStyleBackColor = true;
            // 
            // chkfluxodecaixaconsultar
            // 
            this.chkfluxodecaixaconsultar.AutoSize = true;
            this.chkfluxodecaixaconsultar.Location = new System.Drawing.Point(269, 314);
            this.chkfluxodecaixaconsultar.Name = "chkfluxodecaixaconsultar";
            this.chkfluxodecaixaconsultar.Size = new System.Drawing.Size(15, 14);
            this.chkfluxodecaixaconsultar.TabIndex = 78;
            this.chkfluxodecaixaconsultar.UseVisualStyleBackColor = true;
            // 
            // chkprodutoconsultar
            // 
            this.chkprodutoconsultar.AutoSize = true;
            this.chkprodutoconsultar.Location = new System.Drawing.Point(267, 219);
            this.chkprodutoconsultar.Name = "chkprodutoconsultar";
            this.chkprodutoconsultar.Size = new System.Drawing.Size(15, 14);
            this.chkprodutoconsultar.TabIndex = 77;
            this.chkprodutoconsultar.UseVisualStyleBackColor = true;
            // 
            // chkcardapiocadastrar
            // 
            this.chkcardapiocadastrar.AutoSize = true;
            this.chkcardapiocadastrar.Location = new System.Drawing.Point(479, 124);
            this.chkcardapiocadastrar.Name = "chkcardapiocadastrar";
            this.chkcardapiocadastrar.Size = new System.Drawing.Size(15, 14);
            this.chkcardapiocadastrar.TabIndex = 76;
            this.chkcardapiocadastrar.UseVisualStyleBackColor = true;
            // 
            // chkcardapioalterar
            // 
            this.chkcardapioalterar.AutoSize = true;
            this.chkcardapioalterar.Location = new System.Drawing.Point(380, 122);
            this.chkcardapioalterar.Name = "chkcardapioalterar";
            this.chkcardapioalterar.Size = new System.Drawing.Size(15, 14);
            this.chkcardapioalterar.TabIndex = 75;
            this.chkcardapioalterar.UseVisualStyleBackColor = true;
            // 
            // chkcardapioconsultar
            // 
            this.chkcardapioconsultar.AutoSize = true;
            this.chkcardapioconsultar.Location = new System.Drawing.Point(267, 122);
            this.chkcardapioconsultar.Name = "chkcardapioconsultar";
            this.chkcardapioconsultar.Size = new System.Drawing.Size(15, 14);
            this.chkcardapioconsultar.TabIndex = 74;
            this.chkcardapioconsultar.UseVisualStyleBackColor = true;
            // 
            // chkclientealterar
            // 
            this.chkclientealterar.AutoSize = true;
            this.chkclientealterar.Location = new System.Drawing.Point(381, 351);
            this.chkclientealterar.Name = "chkclientealterar";
            this.chkclientealterar.Size = new System.Drawing.Size(15, 14);
            this.chkclientealterar.TabIndex = 73;
            this.chkclientealterar.UseVisualStyleBackColor = true;
            // 
            // chkclienteconsultar
            // 
            this.chkclienteconsultar.AutoSize = true;
            this.chkclienteconsultar.Location = new System.Drawing.Point(269, 351);
            this.chkclienteconsultar.Name = "chkclienteconsultar";
            this.chkclienteconsultar.Size = new System.Drawing.Size(15, 14);
            this.chkclienteconsultar.TabIndex = 72;
            this.chkclienteconsultar.UseVisualStyleBackColor = true;
            // 
            // chkestoquecadastrar
            // 
            this.chkestoquecadastrar.AutoSize = true;
            this.chkestoquecadastrar.Location = new System.Drawing.Point(478, 158);
            this.chkestoquecadastrar.Name = "chkestoquecadastrar";
            this.chkestoquecadastrar.Size = new System.Drawing.Size(15, 14);
            this.chkestoquecadastrar.TabIndex = 71;
            this.chkestoquecadastrar.UseVisualStyleBackColor = true;
            // 
            // chkestoquealterar
            // 
            this.chkestoquealterar.AutoSize = true;
            this.chkestoquealterar.Location = new System.Drawing.Point(379, 158);
            this.chkestoquealterar.Name = "chkestoquealterar";
            this.chkestoquealterar.Size = new System.Drawing.Size(15, 14);
            this.chkestoquealterar.TabIndex = 70;
            this.chkestoquealterar.UseVisualStyleBackColor = true;
            // 
            // chkestoqueconsultar
            // 
            this.chkestoqueconsultar.AutoSize = true;
            this.chkestoqueconsultar.Location = new System.Drawing.Point(267, 158);
            this.chkestoqueconsultar.Name = "chkestoqueconsultar";
            this.chkestoqueconsultar.Size = new System.Drawing.Size(15, 14);
            this.chkestoqueconsultar.TabIndex = 69;
            this.chkestoqueconsultar.UseVisualStyleBackColor = true;
            // 
            // chkfuncionarioscadastrar
            // 
            this.chkfuncionarioscadastrar.AutoSize = true;
            this.chkfuncionarioscadastrar.Location = new System.Drawing.Point(479, 191);
            this.chkfuncionarioscadastrar.Name = "chkfuncionarioscadastrar";
            this.chkfuncionarioscadastrar.Size = new System.Drawing.Size(15, 14);
            this.chkfuncionarioscadastrar.TabIndex = 68;
            this.chkfuncionarioscadastrar.UseVisualStyleBackColor = true;
            // 
            // chkprodutocadastrar
            // 
            this.chkprodutocadastrar.AutoSize = true;
            this.chkprodutocadastrar.Location = new System.Drawing.Point(480, 219);
            this.chkprodutocadastrar.Name = "chkprodutocadastrar";
            this.chkprodutocadastrar.Size = new System.Drawing.Size(15, 14);
            this.chkprodutocadastrar.TabIndex = 67;
            this.chkprodutocadastrar.UseVisualStyleBackColor = true;
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label33.ForeColor = System.Drawing.Color.Firebrick;
            this.label33.Location = new System.Drawing.Point(240, 50);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(81, 19);
            this.label33.TabIndex = 66;
            this.label33.Text = "Consultar";
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label32.ForeColor = System.Drawing.Color.Firebrick;
            this.label32.Location = new System.Drawing.Point(348, 50);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(60, 19);
            this.label32.TabIndex = 65;
            this.label32.Text = "Alterar";
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label31.ForeColor = System.Drawing.Color.Firebrick;
            this.label31.Location = new System.Drawing.Point(434, 50);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(85, 19);
            this.label31.TabIndex = 64;
            this.label31.Text = "Cadastrar";
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label30.ForeColor = System.Drawing.Color.Firebrick;
            this.label30.Location = new System.Drawing.Point(11, 50);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(74, 19);
            this.label30.TabIndex = 63;
            this.label30.Text = "Módulos";
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label29.ForeColor = System.Drawing.Color.Firebrick;
            this.label29.Location = new System.Drawing.Point(11, 122);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(71, 17);
            this.label29.TabIndex = 62;
            this.label29.Text = "Cardápio";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.ForeColor = System.Drawing.Color.Firebrick;
            this.label28.Location = new System.Drawing.Point(12, 348);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(54, 17);
            this.label28.TabIndex = 61;
            this.label28.Text = "Cliente";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.ForeColor = System.Drawing.Color.Firebrick;
            this.label27.Location = new System.Drawing.Point(11, 155);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(59, 17);
            this.label27.TabIndex = 60;
            this.label27.Text = "Estoque";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.ForeColor = System.Drawing.Color.Firebrick;
            this.label26.Location = new System.Drawing.Point(11, 248);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(54, 17);
            this.label26.TabIndex = 59;
            this.label26.Text = "Pedido";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.ForeColor = System.Drawing.Color.Firebrick;
            this.label25.Location = new System.Drawing.Point(11, 188);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(88, 17);
            this.label25.TabIndex = 58;
            this.label25.Text = "Funcionários";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.ForeColor = System.Drawing.Color.Firebrick;
            this.label24.Location = new System.Drawing.Point(12, 312);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(103, 17);
            this.label24.TabIndex = 57;
            this.label24.Text = "Fluxo de Caixa";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.ForeColor = System.Drawing.Color.Firebrick;
            this.label23.Location = new System.Drawing.Point(11, 89);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(81, 17);
            this.label23.TabIndex = 56;
            this.label23.Text = "Fornecedor";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.ForeColor = System.Drawing.Color.Firebrick;
            this.label21.Location = new System.Drawing.Point(12, 217);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(60, 17);
            this.label21.TabIndex = 55;
            this.label21.Text = "Produto";
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = global::Lanchonha.Properties.Resources.IconesBaita_ver_02;
            this.pictureBox2.Location = new System.Drawing.Point(692, -2);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(40, 40);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox2.TabIndex = 130;
            this.pictureBox2.TabStop = false;
            this.pictureBox2.Click += new System.EventHandler(this.pictureBox2_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Font = new System.Drawing.Font("Comic Sans MS", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Firebrick;
            this.label5.Location = new System.Drawing.Point(748, -2);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(26, 27);
            this.label5.TabIndex = 53;
            this.label5.Text = "X";
            this.label5.Click += new System.EventHandler(this.label5_Click);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.BackColor = System.Drawing.Color.Transparent;
            this.label13.Font = new System.Drawing.Font("Comic Sans MS", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.ForeColor = System.Drawing.Color.Firebrick;
            this.label13.Location = new System.Drawing.Point(728, -4);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(24, 27);
            this.label13.TabIndex = 54;
            this.label13.Text = "_";
            this.label13.Click += new System.EventHandler(this.label13_Click);
            // 
            // frmAlterarFuncionario
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(778, 507);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label12);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frmAlterarFuncionario";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "frmAlterarFuncionario";
            this.Load += new System.EventHandler(this.frmAlterarFuncionario_Load);
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TextBox txtbairro;
        private System.Windows.Forms.TextBox txtid2;
        private System.Windows.Forms.TextBox txtsenha;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtusuario;
        private System.Windows.Forms.MaskedTextBox mktdiacadastro;
        private System.Windows.Forms.Label ID;
        private System.Windows.Forms.MaskedTextBox mktdatanascimento;
        private System.Windows.Forms.TextBox txtcidade;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.TextBox txtestado;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TextBox txtnumerocasa;
        private System.Windows.Forms.TextBox txtrua;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.MaskedTextBox mktcep;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.MaskedTextBox mktcelular;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.MaskedTextBox mkttelefone;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.ComboBox cbosexo;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.MaskedTextBox mktrg;
        private System.Windows.Forms.MaskedTextBox mktcpf;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtcargo;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox txtnome;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.CheckBox chkprodutoalterar;
        private System.Windows.Forms.CheckBox chkfolhadepagamento;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.CheckBox chkfluxodecaixacadastrar;
        private System.Windows.Forms.CheckBox chkfornecedor;
        private System.Windows.Forms.CheckBox chkpedido;
        private System.Windows.Forms.CheckBox chkfluxodecaixa;
        private System.Windows.Forms.CheckBox chkcardapio;
        private System.Windows.Forms.CheckBox chkcliente;
        private System.Windows.Forms.CheckBox chkestoque;
        private System.Windows.Forms.CheckBox chkfuncionarios;
        private System.Windows.Forms.CheckBox chkproduto;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.CheckBox chkcardapiovinculardados;
        private System.Windows.Forms.CheckBox chkfornecedorvinculardados;
        private System.Windows.Forms.CheckBox chkpedidosparafornecedor;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.CheckBox chkfornecedorcadastrar;
        private System.Windows.Forms.CheckBox chkfornecedoralterar;
        private System.Windows.Forms.CheckBox chkfornecedorconsultar;
        private System.Windows.Forms.CheckBox chkfuncionariosalterar;
        private System.Windows.Forms.CheckBox chkfuncionariosconsultar;
        private System.Windows.Forms.CheckBox chkpedidocadastrar;
        private System.Windows.Forms.CheckBox chkpedidoconsultar;
        private System.Windows.Forms.CheckBox chkfluxodecaixaconsultar;
        private System.Windows.Forms.CheckBox chkprodutoconsultar;
        private System.Windows.Forms.CheckBox chkcardapiocadastrar;
        private System.Windows.Forms.CheckBox chkcardapioalterar;
        private System.Windows.Forms.CheckBox chkcardapioconsultar;
        private System.Windows.Forms.CheckBox chkclientealterar;
        private System.Windows.Forms.CheckBox chkclienteconsultar;
        private System.Windows.Forms.CheckBox chkestoquecadastrar;
        private System.Windows.Forms.CheckBox chkestoquealterar;
        private System.Windows.Forms.CheckBox chkestoqueconsultar;
        private System.Windows.Forms.CheckBox chkfuncionarioscadastrar;
        private System.Windows.Forms.CheckBox chkprodutocadastrar;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.CheckBox chkfolhaalterar;
        private System.Windows.Forms.CheckBox chkfolhaconsultar;
        private System.Windows.Forms.CheckBox chkfolhacadastrar;
        private System.Windows.Forms.Panel panel10;
        private System.Windows.Forms.Panel panel9;
        private System.Windows.Forms.Panel panel8;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel1;
    }
}