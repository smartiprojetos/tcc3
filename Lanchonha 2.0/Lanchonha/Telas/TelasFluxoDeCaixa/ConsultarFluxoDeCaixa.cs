﻿using Lanchonha.Banco.BD.Funcionarios;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Lanchonha.Telas.TelasFluxoDeCaixa
{
    public partial class ConsultarFluxoDeCaixa : Form
    {
        public ConsultarFluxoDeCaixa()
        {
            InitializeComponent();
            //CarregarPermissoes();
        }

        private void ConsultarFluxoDeCaixa_Load(object sender, EventArgs e)
        {

        }

        private void dtg_consultarproduto_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void label14_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void label13_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            frmMenu menu = new frmMenu();
            menu.Show();
            this.Close();
        }

        //private void CarregarPermissoes()
        //{
        //    if (UserSession.UsuarioLogado. == false)
        //    {

        //        //dtg_consultarproduto.Columns[3].Visible = false;
        //        //dtg_consultarproduto.Columns[4].Visible = false;
        //    }

        //}
    }
}
