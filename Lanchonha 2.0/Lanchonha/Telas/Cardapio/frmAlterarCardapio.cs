﻿using Lanchonha.Banco.BD.Cardapio;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Lanchonha.Telas.Cardapio
{
    public partial class frmAlterarCardapio : Form
    {
        int idfuncionario;
        public frmAlterarCardapio()
        {
            InitializeComponent();
        }

        private void frmAlterarCardapio_Load(object sender, EventArgs e)
        {

        }

        private void label14_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void label13_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        public void LoadScreen(int Id)
        {
            lblid.Text = Id.ToString();
        }

        private void CarregarDados()
        {
            Business_Cardapio cardapio = new Business_Cardapio();
            List<DTO_Cardapio> lista =cardapio.ConsultarporId(int.Parse(lblid.Text));
            DTO_Cardapio dto = lista[0];
            txtproduto.Text = dto.NomeProduto;
            txtvalor.Text = dto.Valor.ToString();
            txttipo.Text = dto.Tipo;
            idfuncionario = dto.FkFuncionario;
        }

        private void AlterarDados()
        {
            Business_Cardapio business = new Business_Cardapio();
            DTO_Cardapio dados = new DTO_Cardapio();
            dados.ID = int.Parse(lblid.Text);
            dados.NomeProduto = txtproduto.Text.Trim();
            dados.Valor = decimal.Parse(txtvalor.Text);
            dados.Tipo = txttipo.Text.Trim();
            dados.FkFuncionario = idfuncionario;
            business.Alterar(dados);
        }

        private void LimparCampos()
        {
            lblid.Text = string.Empty;
            txtproduto.Text= string.Empty;
            txtvalor.Text= string.Empty;
            txttipo.Text= string.Empty;

        }

        private void button4_Click(object sender, EventArgs e)
        {
            CarregarDados();

        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                AlterarDados();
                MessageBox.Show("Dados alterados com sucesso.", "Back's", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            LimparCampos();
        }

        private void txtvalor_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && (e.KeyChar != ','))
            {
                e.Handled = true;
                MessageBox.Show("este campo aceita somente numero e virgula", "Back's", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            if ((e.KeyChar == ',') && ((sender as TextBox).Text.IndexOf('.') > -1))
            {
                e.Handled = true;
                MessageBox.Show("este campo aceita somente uma virgula", "Back's", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            frmMenu menu = new frmMenu();
            menu.Show();
            this.Close();
        }
    }
}
