﻿using Lanchonha.Banco.BD.Cardapio;
using Lanchonha.Banco.BD.Funcionarios;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Lanchonha.Telas.Cardapio
{
    public partial class frmCadastroCardapio : Form
    {
        public frmCadastroCardapio()
        {
            InitializeComponent();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void label14_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void label13_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void SalvarDados()
        {
            int idfuncionario=UserSession.UsuarioLogado.Id_funcionario;
            Business_Cardapio business = new Business_Cardapio();
            DTO_Cardapio dto = new DTO_Cardapio();
            dto.NomeProduto = txtproduto.Text.Trim();
            dto.Tipo = txttipoproduto.Text.Trim();
            dto.Valor = decimal.Parse(txtvalor.Text);
            dto.FkFuncionario = idfuncionario;
            business.Salvar(dto);
        }

        private void ApagarDados()
        {
            txtproduto.Text = string.Empty;
            txttipoproduto.Text= string.Empty;
            txtvalor.Text= string.Empty;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                SalvarDados();
                MessageBox.Show("Dados salvos com sucesso.", "Back's", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            ApagarDados();
        }

        private void txtvalor_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && (e.KeyChar != ','))
            {
                e.Handled = true;
                MessageBox.Show("este campo aceita somente numero e virgula", "Back's", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            if ((e.KeyChar == ',') && ((sender as TextBox).Text.IndexOf('.') > -1))
            {
                e.Handled = true;
                MessageBox.Show("este campo aceita somente uma virgula", "Back's", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void frmCadastroCardapio_Load(object sender, EventArgs e)
        {

        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            frmMenu menu = new frmMenu();
            menu.Show();
            this.Close();
        }
    }
}
