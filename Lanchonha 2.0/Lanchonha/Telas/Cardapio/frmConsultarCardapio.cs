﻿using Lanchonha.Banco.BD.Cardapio;
using Lanchonha.Banco.BD.Funcionarios;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Lanchonha.Telas.Cardapio
{
    public partial class frmConsultarCardapio : Form
    {
        public frmConsultarCardapio()
        {
            InitializeComponent();
            CarregarPermissoes();
        }

        private void label5_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void label13_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void frmConsultarCardapio_Load(object sender, EventArgs e)
        {

        }

        private void CarregarPermissoes()
        {
            if (UserSession.UsuarioLogado.permissao_cardapio_alterar == false)
            {

                dgvcardapio.Columns[4].Visible = false;
                dgvcardapio.Columns[5].Visible = false;
            }

        }

        private void CarregarDados()
        {
            dgvcardapio.AutoGenerateColumns = false;
            Business_Cardapio businessCardapio = new Business_Cardapio();
            dgvcardapio.DataSource = businessCardapio.ConsultarporNome(txtproduto.Text);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            CarregarDados();
        }

        private void dgvcardapio_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if(e.ColumnIndex==4)
            {
                DTO_Cardapio dto = dgvcardapio.CurrentRow.DataBoundItem as DTO_Cardapio;
                int pk = dto.ID;
                frmAlterarCardapio alterarCardapio = new frmAlterarCardapio();
                alterarCardapio.LoadScreen(pk);
                alterarCardapio.Show();
                this.Close();
            }

            if(e.ColumnIndex==5)
            {
                DTO_Cardapio dto = dgvcardapio.CurrentRow.DataBoundItem as DTO_Cardapio;
                int pk = dto.ID;
                Business_Cardapio business = new Business_Cardapio();
                business.Remover(pk);
                CarregarDados();
            }
        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            frmMenu menu = new frmMenu();
            menu.Show();
            this.Close();
        }
    }
}
