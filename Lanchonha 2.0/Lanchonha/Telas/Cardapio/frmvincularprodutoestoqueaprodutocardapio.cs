﻿using Lanchonha.Banco.BD.CadastroProduto;
using Lanchonha.Banco.BD.Cardapio;
using Lanchonha.Banco.BD.Cardapio.ItemdpProdutonoCardapio;
using Lanchonha.Banco.BD.Estoque;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Lanchonha.Telas.Cardapio
{
    public partial class frmvincularprodutoestoqueaprodutocardapio : Form
    {
        BindingList<ItemProdutoCardapioGrid> adicionarnargrid = new BindingList<ItemProdutoCardapioGrid>();
        public frmvincularprodutoestoqueaprodutocardapio()
        {
            InitializeComponent();
            CarregarComboCardapio();
            CarregarComboEstoque();
            CarregarCombo();
        }

        private void frmvincularprodutoestoqueaprodutocardapio_Load(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        void CarregarComboCardapio()
        {
            Business_Cardapio cardapio = new Business_Cardapio();
            List<DTO_Cardapio> lista = cardapio.Listar();

            cbocardapio.ValueMember = nameof(DTO_Cardapio.ID);
            cbocardapio.DisplayMember = nameof(DTO_Cardapio.NomeProduto);
            cbocardapio.DataSource = lista;
        }

        void CarregarComboEstoque()
        {
            Business_Estoque estoque = new Business_Estoque();
            List<ViewConsultarEstoqueDTO> lista = estoque.ListarEstoqueView();

            cboestoque.ValueMember = nameof(ViewConsultarEstoqueDTO.IDestoque);
            cboestoque.DisplayMember = nameof(ViewConsultarEstoqueDTO.NomeProduto);
            cboestoque.DataSource = lista;
        }

        private void label13_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void label5_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void AdicionarnaGrid()
        {
            DTO_Cardapio cardapio = cbocardapio.SelectedItem as DTO_Cardapio;
            ViewConsultarEstoqueDTO estoque = cboestoque.SelectedItem as ViewConsultarEstoqueDTO;

            ItemProdutoCardapioGrid dto = new ItemProdutoCardapioGrid();
            dto.NomeProduto = estoque.NomeProduto;
            dto.FKidEstoque = estoque.IDestoque;
            dto.NomeProdutodoCardapio = cardapio.NomeProduto;
            dto.FkidCardapio = cardapio.ID;
            adicionarnargrid.Add(dto);
            dgvestoqueecardapio.AutoGenerateColumns = false;
            dgvestoqueecardapio.DataSource = adicionarnargrid;

        }

        private void ConfigurareCarregarGrid()
        {
            
        }

        private void button1_Click(object sender, EventArgs e)
        {
            AdicionarnaGrid();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            try
            {
                ProdutonoCardapioBusiness business = new ProdutonoCardapioBusiness();
            business.Salvar(adicionarnargrid.ToList());
                MessageBox.Show("Dados vinculados com sucesso.", "Back's", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception)
            {
                MessageBox.Show("Por favor insira os dados corretamente.", "Salvo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void listView1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void label4_Click(object sender, EventArgs e)
        {

        }
        private void CarregarCombo()
        {
            Business_Cardapio cardapio = new Business_Cardapio();
            cboItenCardapio.ValueMember = nameof(DTO_Cardapio.ID);
            cboItenCardapio.DisplayMember = nameof(DTO_Cardapio.NomeProduto);
            cboItenCardapio.DataSource = cardapio.Listar();
        }

        private void cboproduto_SelectedIndexChanged(object sender, EventArgs e)
        {
            DTO_Cardapio dto = cboItenCardapio.SelectedItem as DTO_Cardapio;
            Business_CadastroProduto business = new Business_CadastroProduto();
            List<DTO_CadastroProduto> lista = business.ConsultarItensdoCardapio(dto.ID);
            Cardápio.DisplayMember = nameof(DTO_CadastroProduto.Produto);
            Cardápio.ValueMember = nameof(DTO_CadastroProduto.ID);
            Cardápio.DataSource = lista;
        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            frmMenu menu = new frmMenu();
            menu.Show();
            this.Close();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            try { 
            dgvestoqueecardapio.Rows.RemoveAt(dgvestoqueecardapio.CurrentRow.Index);
            }
            catch(Exception)
            {
                MessageBox.Show("Não existe mais itens para serem retirados.", "Back's", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }
    }
}
