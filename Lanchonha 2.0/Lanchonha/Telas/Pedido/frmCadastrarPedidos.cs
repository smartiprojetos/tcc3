﻿using Lanchonha.Banco.BD.Cardapio;
using Lanchonha.Banco.BD.Cliente;
using Lanchonha.Banco.BD.Funcionarios;
using Lanchonha.Banco.BD.Pedido;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Lanchonha.Telas.Pedido
{
    public partial class frmCadastrarPedidos : Form
    {
        public frmCadastrarPedidos()
        {
            InitializeComponent();
            CarregarCombo();
            ConfigurarGrid();
        }

        int pk;
        decimal total = 0;
        int idfuncionario = UserSession.UsuarioLogado.Id_funcionario;
        BindingList<GriedViewPedido> listadepedido = new BindingList<GriedViewPedido>();
        int idcliente;
        string nomecliente, cpfcliente, telefonecliente;

        public void LoadScreen(string nome,string CPF, string contato, int FKcliente)
        {
            idcliente = FKcliente;
            txtnomecliente.Text = nome;
            mktcpf.Text = CPF;
            mktcontato.Text = contato;
        }

        private void label13_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            CarregarPedido();
        }

        private void label5_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void CarregarPedido()
        {
            decimal qtde = nupquantidade.Value;
            if (qtde == 0)
            {
                MessageBox.Show("Por favor preencha o campo quantidade", "Back's", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {

                if (qtde != 0)
                {
                    DTO_Cardapio dto = cboProdutos.SelectedItem as DTO_Cardapio;
                    Business_Cardapio business = new Business_Cardapio();
                    List<DTO_Cardapio> lista = business.Listar();



                    for (int i = 0; i < qtde; i++)
                    {
                        GriedViewPedido itens = new GriedViewPedido();

                        itens.idproduto = dto.ID;
                        itens.produto = dto.NomeProduto;
                        itens.quantidade = nupquantidade.Value;
                        itens.observacao = txtobs.Text;
                        itens.preco = dto.Valor;
                        listadepedido.Add(itens);
                    }

                }

                else if (qtde == 0)
                {
                    MessageBox.Show("Por favor preencha o campo quantidade", "Back's", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }
        }

        private void CarregarCombo()
        {
            Business_Cardapio business = new Business_Cardapio();
            List<DTO_Cardapio> produto = business.Listar();
            cboProdutos.DisplayMember = nameof(DTO_Cardapio.NomeProduto);
            cboProdutos.ValueMember = nameof(DTO_Cardapio.ID);
            cboProdutos.ValueMember = nameof(DTO_Cardapio.Valor);

            cboProdutos.DataSource = produto;
        }

        public void ConfigurarGrid()
        {
            dgvpedido.AutoGenerateColumns = false;
            dgvpedido.DataSource = listadepedido;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            try
            {
                DadosCliente();
                DadosPedido();
                MessageBox.Show("Dados salvos com sucesso.", "Back's", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            frmMenu menu = new frmMenu();
            menu.Show();
            this.Close();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            try
            {
                dgvpedido.Rows.RemoveAt(dgvpedido.CurrentRow.Index);
            }
            catch (Exception)
            {
                MessageBox.Show("Não existe mais itens para serem retirados.", "Back's", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            
        }

        private void DadosCliente()
        {
            if (idcliente != 0)
            {
                pk = idcliente;
            }
            else
            {
                if (txtnomecliente.Text != null || mktcpf.Text != null || mktcontato.Text != null || cboformapagamento.SelectedItem != null || txtnomecliente.Text != null)
                {
                    DTO_Cliente cliente = new DTO_Cliente();
                    cliente.Celular = mktcontato.Text.Trim();
                    cliente.CPF = mktcpf.Text.Trim();
                    cliente.NomeCompleto = txtnomecliente.Text.Trim();
                    cliente.FKIDFuncionario = idfuncionario;
                    Business_Cliente salvacliente = new Business_Cliente();
                    pk = salvacliente.Salvar(cliente);
                }
                else if (txtnomecliente.Text == null || mktcpf.Text == null || mktcontato.Text == null || cboformapagamento.SelectedItem != null || txtnomecliente.Text != null)
                {
                    MessageBox.Show("Por favor preencha todos os campos", "Back's", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }
        }

        private void DadosPedido()
        {
            for (int i = 0; i < dgvpedido.Rows.Count; ++i)
            {
                total = total + Convert.ToDecimal(dgvpedido.Rows[i].Cells[1].Value);
            }

            DTO_Pedidos dados = new DTO_Pedidos();
            dados.ID_Cliente = pk;
            dados.DataVenda = DateTime.Now;

            dados.ID_Funcionario = idfuncionario;
            dados.FormadePagamento = cboformapagamento.SelectedItem.ToString();
            GriedViewPedido cadastroProduto = listadepedido[0];

            dados.ValorPago = total;
            Business_Pedidos business = new Business_Pedidos();
            business.Salvar(dados, listadepedido.ToList());
        }
    }
}
