﻿using Lanchonha.Banco.BD.Pedido;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Lanchonha.Telas.Pedido
{
    public partial class frmConsultarPedido : Form
    {
        public frmConsultarPedido()
        {
            InitializeComponent();
        }

        private void label5_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void label13_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }
        private void CarregarDados()
        {
            dgvconsultarpedido.AutoGenerateColumns = false;
            Business_PedidoItem business = new Business_PedidoItem();
            List<ViewConsultarPedidosDTO> dados = business.Consultar(txtnomecliente.Text);
            dgvconsultarpedido.DataSource = dados;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            CarregarDados();
        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            frmMenu menu = new frmMenu();
            menu.Show();
            this.Close();
        }
    }
}
