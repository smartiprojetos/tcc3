﻿namespace Lanchonha
{
    partial class frmMenu
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmMenu));
            this.panel1 = new System.Windows.Forms.Panel();
            this.label13 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.loginToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cadastrarToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.alterarToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.consultarToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.fornecedorToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cadastrarToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.alterarToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.consultarToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.pedidoParaOFornecedorToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.consultarPedidosParaFornecedorToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.estoqueToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cadastarToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.alterarToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.consultarToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.pedidoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cadastarToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.consultarToolStripMenuItem3 = new System.Windows.Forms.ToolStripMenuItem();
            this.consultarPedidosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.clienteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.alterarToolStripMenuItem3 = new System.Windows.Forms.ToolStripMenuItem();
            this.alterarToolStripMenuItem4 = new System.Windows.Forms.ToolStripMenuItem();
            this.produtoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cadastarToolStripMenuItem3 = new System.Windows.Forms.ToolStripMenuItem();
            this.alterarToolStripMenuItem5 = new System.Windows.Forms.ToolStripMenuItem();
            this.consultarToolStripMenuItem4 = new System.Windows.Forms.ToolStripMenuItem();
            this.cardápioToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cadastarToolStripMenuItem4 = new System.Windows.Forms.ToolStripMenuItem();
            this.vincularProdutosDeEstoqueACardápioToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.consultarToolStripMenuItem5 = new System.Windows.Forms.ToolStripMenuItem();
            this.fluxoDeCaixaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.consultarToolStripMenuItem6 = new System.Windows.Forms.ToolStripMenuItem();
            this.consultarToolStripMenuItem7 = new System.Windows.Forms.ToolStripMenuItem();
            this.folhaDePagamentoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.gerarToolStripMenuItem8 = new System.Windows.Forms.ToolStripMenuItem();
            this.consultarToolStripMenuItem9 = new System.Windows.Forms.ToolStripMenuItem();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.panel1.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Firebrick;
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.label13);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(695, 25);
            this.panel1.TabIndex = 20;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.BackColor = System.Drawing.Color.Transparent;
            this.label13.Font = new System.Drawing.Font("Comic Sans MS", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.ForeColor = System.Drawing.Color.White;
            this.label13.Location = new System.Drawing.Point(631, -15);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(24, 27);
            this.label13.TabIndex = 29;
            this.label13.Text = "_";
            this.label13.Click += new System.EventHandler(this.label13_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Consolas", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(3, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(70, 22);
            this.label1.TabIndex = 6;
            this.label1.Text = "Back\'s";
            this.label1.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Comic Sans MS", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(661, -3);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(26, 27);
            this.label2.TabIndex = 12;
            this.label2.Text = "X";
            this.label2.Click += new System.EventHandler(this.label2_Click);
            // 
            // menuStrip1
            // 
            this.menuStrip1.BackColor = System.Drawing.Color.White;
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.loginToolStripMenuItem,
            this.fornecedorToolStripMenuItem,
            this.estoqueToolStripMenuItem,
            this.pedidoToolStripMenuItem,
            this.clienteToolStripMenuItem,
            this.produtoToolStripMenuItem,
            this.cardápioToolStripMenuItem,
            this.fluxoDeCaixaToolStripMenuItem,
            this.folhaDePagamentoToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 25);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(695, 24);
            this.menuStrip1.TabIndex = 21;
            this.menuStrip1.Text = "Login";
            // 
            // loginToolStripMenuItem
            // 
            this.loginToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.cadastrarToolStripMenuItem,
            this.alterarToolStripMenuItem,
            this.consultarToolStripMenuItem});
            this.loginToolStripMenuItem.ForeColor = System.Drawing.Color.Firebrick;
            this.loginToolStripMenuItem.Name = "loginToolStripMenuItem";
            this.loginToolStripMenuItem.Size = new System.Drawing.Size(82, 20);
            this.loginToolStripMenuItem.Text = "Funcionario";
            // 
            // cadastrarToolStripMenuItem
            // 
            this.cadastrarToolStripMenuItem.ForeColor = System.Drawing.Color.Firebrick;
            this.cadastrarToolStripMenuItem.Name = "cadastrarToolStripMenuItem";
            this.cadastrarToolStripMenuItem.Size = new System.Drawing.Size(190, 22);
            this.cadastrarToolStripMenuItem.Text = "Cadastrar Funcionario";
            this.cadastrarToolStripMenuItem.Click += new System.EventHandler(this.cadastrarToolStripMenuItem_Click);
            // 
            // alterarToolStripMenuItem
            // 
            this.alterarToolStripMenuItem.ForeColor = System.Drawing.Color.Firebrick;
            this.alterarToolStripMenuItem.Name = "alterarToolStripMenuItem";
            this.alterarToolStripMenuItem.Size = new System.Drawing.Size(190, 22);
            this.alterarToolStripMenuItem.Text = "Alterar";
            this.alterarToolStripMenuItem.Visible = false;
            this.alterarToolStripMenuItem.Click += new System.EventHandler(this.alterarToolStripMenuItem_Click);
            // 
            // consultarToolStripMenuItem
            // 
            this.consultarToolStripMenuItem.ForeColor = System.Drawing.Color.Firebrick;
            this.consultarToolStripMenuItem.Name = "consultarToolStripMenuItem";
            this.consultarToolStripMenuItem.Size = new System.Drawing.Size(190, 22);
            this.consultarToolStripMenuItem.Text = "Consultar";
            this.consultarToolStripMenuItem.Click += new System.EventHandler(this.consultarToolStripMenuItem_Click);
            // 
            // fornecedorToolStripMenuItem
            // 
            this.fornecedorToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.cadastrarToolStripMenuItem1,
            this.alterarToolStripMenuItem1,
            this.consultarToolStripMenuItem1,
            this.pedidoParaOFornecedorToolStripMenuItem});
            this.fornecedorToolStripMenuItem.ForeColor = System.Drawing.Color.Firebrick;
            this.fornecedorToolStripMenuItem.Name = "fornecedorToolStripMenuItem";
            this.fornecedorToolStripMenuItem.Size = new System.Drawing.Size(79, 20);
            this.fornecedorToolStripMenuItem.Text = "Fornecedor";
            // 
            // cadastrarToolStripMenuItem1
            // 
            this.cadastrarToolStripMenuItem1.ForeColor = System.Drawing.Color.Firebrick;
            this.cadastrarToolStripMenuItem1.Name = "cadastrarToolStripMenuItem1";
            this.cadastrarToolStripMenuItem1.Size = new System.Drawing.Size(210, 22);
            this.cadastrarToolStripMenuItem1.Text = "Cadastrar";
            this.cadastrarToolStripMenuItem1.Click += new System.EventHandler(this.cadastrarToolStripMenuItem1_Click);
            // 
            // alterarToolStripMenuItem1
            // 
            this.alterarToolStripMenuItem1.ForeColor = System.Drawing.Color.Firebrick;
            this.alterarToolStripMenuItem1.Name = "alterarToolStripMenuItem1";
            this.alterarToolStripMenuItem1.Size = new System.Drawing.Size(210, 22);
            this.alterarToolStripMenuItem1.Text = "Alterar";
            this.alterarToolStripMenuItem1.Visible = false;
            this.alterarToolStripMenuItem1.Click += new System.EventHandler(this.alterarToolStripMenuItem1_Click);
            // 
            // consultarToolStripMenuItem1
            // 
            this.consultarToolStripMenuItem1.ForeColor = System.Drawing.Color.Firebrick;
            this.consultarToolStripMenuItem1.Name = "consultarToolStripMenuItem1";
            this.consultarToolStripMenuItem1.Size = new System.Drawing.Size(210, 22);
            this.consultarToolStripMenuItem1.Text = "Consultar";
            this.consultarToolStripMenuItem1.Click += new System.EventHandler(this.consultarToolStripMenuItem1_Click);
            // 
            // pedidoParaOFornecedorToolStripMenuItem
            // 
            this.pedidoParaOFornecedorToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.consultarPedidosParaFornecedorToolStripMenuItem});
            this.pedidoParaOFornecedorToolStripMenuItem.ForeColor = System.Drawing.Color.Firebrick;
            this.pedidoParaOFornecedorToolStripMenuItem.Name = "pedidoParaOFornecedorToolStripMenuItem";
            this.pedidoParaOFornecedorToolStripMenuItem.Size = new System.Drawing.Size(210, 22);
            this.pedidoParaOFornecedorToolStripMenuItem.Text = "Pedido para o Fornecedor";
            this.pedidoParaOFornecedorToolStripMenuItem.Click += new System.EventHandler(this.pedidoParaOFornecedorToolStripMenuItem_Click);
            // 
            // consultarPedidosParaFornecedorToolStripMenuItem
            // 
            this.consultarPedidosParaFornecedorToolStripMenuItem.ForeColor = System.Drawing.Color.Firebrick;
            this.consultarPedidosParaFornecedorToolStripMenuItem.Name = "consultarPedidosParaFornecedorToolStripMenuItem";
            this.consultarPedidosParaFornecedorToolStripMenuItem.Size = new System.Drawing.Size(259, 22);
            this.consultarPedidosParaFornecedorToolStripMenuItem.Text = "Consultar Pedidos para Fornecedor";
            this.consultarPedidosParaFornecedorToolStripMenuItem.Click += new System.EventHandler(this.consultarPedidosParaFornecedorToolStripMenuItem_Click);
            // 
            // estoqueToolStripMenuItem
            // 
            this.estoqueToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.cadastarToolStripMenuItem,
            this.alterarToolStripMenuItem2,
            this.consultarToolStripMenuItem2});
            this.estoqueToolStripMenuItem.ForeColor = System.Drawing.Color.Firebrick;
            this.estoqueToolStripMenuItem.Name = "estoqueToolStripMenuItem";
            this.estoqueToolStripMenuItem.Size = new System.Drawing.Size(61, 20);
            this.estoqueToolStripMenuItem.Text = "Estoque";
            // 
            // cadastarToolStripMenuItem
            // 
            this.cadastarToolStripMenuItem.ForeColor = System.Drawing.Color.Firebrick;
            this.cadastarToolStripMenuItem.Name = "cadastarToolStripMenuItem";
            this.cadastarToolStripMenuItem.Size = new System.Drawing.Size(125, 22);
            this.cadastarToolStripMenuItem.Text = "Cadastar";
            this.cadastarToolStripMenuItem.Click += new System.EventHandler(this.cadastarToolStripMenuItem_Click);
            // 
            // alterarToolStripMenuItem2
            // 
            this.alterarToolStripMenuItem2.ForeColor = System.Drawing.Color.Firebrick;
            this.alterarToolStripMenuItem2.Name = "alterarToolStripMenuItem2";
            this.alterarToolStripMenuItem2.Size = new System.Drawing.Size(125, 22);
            this.alterarToolStripMenuItem2.Text = "Alterar";
            this.alterarToolStripMenuItem2.Visible = false;
            this.alterarToolStripMenuItem2.Click += new System.EventHandler(this.alterarToolStripMenuItem2_Click);
            // 
            // consultarToolStripMenuItem2
            // 
            this.consultarToolStripMenuItem2.ForeColor = System.Drawing.Color.Firebrick;
            this.consultarToolStripMenuItem2.Name = "consultarToolStripMenuItem2";
            this.consultarToolStripMenuItem2.Size = new System.Drawing.Size(125, 22);
            this.consultarToolStripMenuItem2.Text = "Consultar";
            this.consultarToolStripMenuItem2.Click += new System.EventHandler(this.consultarToolStripMenuItem2_Click);
            // 
            // pedidoToolStripMenuItem
            // 
            this.pedidoToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.cadastarToolStripMenuItem1,
            this.consultarToolStripMenuItem3,
            this.consultarPedidosToolStripMenuItem});
            this.pedidoToolStripMenuItem.ForeColor = System.Drawing.Color.Firebrick;
            this.pedidoToolStripMenuItem.Name = "pedidoToolStripMenuItem";
            this.pedidoToolStripMenuItem.Size = new System.Drawing.Size(56, 20);
            this.pedidoToolStripMenuItem.Text = "Pedido";
            // 
            // cadastarToolStripMenuItem1
            // 
            this.cadastarToolStripMenuItem1.ForeColor = System.Drawing.Color.Firebrick;
            this.cadastarToolStripMenuItem1.Name = "cadastarToolStripMenuItem1";
            this.cadastarToolStripMenuItem1.Size = new System.Drawing.Size(220, 22);
            this.cadastarToolStripMenuItem1.Text = "Cadastar";
            this.cadastarToolStripMenuItem1.Click += new System.EventHandler(this.cadastarToolStripMenuItem1_Click);
            // 
            // consultarToolStripMenuItem3
            // 
            this.consultarToolStripMenuItem3.ForeColor = System.Drawing.Color.Firebrick;
            this.consultarToolStripMenuItem3.Name = "consultarToolStripMenuItem3";
            this.consultarToolStripMenuItem3.Size = new System.Drawing.Size(220, 22);
            this.consultarToolStripMenuItem3.Text = "Consultar itens dos Pedidos";
            this.consultarToolStripMenuItem3.Click += new System.EventHandler(this.consultarToolStripMenuItem3_Click);
            // 
            // consultarPedidosToolStripMenuItem
            // 
            this.consultarPedidosToolStripMenuItem.ForeColor = System.Drawing.Color.Firebrick;
            this.consultarPedidosToolStripMenuItem.Name = "consultarPedidosToolStripMenuItem";
            this.consultarPedidosToolStripMenuItem.Size = new System.Drawing.Size(220, 22);
            this.consultarPedidosToolStripMenuItem.Text = "Consultar Pedidos";
            this.consultarPedidosToolStripMenuItem.Click += new System.EventHandler(this.consultarPedidosToolStripMenuItem_Click);
            // 
            // clienteToolStripMenuItem
            // 
            this.clienteToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.alterarToolStripMenuItem3,
            this.alterarToolStripMenuItem4});
            this.clienteToolStripMenuItem.ForeColor = System.Drawing.Color.Firebrick;
            this.clienteToolStripMenuItem.Name = "clienteToolStripMenuItem";
            this.clienteToolStripMenuItem.Size = new System.Drawing.Size(56, 20);
            this.clienteToolStripMenuItem.Text = "Cliente";
            // 
            // alterarToolStripMenuItem3
            // 
            this.alterarToolStripMenuItem3.ForeColor = System.Drawing.Color.Firebrick;
            this.alterarToolStripMenuItem3.Name = "alterarToolStripMenuItem3";
            this.alterarToolStripMenuItem3.Size = new System.Drawing.Size(125, 22);
            this.alterarToolStripMenuItem3.Text = "Alterar";
            this.alterarToolStripMenuItem3.Visible = false;
            this.alterarToolStripMenuItem3.Click += new System.EventHandler(this.alterarToolStripMenuItem3_Click);
            // 
            // alterarToolStripMenuItem4
            // 
            this.alterarToolStripMenuItem4.ForeColor = System.Drawing.Color.Firebrick;
            this.alterarToolStripMenuItem4.Name = "alterarToolStripMenuItem4";
            this.alterarToolStripMenuItem4.Size = new System.Drawing.Size(125, 22);
            this.alterarToolStripMenuItem4.Text = "Consultar";
            this.alterarToolStripMenuItem4.Click += new System.EventHandler(this.alterarToolStripMenuItem4_Click);
            // 
            // produtoToolStripMenuItem
            // 
            this.produtoToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.cadastarToolStripMenuItem3,
            this.alterarToolStripMenuItem5,
            this.consultarToolStripMenuItem4});
            this.produtoToolStripMenuItem.ForeColor = System.Drawing.Color.Firebrick;
            this.produtoToolStripMenuItem.Name = "produtoToolStripMenuItem";
            this.produtoToolStripMenuItem.Size = new System.Drawing.Size(62, 20);
            this.produtoToolStripMenuItem.Text = "Produto";
            // 
            // cadastarToolStripMenuItem3
            // 
            this.cadastarToolStripMenuItem3.ForeColor = System.Drawing.Color.Firebrick;
            this.cadastarToolStripMenuItem3.Name = "cadastarToolStripMenuItem3";
            this.cadastarToolStripMenuItem3.Size = new System.Drawing.Size(125, 22);
            this.cadastarToolStripMenuItem3.Text = "Cadastar";
            this.cadastarToolStripMenuItem3.Click += new System.EventHandler(this.cadastarToolStripMenuItem3_Click);
            // 
            // alterarToolStripMenuItem5
            // 
            this.alterarToolStripMenuItem5.ForeColor = System.Drawing.Color.Firebrick;
            this.alterarToolStripMenuItem5.Name = "alterarToolStripMenuItem5";
            this.alterarToolStripMenuItem5.Size = new System.Drawing.Size(125, 22);
            this.alterarToolStripMenuItem5.Text = "Alterar";
            this.alterarToolStripMenuItem5.Visible = false;
            this.alterarToolStripMenuItem5.Click += new System.EventHandler(this.alterarToolStripMenuItem5_Click);
            // 
            // consultarToolStripMenuItem4
            // 
            this.consultarToolStripMenuItem4.ForeColor = System.Drawing.Color.Firebrick;
            this.consultarToolStripMenuItem4.Name = "consultarToolStripMenuItem4";
            this.consultarToolStripMenuItem4.Size = new System.Drawing.Size(125, 22);
            this.consultarToolStripMenuItem4.Text = "Consultar";
            this.consultarToolStripMenuItem4.Click += new System.EventHandler(this.consultarToolStripMenuItem4_Click);
            // 
            // cardápioToolStripMenuItem
            // 
            this.cardápioToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.cadastarToolStripMenuItem4,
            this.aToolStripMenuItem,
            this.consultarToolStripMenuItem5});
            this.cardápioToolStripMenuItem.ForeColor = System.Drawing.Color.Firebrick;
            this.cardápioToolStripMenuItem.Name = "cardápioToolStripMenuItem";
            this.cardápioToolStripMenuItem.Size = new System.Drawing.Size(67, 20);
            this.cardápioToolStripMenuItem.Text = "Cardápio";
            // 
            // cadastarToolStripMenuItem4
            // 
            this.cadastarToolStripMenuItem4.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.vincularProdutosDeEstoqueACardápioToolStripMenuItem});
            this.cadastarToolStripMenuItem4.ForeColor = System.Drawing.Color.Firebrick;
            this.cadastarToolStripMenuItem4.Name = "cadastarToolStripMenuItem4";
            this.cadastarToolStripMenuItem4.Size = new System.Drawing.Size(125, 22);
            this.cadastarToolStripMenuItem4.Text = "Cadastrar";
            this.cadastarToolStripMenuItem4.Click += new System.EventHandler(this.cadastarToolStripMenuItem4_Click);
            // 
            // vincularProdutosDeEstoqueACardápioToolStripMenuItem
            // 
            this.vincularProdutosDeEstoqueACardápioToolStripMenuItem.ForeColor = System.Drawing.Color.Firebrick;
            this.vincularProdutosDeEstoqueACardápioToolStripMenuItem.Name = "vincularProdutosDeEstoqueACardápioToolStripMenuItem";
            this.vincularProdutosDeEstoqueACardápioToolStripMenuItem.Size = new System.Drawing.Size(289, 22);
            this.vincularProdutosDeEstoqueACardápioToolStripMenuItem.Text = "Vincular Produtos de Estoque a Cardápio";
            this.vincularProdutosDeEstoqueACardápioToolStripMenuItem.Click += new System.EventHandler(this.vincularProdutosDeEstoqueACardápioToolStripMenuItem_Click);
            // 
            // aToolStripMenuItem
            // 
            this.aToolStripMenuItem.ForeColor = System.Drawing.Color.Firebrick;
            this.aToolStripMenuItem.Name = "aToolStripMenuItem";
            this.aToolStripMenuItem.Size = new System.Drawing.Size(125, 22);
            this.aToolStripMenuItem.Text = "Alterar";
            this.aToolStripMenuItem.Visible = false;
            this.aToolStripMenuItem.Click += new System.EventHandler(this.aToolStripMenuItem_Click);
            // 
            // consultarToolStripMenuItem5
            // 
            this.consultarToolStripMenuItem5.ForeColor = System.Drawing.Color.Firebrick;
            this.consultarToolStripMenuItem5.Name = "consultarToolStripMenuItem5";
            this.consultarToolStripMenuItem5.Size = new System.Drawing.Size(125, 22);
            this.consultarToolStripMenuItem5.Text = "Consultar";
            this.consultarToolStripMenuItem5.Click += new System.EventHandler(this.consultarToolStripMenuItem5_Click);
            // 
            // fluxoDeCaixaToolStripMenuItem
            // 
            this.fluxoDeCaixaToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.consultarToolStripMenuItem6,
            this.consultarToolStripMenuItem7});
            this.fluxoDeCaixaToolStripMenuItem.ForeColor = System.Drawing.Color.Firebrick;
            this.fluxoDeCaixaToolStripMenuItem.Name = "fluxoDeCaixaToolStripMenuItem";
            this.fluxoDeCaixaToolStripMenuItem.Size = new System.Drawing.Size(94, 20);
            this.fluxoDeCaixaToolStripMenuItem.Text = "Fluxo de Caixa";
            // 
            // consultarToolStripMenuItem6
            // 
            this.consultarToolStripMenuItem6.ForeColor = System.Drawing.Color.Firebrick;
            this.consultarToolStripMenuItem6.Name = "consultarToolStripMenuItem6";
            this.consultarToolStripMenuItem6.Size = new System.Drawing.Size(149, 22);
            this.consultarToolStripMenuItem6.Text = "Fluxo de Caixa";
            this.consultarToolStripMenuItem6.Click += new System.EventHandler(this.consultarToolStripMenuItem6_Click);
            // 
            // consultarToolStripMenuItem7
            // 
            this.consultarToolStripMenuItem7.ForeColor = System.Drawing.Color.Firebrick;
            this.consultarToolStripMenuItem7.Name = "consultarToolStripMenuItem7";
            this.consultarToolStripMenuItem7.Size = new System.Drawing.Size(149, 22);
            this.consultarToolStripMenuItem7.Text = "Consultar";
            this.consultarToolStripMenuItem7.Visible = false;
            this.consultarToolStripMenuItem7.Click += new System.EventHandler(this.consultarToolStripMenuItem7_Click);
            // 
            // folhaDePagamentoToolStripMenuItem
            // 
            this.folhaDePagamentoToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.gerarToolStripMenuItem8,
            this.consultarToolStripMenuItem9});
            this.folhaDePagamentoToolStripMenuItem.ForeColor = System.Drawing.Color.Firebrick;
            this.folhaDePagamentoToolStripMenuItem.Name = "folhaDePagamentoToolStripMenuItem";
            this.folhaDePagamentoToolStripMenuItem.Size = new System.Drawing.Size(128, 20);
            this.folhaDePagamentoToolStripMenuItem.Text = "Folha de Pagamento";
            this.folhaDePagamentoToolStripMenuItem.Click += new System.EventHandler(this.folhaDePagamentoToolStripMenuItem_Click);
            // 
            // gerarToolStripMenuItem8
            // 
            this.gerarToolStripMenuItem8.ForeColor = System.Drawing.Color.Firebrick;
            this.gerarToolStripMenuItem8.Name = "gerarToolStripMenuItem8";
            this.gerarToolStripMenuItem8.Size = new System.Drawing.Size(125, 22);
            this.gerarToolStripMenuItem8.Text = "Gerar";
            this.gerarToolStripMenuItem8.Click += new System.EventHandler(this.consultarToolStripMenuItem8_Click);
            // 
            // consultarToolStripMenuItem9
            // 
            this.consultarToolStripMenuItem9.ForeColor = System.Drawing.Color.Firebrick;
            this.consultarToolStripMenuItem9.Name = "consultarToolStripMenuItem9";
            this.consultarToolStripMenuItem9.Size = new System.Drawing.Size(125, 22);
            this.consultarToolStripMenuItem9.Text = "Consultar";
            this.consultarToolStripMenuItem9.Click += new System.EventHandler(this.consultarToolStripMenuItem9_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox1.Image = global::Lanchonha.Properties.Resources.logout_rounded_filled;
            this.pictureBox1.Location = new System.Drawing.Point(637, 428);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(58, 39);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 22;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Click += new System.EventHandler(this.pictureBox1_Click);
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox2.Image")));
            this.pictureBox2.Location = new System.Drawing.Point(0, 12);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(695, 455);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox2.TabIndex = 23;
            this.pictureBox2.TabStop = false;
            // 
            // frmMenu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(695, 466);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.menuStrip1);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.pictureBox2);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frmMenu";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "frmMenu";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem loginToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem cadastrarToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem alterarToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem consultarToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem fornecedorToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem cadastrarToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem alterarToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem consultarToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem estoqueToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem cadastarToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem alterarToolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem consultarToolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem pedidoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem cadastarToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem consultarToolStripMenuItem3;
        private System.Windows.Forms.ToolStripMenuItem clienteToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem alterarToolStripMenuItem3;
        private System.Windows.Forms.ToolStripMenuItem alterarToolStripMenuItem4;
        private System.Windows.Forms.ToolStripMenuItem produtoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem cadastarToolStripMenuItem3;
        private System.Windows.Forms.ToolStripMenuItem alterarToolStripMenuItem5;
        private System.Windows.Forms.ToolStripMenuItem consultarToolStripMenuItem4;
        private System.Windows.Forms.ToolStripMenuItem cardápioToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem cadastarToolStripMenuItem4;
        private System.Windows.Forms.ToolStripMenuItem aToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem consultarToolStripMenuItem5;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.ToolStripMenuItem pedidoParaOFornecedorToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem fluxoDeCaixaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem folhaDePagamentoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem consultarToolStripMenuItem6;
        private System.Windows.Forms.ToolStripMenuItem consultarToolStripMenuItem7;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.ToolStripMenuItem vincularProdutosDeEstoqueACardápioToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem consultarPedidosParaFornecedorToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem gerarToolStripMenuItem8;
        private System.Windows.Forms.ToolStripMenuItem consultarToolStripMenuItem9;
        private System.Windows.Forms.ToolStripMenuItem consultarPedidosToolStripMenuItem;
        private System.Windows.Forms.PictureBox pictureBox2;
    }
}