﻿using Lanchonha.Banco.BD.Funcionarios;
using Lanchonha.Telas.CadastroProduto;
using Lanchonha.Telas.Cardapio;
using Lanchonha.Telas.Cliente;
using Lanchonha.Telas.Estoque;
using Lanchonha.Telas.FolhadePagamento;
using Lanchonha.Telas.Pedido;
using Lanchonha.Telas.TelasFluxoDeCaixa;
using Lanchonha.Telas.TelasFornecedor;
using Lanchonha.Telas.TelasFuncionarios;
using System;
using System.Windows.Forms;

namespace Lanchonha
{
    public partial class frmMenu : Form
    {
        public frmMenu()
        {
            InitializeComponent();
            VerificarPermissao();
        }
        void VerificarPermissao()
        {
            if(UserSession.UsuarioLogado.permissao_funcionarios==false)
            {
                loginToolStripMenuItem.Enabled = false;
            }
            if (UserSession.UsuarioLogado.permissao_cadastrar_funcionarios == false)
            {
                cadastrarToolStripMenuItem.Enabled = false;
            }

            if (UserSession.UsuarioLogado.permissao_cadastrar_funcionarios == false)
            {
                cadastrarToolStripMenuItem.Enabled = false;
            }

            if (UserSession.UsuarioLogado.permissao_funcionario_alterar == false)
            {
                alterarToolStripMenuItem.Enabled = false;
            }

            if (UserSession.UsuarioLogado.permissao_consultar_funcionarios == false)
            {
                consultarToolStripMenuItem.Enabled = false;
            }

            if (UserSession.UsuarioLogado.permissao_fornecedor == false)
            {
                fornecedorToolStripMenuItem.Enabled = false;
            }       
            if (UserSession.UsuarioLogado.permissao_novo_fornecedor == false)
            {
                cadastrarToolStripMenuItem1.Enabled = false;
            }
            if (UserSession.UsuarioLogado.permissao_fornecedor_alterar == false)
            {
                alterarToolStripMenuItem1.Enabled = false;
            }
            if (UserSession.UsuarioLogado.permissao_consultar_fornecedor == false)
            {
                consultarToolStripMenuItem1.Enabled = false;
            }
            if (UserSession.UsuarioLogado.permissao_pedido_fornecedor== false)
            {
                pedidoParaOFornecedorToolStripMenuItem.Enabled = false;
            }

            if (UserSession.UsuarioLogado.permissao_estoque == false)
            {
                estoqueToolStripMenuItem.Enabled = false;
            }
            if (UserSession.UsuarioLogado.permissao_atualizar_estoque== false)
            {
                cadastarToolStripMenuItem.Enabled = false;
            }
            if (UserSession.UsuarioLogado.permissao_estoque_alterar == false)
            {
                alterarToolStripMenuItem2.Enabled = false;
            }
            if (UserSession.UsuarioLogado.permissao_consultar_estoque == false)
            {
                consultarToolStripMenuItem2.Enabled = false;
            }


            if (UserSession.UsuarioLogado.permissao_pedido == false)
            {
                pedidoToolStripMenuItem.Enabled = false;
            }
            if (UserSession.UsuarioLogado.permissao_pedido_cadastrar == false)
            {
                cadastarToolStripMenuItem1.Enabled = false;
            }
            if (UserSession.UsuarioLogado.permissao_pedido_consultar == false)
            {
                consultarToolStripMenuItem3.Enabled = false;
            }
            if (UserSession.UsuarioLogado.permissao_pedido_consultar == false)
            {
                consultarPedidosToolStripMenuItem.Enabled = false;
            }


            if (UserSession.UsuarioLogado.permissao_cliente == false)
            {
                clienteToolStripMenuItem.Enabled = false;
            }
            if (UserSession.UsuarioLogado.permissao_controle_cliente == false)
            {
                alterarToolStripMenuItem3.Enabled = false;
            }
            if (UserSession.UsuarioLogado.permissao_consultar_cliente == false)
            {
                alterarToolStripMenuItem4.Enabled = false;
            }


            if (UserSession.UsuarioLogado.permissao_produtos == false)
            {
                produtoToolStripMenuItem.Enabled = false;
            }
            if (UserSession.UsuarioLogado.permissao_cadastrar_novo_produto == false)
            {
                cadastarToolStripMenuItem3.Enabled = false;
            }
            if (UserSession.UsuarioLogado.permissao_produto_alterar == false)
            {
                alterarToolStripMenuItem5.Enabled = false;
            }
            if (UserSession.UsuarioLogado.consultar_produtos == false)
            {
                consultarToolStripMenuItem4.Enabled = false;
            }


            if (UserSession.UsuarioLogado.permissao_cardapio == false)
            {
                cardápioToolStripMenuItem.Enabled = false;
            }
            if (UserSession.UsuarioLogado.permissao_cadastro_novo_cardapio == false)
            {
                cadastarToolStripMenuItem4.Enabled = false;
            }
            if (UserSession.UsuarioLogado.permissao_cardapio_alterar == false)
            {
                aToolStripMenuItem.Enabled = false;
            }
            if (UserSession.UsuarioLogado.permissao_consultar_cardapio == false)
            {
                consultarToolStripMenuItem5.Enabled = false;
            }
            if(UserSession.UsuarioLogado.permissao_vinculardadosdecardapio_e_produto == false)
            {
                vincularProdutosDeEstoqueACardápioToolStripMenuItem.Enabled = false;
            }


            if (UserSession.UsuarioLogado.permissao_fluxo_de_caixa == false)
            {
                fluxoDeCaixaToolStripMenuItem.Enabled = false;
            }
            if (UserSession.UsuarioLogado.permissao_fluxo_de_caixa_bt == false)
            {
                consultarToolStripMenuItem6.Enabled = false;
            }
            if (UserSession.UsuarioLogado.permissao_fluxo_de_caixa_consultar == false)
            {
                consultarToolStripMenuItem7.Enabled = false;
            }

            if (UserSession.UsuarioLogado.permissao_folha_de_pagamento == false)
            {
                folhaDePagamentoToolStripMenuItem.Enabled = false;
            }
            if (UserSession.UsuarioLogado.permissao_folha_de_pagamento_gerar == false)
            {
                gerarToolStripMenuItem8.Enabled = false;
            }
            if (UserSession.UsuarioLogado.permissao_folha_de_pagamento_consultar == false)
            {
                consultarToolStripMenuItem9.Enabled = false;
            }
            //if (UserSession.UsuarioLogado.permissao_folha_de_pagamento_alterar == false)
            //{
            //    consultarToolStripMenuItem9.Enabled = false;
            //}


        }

        private void cadastrarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmCadastroFuncionario funcionario = new frmCadastroFuncionario();
            funcionario.Show();
            this.Close();
        }

        private void alterarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmAlterarFuncionario Alterar = new frmAlterarFuncionario();
            Alterar.Show();
            this.Close();
        }

        private void consultarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmConsultarFuncionario Consultar = new frmConsultarFuncionario();
            Consultar.Show();
            this.Close();
        }

        private void loginToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            frmLogin Login = new frmLogin();
            Login.Show();
            this.Close();
        }

        private void cadastrarToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            frmcadastrofornecedor cadastrofornecedor = new frmcadastrofornecedor();
            cadastrofornecedor.Show();
            this.Close();
        }

        private void alterarToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            frmAlterarDadosdeFornecedor AlterarDadosdeFornecedor = new frmAlterarDadosdeFornecedor();
            AlterarDadosdeFornecedor.Show();
            this.Close();
        }

        private void consultarToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            frmConsultarFornecedor ConsultarFornecedor = new frmConsultarFornecedor();
            ConsultarFornecedor.Show();
            this.Close();
        }

        private void pedidoParaOFornecedorToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmPedidoFornecedor PedidoFornecedor = new frmPedidoFornecedor();
            PedidoFornecedor.Show();
            this.Close();
        }

        private void cadastarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmCadastroEstoque cadastroEstoque = new frmCadastroEstoque();
            cadastroEstoque.Show();
            this.Close();
        }

        private void alterarToolStripMenuItem2_Click(object sender, EventArgs e)
        {
            frmAlterarEstoque alterarEstoque = new frmAlterarEstoque();
            alterarEstoque.Show();
            this.Close();
        }

        private void consultarToolStripMenuItem2_Click(object sender, EventArgs e)
        {
            frmConsultarEstoque consultarEstoque = new frmConsultarEstoque();
            consultarEstoque.Show();
            this.Close();
        }

        private void cadastarToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            frmCadastrarPedidos cadastrarPedidos = new frmCadastrarPedidos();
            cadastrarPedidos.Show();
            this.Close();
        }

        private void consultarToolStripMenuItem3_Click(object sender, EventArgs e)
        {
            frmConsultarPedido consultarPedido = new frmConsultarPedido();
            consultarPedido.Show();
            this.Close();
        }

        private void cadastarToolStripMenuItem2_Click(object sender, EventArgs e)
        {
            frmCadastroCliente cadastroCliente = new frmCadastroCliente();
            cadastroCliente.Show();
            this.Close();
        }

        private void alterarToolStripMenuItem3_Click(object sender, EventArgs e)
        {
            frmAlterarCliente alterarCliente = new frmAlterarCliente();
            alterarCliente.Show();
            this.Close();
        }

        private void alterarToolStripMenuItem4_Click(object sender, EventArgs e)
        {
            frmConsultarCliente consultarCliente = new frmConsultarCliente();
            consultarCliente.Show();
            this.Close();
        }

        private void cadastarToolStripMenuItem3_Click(object sender, EventArgs e)
        {
            frmCadastrarProdutos cadastroProduto = new frmCadastrarProdutos();
            cadastroProduto.Show();
            this.Close();
        }

        private void alterarToolStripMenuItem5_Click(object sender, EventArgs e)
        {
            FrmAlterarProduto alterarCliente = new FrmAlterarProduto();
            alterarCliente.Show();
            this.Close();
        }

        private void consultarToolStripMenuItem4_Click(object sender, EventArgs e)
        {
            frmConsultarProduto consultarProduto = new frmConsultarProduto();
            consultarProduto.Show();
            this.Close();
        }

        private void cadastarToolStripMenuItem4_Click(object sender, EventArgs e)
        {
            frmCadastroCardapio cadastroCardapio = new frmCadastroCardapio();
            cadastroCardapio.Show();
            this.Close();
        }

        private void aToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmAlterarCardapio alterarCardapio = new frmAlterarCardapio();
            alterarCardapio.Show();
            this.Close();
        }

        private void consultarToolStripMenuItem5_Click(object sender, EventArgs e)
        {
            frmConsultarCardapio consultarCardapio = new frmConsultarCardapio();
            consultarCardapio.Show();
            this.Close();
        }

        private void label2_Click(object sender, EventArgs e)
        {
            DialogResult re = MessageBox.Show("Deseja mesmo sair?",
                                 "Sair",
                                 MessageBoxButtons.OKCancel,
                                 MessageBoxIcon.Question);
            if (re == DialogResult.OK)
            {
                Application.Exit();
            }
        }

        private void label13_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void consultarToolStripMenuItem6_Click(object sender, EventArgs e)
        {
            FluxoDeCaixa fluxoDeCaixa = new FluxoDeCaixa();
            fluxoDeCaixa.Show();
            this.Close();

        }

        private void consultarToolStripMenuItem7_Click(object sender, EventArgs e)
        {
            ConsultarFluxoDeCaixa consultarFluxo = new ConsultarFluxoDeCaixa();
            consultarFluxo.Show();
            this.Close();
        }

        private void folhaDePagamentoToolStripMenuItem_Click(object sender, EventArgs e)
        {
           
        }

        private void Logoff()
        {
            frmLogin menu = new frmLogin();
            menu.Show();
            this.Close();

        }

        private void label3_Click(object sender, EventArgs e)
        {
            
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            Logoff();
        }

        private void vincularProdutosDeEstoqueACardápioToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmvincularprodutoestoqueaprodutocardapio frmvincularprodutoestoqueaprodutocardapio = new frmvincularprodutoestoqueaprodutocardapio();
            frmvincularprodutoestoqueaprodutocardapio.Show();
            this.Close();
        }

        private void consultarPedidosParaFornecedorToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmConsultarPedidoFornecedor frmConsultarPedido = new FrmConsultarPedidoFornecedor();
            frmConsultarPedido.Show();
            this.Close();
        }

        private void consultarToolStripMenuItem8_Click(object sender, EventArgs e)
        {

            frmFolhadePagamento folhaDePagamento = new frmFolhadePagamento();
            folhaDePagamento.Show();
            this.Close();
        }

        private void consultarToolStripMenuItem9_Click(object sender, EventArgs e)
        {

            frmConsultarFolhadePagamento consultarfolha = new frmConsultarFolhadePagamento();
            consultarfolha.Show();
            this.Close();
        }

        private void consultarPedidosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmConsultarPedidos consultarPedidos = new frmConsultarPedidos();
            consultarPedidos.Show();
            this.Close();
        }
    }
}
