﻿using Lanchonha.Utilitarios;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lanchonha.Banco.BD.CadastroProduto
{
    class Business_CadastroProduto
    {
        public int Salvar (DTO_CadastroProduto dto)
        {
            exReg reg = new exReg();
            reg.ValidarNome(dto.Produto);
            if (String.IsNullOrEmpty(dto.Produto) || dto.Produto.Trim().Length == 0)
            {
                throw new ArgumentException("O campo Produto só tem espaços.");
            }
            if (dto.Produto == string.Empty)
            {
                throw new ArgumentException("Produto é obrigatório");
            }

            if (dto.Preco == 0)
            {
                throw new ArgumentException("Preço é obrigatório");
            }


            Database_CadastroProduto business = new Database_CadastroProduto();
            return business.Salvar(dto);
        }

        public List<DTO_CadastroProduto> Listar()
        {
            Database_CadastroProduto business = new  Database_CadastroProduto();
            return business.Listar();
        }

        public void Remover (int id)
        {
            if(id==0)
            {
                throw new ArgumentException("Id não pode ser nulo.");
            }

            Database_CadastroProduto db = new Database_CadastroProduto();
            db.Remover(id);
        } 

        public List<DTO_CadastroProduto> Consultar (string produto)
        {

            Database_CadastroProduto db = new Database_CadastroProduto();
            return db.Consultar(produto);
        }

        public void Alterar(DTO_CadastroProduto dto)
        {
            exReg reg = new exReg();
            reg.ValidarNome(dto.Produto);
            if (String.IsNullOrEmpty(dto.Produto) || dto.Produto.Trim().Length == 0)
            {
                throw new ArgumentException("O campo Produto só tem espaços.");
            }
            if (dto.Produto == string.Empty)
            {
                throw new ArgumentException("Produto é obrigatório");
            }

            if (dto.Preco == 0)
            {
                throw new ArgumentException("Preço é obrigatório");
            }
            Database_CadastroProduto db = new Database_CadastroProduto();
            db.Alterar(dto);
        }

        public List<DTO_CadastroProduto> Buscar(int id)
        {
            //if(id==0)
            //{
            //    throw new ArgumentException("Id é obrigatório.");
            //}
            Database_CadastroProduto db = new Database_CadastroProduto();
            return db.Buscar(id);
        }

        public List<DTO_CadastroProduto> ConsultarItensdoCardapio(int ID)
        {
            Database_CadastroProduto db = new Database_CadastroProduto();
            return db.ConsultarItensdoCardapio(ID);
        }
    }
}
