﻿using Lanchonha.Banco.Banco;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lanchonha.Banco.BD.CadastroProduto
{
    class Database_CadastroProduto
    {
        public int Salvar(DTO_CadastroProduto dto)
        {
            string script = @"INSERT INTO tb_cadastro_produtos (nm_produto,dt_validade,pr_preco,fk_id_cadastro_funcionario)
                                                 values     (@nm_produto,@dt_validade,@pr_preco,@fk_id_cadastro_funcionario)";
            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_produto",dto.Produto));
            parms.Add(new MySqlParameter("dt_validade",dto.Validade));
            parms.Add(new MySqlParameter("pr_preco",dto.Preco));
            parms.Add(new MySqlParameter("fk_id_cadastro_funcionario", dto.FkFuncionario));

            DataBase db = new DataBase();
            int pk = db.ExecuteInsertScriptWithPk(script, parms);
            return pk;
        }

        public void Alterar(DTO_CadastroProduto dto)
        {
            string script = @"update tb_cadastro_produtos set 
                                                         nm_produto=@nm_produto,
                                                         dt_validade=@dt_validade,
                                                         pr_preco=@pr_preco,
                                                         fk_id_cadastro_funcionario=@fk_id_cadastro_funcionario 
                                               where        id_cadastro_produtos=@id_cadastro_produtos";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_cadastro_produtos", dto.ID));
            parms.Add(new MySqlParameter("nm_produto", dto.Produto));
            parms.Add(new MySqlParameter("dt_validade", dto.Validade));
            parms.Add(new MySqlParameter("pr_preco", dto.Preco));
            parms.Add(new MySqlParameter("fk_id_cadastro_funcionario", dto.FkFuncionario));
            DataBase db = new DataBase();
            db.ExecuteInsertScript(script,parms);
        }    

        public List<DTO_CadastroProduto> Listar()
        {
            string script = @"select * from tb_cadastro_produtos";
            List<MySqlParameter> parms = new List<MySqlParameter>();
            DataBase db = new DataBase();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);
            List<DTO_CadastroProduto> lista = new List<DTO_CadastroProduto>();
            while (reader.Read())
            {
                DTO_CadastroProduto dados = new DTO_CadastroProduto();
                dados.ID = reader.GetInt32("id_cadastro_produtos");
                dados.Produto = reader.GetString("nm_produto");
                dados.Validade = reader.GetDateTime("dt_validade");
                dados.Preco = reader.GetDecimal("pr_preco");
                dados.FkFuncionario = reader.GetInt32("fk_id_cadastro_funcionario");        
                lista.Add(dados);
            }
            reader.Close();
            return lista;
        }

        public void Remover (int id)
        {
            string script = @"delete from tb_cadastro_produtos where id_cadastro_produtos=@id_cadastro_produtos";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_cadastro_produtos", id));

            DataBase db = new DataBase();
            db.ExecuteInsertScript(script, parms);
        }

        public List<DTO_CadastroProduto> Consultar (string Produto)
        {
            string script = @"select *from tb_cadastro_produtos where nm_produto like @nm_produto";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_produto", Produto + "%"));
            DataBase db = new DataBase();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);
            List<DTO_CadastroProduto> lista = new List<DTO_CadastroProduto>();
            DTO_CadastroProduto dto = null;
            while(reader.Read())
            {
                dto = new DTO_CadastroProduto();
                dto.ID = reader.GetInt32("id_cadastro_produtos");
                dto.Produto = reader.GetString("nm_produto");
                dto.Validade = reader.GetDateTime("dt_validade");
                dto.Preco = reader.GetDecimal("pr_preco");
                dto.FkFuncionario = reader.GetInt32("fk_id_cadastro_funcionario");
                lista.Add(dto);
            }
            reader.Close();
            return lista;
        }

        public List<DTO_CadastroProduto> Buscar(int id)
        {
            string script = @"select * from tb_cadastro_produtos where id_cadastro_produtos like @id_cadastro_produtos";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_cadastro_produtos", id+ "%"));
            DataBase db = new DataBase();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);
            List<DTO_CadastroProduto> lista = new List<DTO_CadastroProduto>();
            DTO_CadastroProduto dto = null;
            while (reader.Read())
            {
                dto = new DTO_CadastroProduto();
                dto.ID = reader.GetInt32("id_cadastro_produtos");
                dto.Produto = reader.GetString("nm_produto");
                dto.Validade = reader.GetDateTime("dt_validade");
                dto.Preco = reader.GetDecimal("pr_preco");
                dto.FkFuncionario = reader.GetInt32("fk_id_cadastro_funcionario");
                lista.Add(dto);
            }
            reader.Close();
            return lista;
        }

        public List<DTO_CadastroProduto> ConsultarItensdoCardapio(int ID)
        {
            string script = @"select  fk_id_produto_para_es,tb_cadastro_produtos.nm_produto from tb_estoque
                                join tb_produto_no_cardapio
                                on tb_estoque.id_estoque = tb_produto_no_cardapio.fk_id_estoque_para_pc
                                join tb_cadastro_produtos
                                on tb_cadastro_produtos.id_cadastro_produtos = tb_estoque.fk_id_produto_para_es
                                where tb_produto_no_cardapio.fk_id_cardapio_para_pc =" +ID;
            List<MySqlParameter> parms = new List<MySqlParameter>();
            DataBase db = new DataBase();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);
            List<DTO_CadastroProduto> lista = new List<DTO_CadastroProduto>();
            while(reader.Read())
            {
                DTO_CadastroProduto dto = new DTO_CadastroProduto();
                dto.ID = reader.GetInt32("fk_id_produto_para_es");
                dto.Produto = reader.GetString("nm_produto");

                lista.Add(dto);
            }
            reader.Close();
            return lista;
        }



    }
}
