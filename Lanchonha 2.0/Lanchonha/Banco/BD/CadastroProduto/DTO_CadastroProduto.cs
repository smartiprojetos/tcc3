﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lanchonha.Banco.BD.CadastroProduto
{
    class DTO_CadastroProduto
  {
        public int ID { get; set; }
        public string Produto { get; set; }
        public DateTime Validade { get; set; }
        public decimal Preco { get; set; }
        public int FkFuncionario { get; set; }
    }
}
