﻿using Lanchonha.Banco.Banco;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lanchonha.Banco.BD.Cliente
{
    class Database_Cliente
    {
        public int Salvar(DTO_Cliente dto)
        {
            string script = @"insert into tb_cadastro_cliente (nm_completo,cf_cpf,cl_celular,fk_id_funcionario_cc)
                                                   values   (@nm_completo,@cf_cpf,@cl_celular,@fk_id_funcionario_cc)";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_completo", dto.NomeCompleto));
            parms.Add(new MySqlParameter("cf_cpf", dto.CPF));
            parms.Add(new MySqlParameter("cl_celular", dto.Celular));
            parms.Add(new MySqlParameter("fk_id_funcionario_cc", dto.FKIDFuncionario));

            DataBase db = new DataBase();
            return db.ExecuteInsertScriptWithPk(script, parms);
        }


        public void Alterar (DTO_Cliente dto)
        {
            string script = @"update tb_cadastro_cliente set    nm_completo=@nm_completo, 
                                                                cf_cpf =@cf_cpf,
                                                                cl_celular=@cl_celular,
                                                                fk_id_funcionario_cc=@fk_id_funcionario_cc
                                                         where id_cadastro_cliente=@id_cadastro_cliente";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_cadastro_cliente", dto.ID));
            parms.Add(new MySqlParameter("nm_completo", dto.NomeCompleto));
            parms.Add(new MySqlParameter("cf_cpf", dto.CPF));
            parms.Add(new MySqlParameter("cl_celular", dto.Celular));
            parms.Add(new MySqlParameter("fk_id_funcionario_cc", dto.FKIDFuncionario));
            DataBase db = new DataBase();
            db.ExecuteInsertScript(script, parms);

        }

        public List<DTO_Cliente> Listar()
        {
            string script = @"select * from tb_cadastro_cliente";
            List<MySqlParameter> parms = new List<MySqlParameter>();
            DataBase db = new DataBase();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);
            List<DTO_Cliente> lista = new List<DTO_Cliente>();
            while(reader.Read())
            {
                DTO_Cliente dto = new DTO_Cliente();
                dto.ID = reader.GetInt32("id_cadastro_cliente");
                dto.NomeCompleto = reader.GetString("nm_completo");
                dto.CPF = reader.GetString("cf_cpf");
                dto.Celular = reader.GetString("cl_celular");
                dto.FKIDFuncionario = reader.GetInt32("fk_id_funcionario_cc");

                lista.Add(dto);
            }

            reader.Close();
            return lista;
        }

        public List<DTO_Cliente> ConsultarporNome(string nome)
        {
            string script = @"select * from tb_cadastro_cliente  where nm_completo like @nm_completo";
            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_completo", nome + "%"));
            DataBase db = new DataBase();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);
            List<DTO_Cliente> lista = new List<DTO_Cliente>();
            while (reader.Read())
            {
                DTO_Cliente dto = new DTO_Cliente();
                dto.ID = reader.GetInt32("id_cadastro_cliente");
                dto.NomeCompleto = reader.GetString("nm_completo");
                dto.CPF = reader.GetString("cf_cpf");
                dto.Celular = reader.GetString("cl_celular");
                dto.FKIDFuncionario = reader.GetInt32("fk_id_funcionario_cc");

                lista.Add(dto);
            }

            reader.Close();
            return lista;
        }

        public List<DTO_Cliente> ConsultarporID(int ID)
        {
            string script = @"select * from tb_cadastro_cliente where id_cadastro_cliente like @id_cadastro_cliente";
            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_cadastro_cliente", ID + "%"));
            DataBase db = new DataBase();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);
            List<DTO_Cliente> lista = new List<DTO_Cliente>();
            while (reader.Read())
            {
                DTO_Cliente dto = new DTO_Cliente();
                dto.ID = reader.GetInt32("id_cadastro_cliente");
                dto.NomeCompleto = reader.GetString("nm_completo");
                dto.CPF = reader.GetString("cf_cpf");
                dto.Celular = reader.GetString("cl_celular");
                dto.FKIDFuncionario = reader.GetInt32("fk_id_funcionario_cc");

                lista.Add(dto);
            }

            reader.Close();
            return lista;
        }

        public void Remover(int Id)
        {
            string script = @"delete from tb_cadastro_cliente where id_cadastro_cliente like @id_cadastro_cliente";
            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_cadastro_cliente", Id));

            DataBase db = new DataBase();
            db.ExecuteInsertScript(script, parms);
        }
    }
}
