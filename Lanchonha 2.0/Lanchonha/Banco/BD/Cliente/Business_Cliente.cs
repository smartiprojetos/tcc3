﻿using Lanchonha.Utilitarios;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lanchonha.Banco.BD.Cliente
{
    class Business_Cliente
    {
        public int Salvar (DTO_Cliente dto)
        {

            CPF reg = new CPF();
            reg.ValidarCPF(dto.CPF);
            exReg nha = new exReg();
            nha.ValidarNome(dto.NomeCompleto);
            if (String.IsNullOrEmpty(dto.NomeCompleto) || dto.NomeCompleto.Trim().Length == 0)
            {
                throw new ArgumentException("O campo Nome só tem espaços.");
            }
            if (dto.Celular == null)
            {
                throw new ArgumentException("O campo Celular está em branco, por favor preencher.");
            }
            if (dto.NomeCompleto == null)
            {
                throw new ArgumentException("O campo Nome está em branco, por favor preencher.");
            }
            if (dto.CPF == null)
            {
                throw new ArgumentException("O campo CPF está em branco, por favor preencher.");
            }


            Database_Cliente db = new Database_Cliente();
            return db.Salvar(dto);
        }

        public List<DTO_Cliente> Listar()
        {
            Database_Cliente db = new Database_Cliente();
            return db.Listar();
        }

        public void Alterar(DTO_Cliente dto)
        {
            CPF reg = new CPF();
            reg.ValidarCPF(dto.CPF);
            exReg nha = new exReg();
            nha.ValidarNome(dto.NomeCompleto);
            if (String.IsNullOrEmpty(dto.NomeCompleto) || dto.NomeCompleto.Trim().Length == 0)
            {
                throw new ArgumentException("O campo Nome só tem espaços.");
            }
            if (String.IsNullOrEmpty(dto.Celular) || dto.Celular.Trim().Length == 0)
            {
                throw new ArgumentException("O campo Celular só tem espaços.");
            }
            if (dto.Celular == null)
            {
                throw new ArgumentException("O campo Celular está em branco, por favor preencher.");
            }
            if (dto.NomeCompleto == null)
            {
                throw new ArgumentException("O campo Nome está em branco, por favor preencher.");
            }
            if (dto.CPF == null)
            {
                throw new ArgumentException("O campo CPF está em branco, por favor preencher.");
            }
            Database_Cliente db = new Database_Cliente();
            db.Alterar(dto);
        }

        public void Remover(int Id)
        {
            Database_Cliente db = new Database_Cliente();
            db.Remover(Id);
        }

        public List<DTO_Cliente> ConsultarporNome(string nome)
        {
            Database_Cliente db = new Database_Cliente();
            return db.ConsultarporNome(nome);
        }

        public List<DTO_Cliente> ConsultarporID(int ID)
        {
            Database_Cliente db = new Database_Cliente();
            return db.ConsultarporID(ID);
        }
    }
}
