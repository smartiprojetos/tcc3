﻿using Lanchonha.Utilitarios;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lanchonha.Banco.BD.Cardapio
{
    class Business_Cardapio
    {
        public int Salvar (DTO_Cardapio dto)
        {
            exReg reg = new exReg();
            reg.ValidarNome(dto.NomeProduto);
            reg.ValidarValor(Convert.ToString(dto.Valor));
            if (String.IsNullOrEmpty(dto.NomeProduto) || dto.NomeProduto.Trim().Length == 0)
            {
                throw new ArgumentException("O campo Nome do Produto só tem espaços.");
            }
            //if (String.IsNullOrEmpty(dto.Tipo) || dto.Tipo.Trim().Length == 0)
            //{
            //    throw new ArgumentException("O campo Descrição só tem espaços.");
            //}
            if (dto.NomeProduto == null)
            {
                throw new ArgumentException("O campo Nome do Produto está em branco, por favor preencher.");
            }
            //if (dto.Tipo == string.Empty)
            //{
            //    throw new ArgumentException("O campo Tipo está em branco, por favor preencher.");
            //}


            Database_Cardapio db = new Database_Cardapio();
            return db.Salvar(dto);
        }

        public void Alterar (DTO_Cardapio dto)
        {

            exReg reg = new exReg();
            reg.ValidarNome(dto.NomeProduto);
            reg.ValidarValor(Convert.ToString(dto.Valor));
            if (String.IsNullOrEmpty(dto.NomeProduto) || dto.NomeProduto.Trim().Length == 0)
            {
                throw new ArgumentException("O campo Nome do Produto só tem espaços.");
            }
            //if (String.IsNullOrEmpty(dto.Tipo) || dto.Tipo.Trim().Length == 0)
            //{
            //    throw new ArgumentException("O campo Descrição só tem espaços.");
            //}
            if (dto.NomeProduto == null)
            {
                throw new ArgumentException("O campo Nome do Produto está em branco, por favor preencher.");
            }
            //if (dto.Tipo == string.Empty)
            //{
            //    throw new ArgumentException("O campo Tipo está em branco, por favor preencher.");
            //}
            Database_Cardapio db = new Database_Cardapio();
            db.Alterar(dto);
        }

        public void Remover(int ID)
        {
            Database_Cardapio db = new Database_Cardapio();
            db.Remover(ID);
        }

        public List<DTO_Cardapio> Listar()
        {
            Database_Cardapio db = new Database_Cardapio();
            return db.Listar();
        }

        public List<DTO_Cardapio> ConsultarporNome(string produto)
        {
            Database_Cardapio db = new Database_Cardapio();
            return db.ConsultarporNome(produto);
        }

        public List<DTO_Cardapio> ConsultarporId(int ID)
        {
            Database_Cardapio db = new Database_Cardapio();
            return db.ConsultarporId(ID);
        }

    }
}
