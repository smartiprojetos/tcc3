﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lanchonha.Banco.BD.Cardapio
{
    class DTO_Cardapio
    {
        public int ID { get; set; }
        public string NomeProduto { get; set; }
        public decimal Valor { get; set; }
        public string Tipo { get; set; }
        public int FkFuncionario { get; set; }

    }
}
