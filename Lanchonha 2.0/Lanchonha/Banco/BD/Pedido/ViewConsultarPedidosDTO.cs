﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lanchonha.Banco.BD.Pedido
{
    class ViewConsultarPedidosDTO
    {
        public int FKIDCliente { get; set; }
        public string NomeCliente { get; set; }
        public string NomeProduto { get; set; }
        public decimal ValorTotaldaCompra { get; set; }
        public DateTime DatadeCompra { get; set; }
        public string Observacoes { get; set; }

    }
}
