﻿using Lanchonha.Banco.Banco;
using Lanchonha.Banco.BD.Fornecedor;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lanchonha.Banco.BD.Pedido
{
    class Database_Pedidos
    {

        public int Salvar(DTO_Pedidos dto) {

            string script = @"Insert into tb_pedido (
                                            vl_valor_pago,
                                            meiodepagamento,
                                            dt_venda,
                                            fk_id_cliente_para_pe,
                                            fk_id_funcionario_para_pe)
                                    Values(
                                            @vl_valor_pago,
                                            @meiodepagamento,
                                            @dt_venda,
                                            @fk_id_cliente_para_pe,
                                            @fk_id_funcionario_para_pe)  ";
            List<MySqlParameter> parm = new List<MySqlParameter>();
            parm.Add(new MySqlParameter("vl_valor_pago", dto.ValorPago));
            parm.Add(new MySqlParameter("meiodepagamento",dto.FormadePagamento));
            parm.Add(new MySqlParameter("dt_venda",dto.DataVenda));
            parm.Add(new MySqlParameter("fk_id_cliente_para_pe", dto.ID_Cliente));
            parm.Add(new MySqlParameter("fk_id_funcionario_para_pe",dto.ID_Funcionario));

            DataBase db = new DataBase();

            return db.ExecuteInsertScriptWithPk(script, parm);
        }


        public List<DTO_Pedidos> Consultar(string cliente)
        {

            string script = @"select * from tb_pedido where like %@cliente%";
            List<MySqlParameter> parm = new List<MySqlParameter>();
            parm.Add(new MySqlParameter("id_cadastro_funcionario",cliente + "%"));
            DataBase db = new DataBase();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parm);
            List<DTO_Pedidos> lista = new List<DTO_Pedidos>();

            while (reader.Read())
            {
                DTO_Pedidos dto = new DTO_Pedidos();
                dto.ID = reader.GetInt32("id_pedido");
                dto.ID_Cliente = reader.GetInt32("fk_id_cliente_para_pe");
                dto.DataVenda = reader.GetDateTime("dt_venda");
                dto.ID_Funcionario = reader.GetInt32("fk_id_funcionario_para_pe");
                dto.FormadePagamento = reader.GetString("meiodepagamento");
                dto.ValorPago = reader.GetDecimal("vl_valor_pago");

                lista.Add(dto);
            }
            reader.Close();
            return lista;

        }

        public List<DTO_Pedidos> Listar()
        {
            
            string script = @"select * from tb_pedido";
            List<MySqlParameter> parm = new List<MySqlParameter>();
            DataBase db = new DataBase();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parm);
            List<DTO_Pedidos> lista = new List<DTO_Pedidos>();

            while (reader.Read())
            { 
                DTO_Pedidos dto = new DTO_Pedidos();    
                dto.ID = reader.GetInt32("id_pedido");
                dto.ID_Cliente = reader.GetInt32("fk_id_cliente_para_pe");
                dto.DataVenda = reader.GetDateTime("dt_venda");
                dto.ID_Funcionario = reader.GetInt32("fk_id_funcionario_para_pe");
                dto.FormadePagamento = reader.GetString("meiodepagamento");
                dto.ValorPago = reader.GetDecimal("vl_valor_pago");

                lista.Add(dto);
            }
            reader.Close();
            return lista;

        }

        public List<view_pedidoDTO> ConsultarPorNomeCliente(string nome)
        {
            string script = @"select * from view_pedido where nm_completo like @nm_completo";
            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_completo", nome + "%"));
            DataBase db = new DataBase();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);
            List<view_pedidoDTO> lista = new List<view_pedidoDTO>();
            while(reader.Read())
            {
                view_pedidoDTO dto = new view_pedidoDTO();
                dto.IDPedido = reader.GetInt32("id_pedido");
                dto.ValorPago = reader.GetDecimal("vl_valor_pago");
                dto.MeiodePagamento = reader.GetString("meiodepagamento");
                dto.DatadeVenda = reader.GetDateTime("dt_venda");
                dto.NomeFuncionario = reader.GetString("nm_nome_completo");
                dto.NomeCliente = reader.GetString("nm_completo");

                lista.Add(dto);
            }
            reader.Close();
            return lista;
        }          
    }
}
