﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lanchonha.Banco.BD.Pedido
{
    class Business_PedidoItem
    {
        public int Salvar(DTO_PedidoItem dto)
        {
           if(dto.qnt_quantidade==0)
            {
                throw new ArgumentException("O Campo Quantidade não pode ser vazio.");
            }
            Database_PedidoItem db = new Database_PedidoItem();
            return db.Salvar(dto);
        }

        public List<ViewConsultarPedidosDTO> Consultar(string nome)
        {
            Database_PedidoItem db = new Database_PedidoItem();
            return db.Consultar(nome);
        }
    }
}
