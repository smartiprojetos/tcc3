﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lanchonha.Banco.BD.Pedido
{
    class view_pedidoDTO
    {
        public int IDPedido { get; set; }
        public decimal ValorPago { get; set; }
        public string MeiodePagamento { get; set; }
        public DateTime DatadeVenda { get; set; }
        public string NomeFuncionario { get; set; }
        public string NomeCliente { get; set; }
    }
}
