﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lanchonha.Banco.BD.Pedido
{
    class DTO_Pedidos
    {
        public int ID { get; set; }
        public decimal ValorPago { get; set; }
        public DateTime DataVenda { get; set; }
        public string CPF { get; set; }
        public string FormadePagamento { get; set; }

        public int ID_Cliente { get; set; }
        public int ID_Funcionario { get; set; }

    }
}
