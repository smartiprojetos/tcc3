﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lanchonha.Banco.BD.Pedido
{
    class DTO_PedidoItem
    {
        public int id_pedido_item { get; set; }
        public decimal qnt_quantidade { get; set; }
        public string obs_observacoes { get; set; }
        public int fk_id_pedido_para_pi { get; set; }
        public int fk_id_cardapio_para_pi { get; set; }


    }

   
}
