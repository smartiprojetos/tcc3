﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lanchonha.Banco.BD.Pedido
{
    class GriedViewPedido
    {
        public string produto { get; set; }
        public decimal preco { get; set; }
        public int idproduto { get; set; }
        public string observacao { get; set; }
        public decimal quantidade { get; set; }
    }
}
