﻿using Lanchonha.Banco.Banco;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lanchonha.Banco.BD.Pedido
{
    class Database_PedidoItem
    {
        public int Salvar(DTO_PedidoItem dto)
        {
            string script = @"INSERT INTO
                                tb_pedido_item (
                                                fk_id_pedido_para_pi,
                                                fk_id_cardapio_para_pi,
                                                obs_observacoes,
                                                qnt_quantidade
                                                ) 
                                        VALUES (
                                                @fk_id_pedido_para_pi,
                                                @fk_id_cardapio_para_pi,
                                                @obs_observacoes,
                                                @qnt_quantidade)";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("fk_id_pedido_para_pi", dto.fk_id_pedido_para_pi));
            parms.Add(new MySqlParameter("fk_id_cardapio_para_pi", dto.fk_id_cardapio_para_pi));
            parms.Add(new MySqlParameter("obs_observacoes", dto.obs_observacoes));
            parms.Add(new MySqlParameter("qnt_quantidade", dto.qnt_quantidade));


            DataBase db = new DataBase();
            return db.ExecuteInsertScriptWithPk(script, parms);
        }

        public void Remover(int id)
        {
            string script = @"DELETE FROM tb_pedido_item WHERE id_pedido_item = @id_pedido_item";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_pedido_item", id));

            DataBase db = new DataBase();
            db.ExecuteInsertScript(script, parms);
        }

        public List<ViewConsultarPedidosDTO> Consultar (string nome)
        {
            string script = @"select * from consultar_pedido where nm_completo like @nm_completo";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_completo", nome + "%"));
            DataBase db = new DataBase();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);
            List<ViewConsultarPedidosDTO> lista = new List<ViewConsultarPedidosDTO>();
            while(reader.Read())
            {
                ViewConsultarPedidosDTO dto = new ViewConsultarPedidosDTO();
                dto.FKIDCliente = reader.GetInt32("fk_id_cliente_para_pe");
                dto.NomeCliente = reader.GetString("nm_completo");
                dto.NomeProduto = reader.GetString("nm_produto");
                dto.ValorTotaldaCompra = reader.GetDecimal("vl_valor_pago");
                dto.DatadeCompra = reader.GetDateTime("dt_venda");
                dto.Observacoes = reader.GetString("obs_observacoes");

                lista.Add(dto);
            }
            reader.Close();
            return lista;
        }

        
    }
}
