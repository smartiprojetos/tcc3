﻿using Lanchonha.Banco.BD.CadastroProduto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lanchonha.Banco.BD.Pedido
{
    class Business_Pedidos
    {


        public int Salvar(DTO_Pedidos pedido, List<GriedViewPedido> produtos)
        {
            
           
            if (pedido.FormadePagamento==string.Empty)
            {
                throw new ArgumentException("Escolha uma forma de pagamento.");
            }
            Database_Pedidos pedidoDatabase = new Database_Pedidos();
            int idPedido = pedidoDatabase.Salvar(pedido);

            Business_PedidoItem itemBusiness = new Business_PedidoItem();
            foreach (GriedViewPedido item in produtos)
            {
                DTO_PedidoItem itemDto = new DTO_PedidoItem();
                itemDto.fk_id_pedido_para_pi = idPedido;
                itemDto.fk_id_cardapio_para_pi = item.idproduto;
                itemDto.obs_observacoes = item.observacao;
                itemDto.qnt_quantidade = item.quantidade;


                itemBusiness.Salvar(itemDto);
            }

            return idPedido;
        }


        public List<DTO_Pedidos> Lista()
        {
            Database_Pedidos data =  new Database_Pedidos();
            return data.Listar();
          
        }

        public List<view_pedidoDTO> ConsultarPorNomeCliente(string nomecliente)
        {
            Database_Pedidos data = new Database_Pedidos();
            return data.ConsultarPorNomeCliente(nomecliente);

        }
    }
}
