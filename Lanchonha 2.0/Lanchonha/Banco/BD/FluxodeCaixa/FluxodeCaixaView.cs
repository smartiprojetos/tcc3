﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lanchonha.Banco.BD.FluxodeCaixa
{
    public class FluxodeCaixaView
    {
        public DateTime dt_operacao { get; set; }
        public decimal vl_total { get; set; }
        public string to_operacao { get; set; }
    }
}
