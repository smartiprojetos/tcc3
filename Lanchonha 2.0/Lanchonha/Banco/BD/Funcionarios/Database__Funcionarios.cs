﻿using Lanchonha.Banco.Banco;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lanchonha.Banco.BD.Funcionarios
{
    public class Database__Funcionarios
    {
        public int Salvar(FuncionarioDTO dto)
        {
            string script = @"INSERT into tb_cadastro_funcionario
            (nm_nome_completo,user_usuario, sn_senha,cg_cargo,rg,dt_data_nascimento,cf_cpf,dt_data_cadastro, 
                                                                            pr_permissao_funcionarios, 
                                                                            pr_permissao_produto, 
                                                                            pr_permissao_cardapio, 
                                                                            pr_permissao_fornecedor, 
                                                                            pr_permissao_cadastrar, 
                                                                            pr_permissao_estoque, 
                                                                            pr_permissao_atualizar_estoque, 
                                                                            pr_permissao_consultar_estoque, 
                                                                            pr_permissao_pedido_fornecedor, 
                                                                            pr_permissao_cliente, 
                                                                            pr_permissao_controle_cliente, 
                                                                            pr_permissao_consultar_cliente, 
                                                                            pr_permissao_cadastrar_funcionarios, 
                                                                            pr_permissao_consultar_funcionarios, 
                                                                            pr_permissao_cadastrar_novo_produto, 
                                                                            pr_permissao_consultar_produtos, 
                                                                            pr_permissao_consultar_cardapio, 
                                                                            pr_permissao_cadastro_novo_cardapio, 
                                                                            pr_permissao_novo_fornecedor, 
                                                                            pr_permissao_consultar_fornecedor,
                                                                     
                                                                            pr_permissao_fluxo_de_caixa,
                                                                            pr_permissao_fluxo_de_caixa_bt,
                                                                            pr_permissao_fluxo_de_caixa_consultar,
                                                                            pr_permissao_folha_de_pagamento,
                                                                            permissao_folha_de_pagamento_gerar,
                                                                            permissao_folha_de_pagamento_consultar,
                                                                            permissao_folha_de_pagamento_alterar,
                                                                            pr_permissao_funcionario_alterar,
                                                                            pr_permissao_fornecedor_alterar,
                                                                            pr_permissao_estoque_alterar,
                                                                            pr_permissao_pedido_cadastrar,
                                                                            pr_permissao_pedido_consultar,
                                                                            pr_permissao_produto_alterar,                                                                          
                                                                            pr_permissao_cardapio_alterar,
                                                                            pr_permissao_pedido,
                                                                            sx_sexo,
                                                                            tl_telefone,
                                                                            cl_celular,
                                                                            cp_cep,
                                                                            rua,
                                                                            br_bairro,
                                                                            nr_numero_da_casa,
                                                                            st_estado_uf,
                                                                            cd_cidade)  
          VALUES
         (@nm_nome_completo,@user_usuario, @sn_senha,@cg_cargo,@rg, @dt_data_nascimento, @cf_cpf, @dt_data_cadastro,
                                                                            @pr_permissao_funcionarios, 
                                                                            @pr_permissao_produto, 
                                                                            @pr_permissao_cardapio, 
                                                                            @pr_permissao_fornecedor, 
                                                                            @pr_permissao_cadastrar, 
                                                                            @pr_permissao_estoque, 
                                                                            @pr_permissao_atualizar_estoque, 
                                                                            @pr_permissao_consultar_estoque, 
                                                                            @pr_permissao_pedido_fornecedor, 
                                                                            @pr_permissao_cliente, 
                                                                            @pr_permissao_controle_cliente, 
                                                                            @pr_permissao_consultar_cliente , 
                                                                            @pr_permissao_cadastrar_funcionarios, 
                                                                            @pr_permissao_consultar_funcionarios, 
                                                                            @pr_permissao_cadastrar_novo_produto, 
                                                                            @pr_permissao_consultar_produtos, 
                                                                            @pr_permissao_consultar_cardapio, 
                                                                            @pr_permissao_cadastro_novo_cardapio, 
                                                                            @pr_permissao_novo_fornecedor, 
                                                                            @pr_permissao_consultar_fornecedor,
                                                                           
                                                                            @pr_permissao_fluxo_de_caixa,
                                                                            @pr_permissao_fluxo_de_caixa_bt,
                                                                            @pr_permissao_fluxo_de_caixa_consultar,
                                                                            @pr_permissao_folha_de_pagamento,
                                                                            @permissao_folha_de_pagamento_gerar,
                                                                            @permissao_folha_de_pagamento_consultar,
                                                                            @permissao_folha_de_pagamento_alterar,
                                                                            @pr_permissao_funcionario_alterar,
                                                                            @pr_permissao_fornecedor_alterar,
                                                                            @pr_permissao_estoque_alterar,
                                                                            @pr_permissao_pedido_cadastrar,
                                                                            @pr_permissao_pedido_consultar,
                                                                            @pr_permissao_produto_alterar,                                                                          
                                                                            @pr_permissao_cardapio_alterar,
                                                                            @pr_permissao_pedido,
                                                                            @sx_sexo,
                                                                            @tl_telefone,
                                                                            @cl_celular,
                                                                            @cp_cep,
                                                                            @rua,
                                                                            @br_bairro,
                                                                            @nr_numero_da_casa,
                                                                            @st_estado_uf,
                                                                            @cd_cidade )";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_nome_completo", dto.nomecompleto));
            parms.Add(new MySqlParameter("user_usuario", dto.usuario));
            parms.Add(new MySqlParameter("sn_senha", dto.senha));
            parms.Add(new MySqlParameter("cg_cargo", dto.cargo));
            parms.Add(new MySqlParameter("rg", dto.RG));
            parms.Add(new MySqlParameter("dt_data_nascimento", dto.Nascimento));
            parms.Add(new MySqlParameter("cf_cpf", dto.cpf));
            parms.Add(new MySqlParameter("dt_data_cadastro", dto.cadastro));
            parms.Add(new MySqlParameter("pr_permissao_funcionarios", dto.permissao_funcionarios));
            parms.Add(new MySqlParameter("pr_permissao_produto", dto.permissao_produtos));
            parms.Add(new MySqlParameter("pr_permissao_cardapio", dto.permissao_cardapio));
            parms.Add(new MySqlParameter("pr_permissao_fornecedor", dto.permissao_fornecedor));
            parms.Add(new MySqlParameter("pr_permissao_cadastrar", dto.permissao_vinculardadosdecardapio_e_produto));
            parms.Add(new MySqlParameter("pr_permissao_estoque", dto.permissao_estoque));
            parms.Add(new MySqlParameter("pr_permissao_atualizar_estoque", dto.permissao_atualizar_estoque));
            parms.Add(new MySqlParameter("pr_permissao_consultar_estoque", dto.permissao_consultar_estoque));
            parms.Add(new MySqlParameter("pr_permissao_pedido_fornecedor", dto.permissao_pedido_fornecedor));
            parms.Add(new MySqlParameter("pr_permissao_cliente", dto.permissao_cliente));
            parms.Add(new MySqlParameter("pr_permissao_controle_cliente", dto.permissao_controle_cliente));
            parms.Add(new MySqlParameter("pr_permissao_consultar_cliente", dto.permissao_consultar_cliente));
            parms.Add(new MySqlParameter("pr_permissao_cadastrar_funcionarios", dto.permissao_cadastrar_funcionarios));
            parms.Add(new MySqlParameter("pr_permissao_consultar_funcionarios", dto.permissao_consultar_funcionarios));
            parms.Add(new MySqlParameter("pr_permissao_cadastrar_novo_produto", dto.permissao_cadastrar_novo_produto));
            parms.Add(new MySqlParameter("pr_permissao_consultar_produtos", dto.consultar_produtos));
            parms.Add(new MySqlParameter("pr_permissao_consultar_cardapio", dto.permissao_consultar_cardapio));
            parms.Add(new MySqlParameter("pr_permissao_cadastro_novo_cardapio", dto.permissao_cadastro_novo_cardapio));
            parms.Add(new MySqlParameter("pr_permissao_novo_fornecedor", dto.permissao_novo_fornecedor));
            parms.Add(new MySqlParameter("pr_permissao_consultar_fornecedor", dto.permissao_consultar_fornecedor));
            parms.Add(new MySqlParameter("pr_permissao_fluxo_de_caixa", dto.permissao_fluxo_de_caixa));
            parms.Add(new MySqlParameter("pr_permissao_fluxo_de_caixa_bt", dto.permissao_fluxo_de_caixa_bt));
            parms.Add(new MySqlParameter("pr_permissao_fluxo_de_caixa_consultar", dto.permissao_fluxo_de_caixa_consultar));
            parms.Add(new MySqlParameter("pr_permissao_folha_de_pagamento", dto.permissao_folha_de_pagamento));

            parms.Add(new MySqlParameter("permissao_folha_de_pagamento_gerar", dto.permissao_folha_de_pagamento_gerar));
            parms.Add(new MySqlParameter("permissao_folha_de_pagamento_consultar", dto.permissao_folha_de_pagamento_consultar));
            parms.Add(new MySqlParameter("permissao_folha_de_pagamento_alterar", dto.permissao_folha_de_pagamento_alterar));

            parms.Add(new MySqlParameter("pr_permissao_funcionario_alterar", dto.permissao_funcionario_alterar));
            parms.Add(new MySqlParameter("pr_permissao_fornecedor_alterar", dto.permissao_fornecedor_alterar));
            parms.Add(new MySqlParameter("pr_permissao_pedido_cadastrar", dto.permissao_pedido_cadastrar));
            parms.Add(new MySqlParameter("pr_permissao_pedido_consultar", dto.permissao_pedido_consultar));
            parms.Add(new MySqlParameter("pr_permissao_produto_alterar", dto.permissao_produto_alterar));
            parms.Add(new MySqlParameter("pr_permissao_cardapio_alterar", dto.permissao_cardapio_alterar));
            parms.Add(new MySqlParameter("pr_permissao_estoque_alterar", dto.permissao_estoque_alterar));
            parms.Add(new MySqlParameter("pr_permissao_pedido", dto.permissao_pedido));
            parms.Add(new MySqlParameter("sx_sexo", dto.Sexo));
            parms.Add(new MySqlParameter("tl_telefone", dto.Telefone));
            parms.Add(new MySqlParameter("cl_celular", dto.Celular));
            parms.Add(new MySqlParameter("cp_cep", dto.CEP));
            parms.Add(new MySqlParameter("rua", dto.Rua));
            parms.Add(new MySqlParameter("br_bairro", dto.Bairro));
            parms.Add(new MySqlParameter("nr_numero_da_casa", dto.numerocasa));
            parms.Add(new MySqlParameter("st_estado_uf", dto.Estado));
            parms.Add(new MySqlParameter("cd_cidade", dto.Cidade));



            DataBase db = new DataBase();
            int pk = db.ExecuteInsertScriptWithPk(script, parms);
            return pk;
        }

        public void Alterar(FuncionarioDTO dto)
        {
            string script =
               @"UPDATE tb_cadastro_funcionario SET  
                                                nm_nome_completo=@nm_nome_completo,
                                                user_usuario=@user_usuario,
                                                sn_senha=@sn_senha,
                                                cg_cargo=@cg_cargo,
                                                rg=@rg,
                                                dt_data_nascimento=@dt_data_nascimento,
                                                cf_cpf=@cf_cpf,
                                                dt_data_cadastro=@dt_data_cadastro,
                                                pr_permissao_funcionarios=@pr_permissao_funcionarios,
                                                pr_permissao_produto=@pr_permissao_produto, 
                                                pr_permissao_cardapio=@pr_permissao_cardapio, 
                                                pr_permissao_fornecedor=@pr_permissao_fornecedor,
                                                pr_permissao_cadastrar=@pr_permissao_cadastrar, 
                                                pr_permissao_estoque=@pr_permissao_estoque, 
                                                pr_permissao_atualizar_estoque=@pr_permissao_atualizar_estoque,
                                                pr_permissao_consultar_estoque=@pr_permissao_consultar_estoque, 
                                                pr_permissao_pedido_fornecedor=@pr_permissao_pedido_fornecedor,
                                                pr_permissao_cliente=@pr_permissao_cliente,
                                                pr_permissao_controle_cliente=@pr_permissao_controle_cliente, 
                                                pr_permissao_consultar_cliente=@pr_permissao_consultar_cliente, 
                                                pr_permissao_cadastrar_funcionarios=@pr_permissao_cadastrar_funcionarios, 
                                                pr_permissao_consultar_funcionarios=@pr_permissao_consultar_funcionarios, 
                                                pr_permissao_cadastrar_novo_produto =@pr_permissao_cadastrar_novo_produto,
                                                pr_permissao_consultar_produtos=@pr_permissao_consultar_produtos, 
                                                pr_permissao_consultar_cardapio=@pr_permissao_consultar_cardapio, 
                                                pr_permissao_cadastro_novo_cardapio=@pr_permissao_cadastro_novo_cardapio, 
                                                pr_permissao_novo_fornecedor=@pr_permissao_novo_fornecedor, 
                                                pr_permissao_consultar_fornecedor=@pr_permissao_consultar_fornecedor,
                                               
                                                pr_permissao_fluxo_de_caixa=@pr_permissao_fluxo_de_caixa,
                                                pr_permissao_fluxo_de_caixa_bt=@pr_permissao_fluxo_de_caixa_bt,
                                                pr_permissao_fluxo_de_caixa_consultar=@pr_permissao_fluxo_de_caixa_consultar,
                                                pr_permissao_folha_de_pagamento=@pr_permissao_folha_de_pagamento,
                                                permissao_folha_de_pagamento_gerar=@permissao_folha_de_pagamento_gerar,
                                                permissao_folha_de_pagamento_consultar=@permissao_folha_de_pagamento_consultar,
                                                permissao_folha_de_pagamento_alterar=@permissao_folha_de_pagamento_alterar,
                                                pr_permissao_funcionario_alterar=@pr_permissao_funcionario_alterar,
                                                pr_permissao_fornecedor_alterar=@pr_permissao_fornecedor_alterar,
                                                pr_permissao_estoque_alterar=@pr_permissao_estoque_alterar,
                                                pr_permissao_pedido_cadastrar=@pr_permissao_pedido_cadastrar,
                                                pr_permissao_pedido_consultar=@pr_permissao_pedido_consultar,
                                                pr_permissao_produto_alterar=@pr_permissao_produto_alterar,                                                                          
                                                pr_permissao_cardapio_alterar=@pr_permissao_cardapio_alterar,
                                                pr_permissao_pedido=@pr_permissao_pedido,    


                                                sx_sexo=@sx_sexo,
                                                tl_telefone=@tl_telefone,
                                                cl_celular=@cl_celular,
                                                cp_cep=@cp_cep,
                                                rua=@rua,
                                                br_bairro=@br_bairro,
                                                nr_numero_da_casa=@nr_numero_da_casa, 
                                                st_estado_uf=@st_estado_uf,
                                                cd_cidade=@cd_cidade
                                                WHERE id_cadastro_funcionario = @id_cadastro_funcionario";
            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_cadastro_funcionario", dto.Id_funcionario));
            parms.Add(new MySqlParameter("nm_nome_completo", dto.nomecompleto));
            parms.Add(new MySqlParameter("user_usuario", dto.usuario));
            parms.Add(new MySqlParameter("sn_senha", dto.senha));
            parms.Add(new MySqlParameter("cg_cargo", dto.cargo));
            parms.Add(new MySqlParameter("rg", dto.RG));
            parms.Add(new MySqlParameter("dt_data_nascimento", dto.Nascimento));
            parms.Add(new MySqlParameter("cf_cpf", dto.cpf));
            parms.Add(new MySqlParameter("dt_data_cadastro", dto.cadastro));
            parms.Add(new MySqlParameter("pr_permissao_funcionarios", dto.permissao_funcionarios));
            parms.Add(new MySqlParameter("pr_permissao_produto", dto.permissao_produtos));
            parms.Add(new MySqlParameter("pr_permissao_cardapio", dto.permissao_cardapio));
            parms.Add(new MySqlParameter("pr_permissao_fornecedor", dto.permissao_fornecedor));
            parms.Add(new MySqlParameter("pr_permissao_cadastrar", dto.permissao_vinculardadosdecardapio_e_produto));
            parms.Add(new MySqlParameter("pr_permissao_estoque", dto.permissao_estoque));
            parms.Add(new MySqlParameter("pr_permissao_atualizar_estoque", dto.permissao_atualizar_estoque));
            parms.Add(new MySqlParameter("pr_permissao_consultar_estoque", dto.permissao_consultar_estoque));
            parms.Add(new MySqlParameter("pr_permissao_pedido_fornecedor", dto.permissao_pedido_fornecedor));
            parms.Add(new MySqlParameter("pr_permissao_cliente", dto.permissao_cliente));
            parms.Add(new MySqlParameter("pr_permissao_controle_cliente", dto.permissao_controle_cliente));
            parms.Add(new MySqlParameter("pr_permissao_consultar_cliente", dto.permissao_consultar_cliente));
            parms.Add(new MySqlParameter("pr_permissao_cadastrar_funcionarios", dto.permissao_cadastrar_funcionarios));
            parms.Add(new MySqlParameter("pr_permissao_consultar_funcionarios", dto.permissao_consultar_funcionarios));
            parms.Add(new MySqlParameter("pr_permissao_cadastrar_novo_produto", dto.permissao_cadastrar_novo_produto));
            parms.Add(new MySqlParameter("pr_permissao_consultar_produtos", dto.consultar_produtos));
            parms.Add(new MySqlParameter("pr_permissao_consultar_cardapio", dto.permissao_consultar_cardapio));
            parms.Add(new MySqlParameter("pr_permissao_cadastro_novo_cardapio", dto.permissao_cadastro_novo_cardapio));
            parms.Add(new MySqlParameter("pr_permissao_novo_fornecedor", dto.permissao_novo_fornecedor));
            parms.Add(new MySqlParameter("pr_permissao_consultar_fornecedor", dto.permissao_consultar_fornecedor));
            parms.Add(new MySqlParameter("pr_permissao_fluxo_de_caixa", dto.permissao_fluxo_de_caixa));
            parms.Add(new MySqlParameter("pr_permissao_fluxo_de_caixa_bt", dto.permissao_fluxo_de_caixa_bt));
            parms.Add(new MySqlParameter("pr_permissao_fluxo_de_caixa_consultar", dto.permissao_fluxo_de_caixa_consultar));
            parms.Add(new MySqlParameter("pr_permissao_folha_de_pagamento", dto.permissao_folha_de_pagamento));
            parms.Add(new MySqlParameter("permissao_folha_de_pagamento_gerar", dto.permissao_folha_de_pagamento_gerar));
            parms.Add(new MySqlParameter("permissao_folha_de_pagamento_consultar", dto.permissao_folha_de_pagamento_consultar));
            parms.Add(new MySqlParameter("permissao_folha_de_pagamento_alterar", dto.permissao_folha_de_pagamento_alterar));
            parms.Add(new MySqlParameter("pr_permissao_funcionario_alterar", dto.permissao_funcionario_alterar));
            parms.Add(new MySqlParameter("pr_permissao_fornecedor_alterar", dto.permissao_fornecedor_alterar));
            parms.Add(new MySqlParameter("pr_permissao_pedido_cadastrar", dto.permissao_pedido_cadastrar));
            parms.Add(new MySqlParameter("pr_permissao_pedido_consultar", dto.permissao_pedido_consultar));
            parms.Add(new MySqlParameter("pr_permissao_produto_alterar", dto.permissao_produto_alterar));
            parms.Add(new MySqlParameter("pr_permissao_cardapio_alterar", dto.permissao_cardapio_alterar));
            parms.Add(new MySqlParameter("pr_permissao_estoque_alterar", dto.permissao_estoque_alterar));
            parms.Add(new MySqlParameter("pr_permissao_pedido", dto.permissao_pedido));
            parms.Add(new MySqlParameter("sx_sexo", dto.Sexo));
            parms.Add(new MySqlParameter("tl_telefone", dto.Telefone));
            parms.Add(new MySqlParameter("cl_celular", dto.Celular));
            parms.Add(new MySqlParameter("cp_cep", dto.CEP));
            parms.Add(new MySqlParameter("rua", dto.Rua));
            parms.Add(new MySqlParameter("br_bairro", dto.Bairro));
            parms.Add(new MySqlParameter("nr_numero_da_casa", dto.numerocasa));
            parms.Add(new MySqlParameter("st_estado_uf", dto.Estado));
            parms.Add(new MySqlParameter("cd_cidade", dto.Cidade));


            DataBase db = new DataBase();
            db.ExecuteInsertScript(script, parms);
        }

        public void Remover(int Id)
        {
            string script =
            @"DELETE FROM tb_cadastro_funcionario WHERE id_cadastro_funcionario = @id_cadastro_funcionario";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_cadastro_funcionario", Id));

            DataBase db = new DataBase();
            db.ExecuteInsertScript(script, parms);

        }

        public List<FuncionarioDTO> Listar()
        {
            string script = @"select * from tb_cadastro_funcionario";
            List<MySqlParameter> parms = new List<MySqlParameter>();
            DataBase db = new DataBase();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);
            List<FuncionarioDTO> lista = new List<FuncionarioDTO>();
            while (reader.Read())
            {
                FuncionarioDTO dto = new FuncionarioDTO();
                dto.Id_funcionario = reader.GetInt32("id_cadastro_funcionario");
                dto.nomecompleto = reader.GetString("nm_nome_completo");
                dto.usuario = reader.GetString("user_usuario");
                dto.senha = reader.GetString("sn_senha");
                dto.cargo = reader.GetString("cg_cargo");
                dto.RG = reader.GetString("rg");
                dto.Nascimento = reader.GetDateTime("dt_data_nascimento");
                dto.cpf = reader.GetString("cf_cpf");
                dto.cadastro = reader.GetDateTime("dt_data_cadastro");
                dto.permissao_funcionarios = reader.GetBoolean("pr_permissao_funcionarios");
                dto.permissao_produtos = reader.GetBoolean("pr_permissao_produto");
                dto.permissao_cardapio = reader.GetBoolean("pr_permissao_cardapio");
                dto.permissao_fornecedor = reader.GetBoolean("pr_permissao_fornecedor");
                dto.permissao_vinculardadosdecardapio_e_produto = reader.GetBoolean("pr_permissao_cadastrar");
                dto.permissao_estoque = reader.GetBoolean("pr_permissao_estoque");
                dto.permissao_atualizar_estoque = reader.GetBoolean("pr_permissao_atualizar_estoque");
                dto.permissao_consultar_estoque = reader.GetBoolean("pr_permissao_consultar_estoque");
                dto.permissao_pedido_fornecedor = reader.GetBoolean("pr_permissao_pedido_fornecedor");
                dto.permissao_cliente = reader.GetBoolean("pr_permissao_cliente");
                dto.permissao_controle_cliente = reader.GetBoolean("pr_permissao_controle_cliente");
                dto.permissao_consultar_cliente = reader.GetBoolean("pr_permissao_consultar_cliente");
                dto.permissao_cadastrar_funcionarios = reader.GetBoolean("pr_permissao_cadastrar_funcionarios");
                dto.permissao_consultar_funcionarios = reader.GetBoolean("pr_permissao_consultar_funcionarios");
                dto.permissao_cadastrar_novo_produto = reader.GetBoolean("pr_permissao_cadastrar_novo_produto");
                dto.consultar_produtos = reader.GetBoolean("pr_permissao_consultar_produtos");
                dto.permissao_consultar_cardapio = reader.GetBoolean("pr_permissao_consultar_cardapio");
                dto.permissao_cadastro_novo_cardapio = reader.GetBoolean("pr_permissao_cadastro_novo_cardapio");
                dto.permissao_novo_fornecedor = reader.GetBoolean("pr_permissao_novo_fornecedor");
                dto.permissao_consultar_fornecedor = reader.GetBoolean("pr_permissao_consultar_fornecedor");
                dto.permissao_fluxo_de_caixa = reader.GetBoolean("pr_permissao_fluxo_de_caixa");
                dto.permissao_fluxo_de_caixa_bt = reader.GetBoolean("pr_permissao_fluxo_de_caixa_bt");
                dto.permissao_fluxo_de_caixa_consultar = reader.GetBoolean("pr_permissao_fluxo_de_caixa_consultar");
                dto.permissao_folha_de_pagamento = reader.GetBoolean("pr_permissao_folha_de_pagamento");

                dto.permissao_folha_de_pagamento_gerar = reader.GetBoolean("permissao_folha_de_pagamento_gerar");
                dto.permissao_folha_de_pagamento_consultar = reader.GetBoolean("permissao_folha_de_pagamento_consultar");
                dto.permissao_folha_de_pagamento_alterar = reader.GetBoolean("permissao_folha_de_pagamento_alterar");

                dto.permissao_funcionario_alterar = reader.GetBoolean("pr_permissao_funcionario_alterar");
                dto.permissao_fornecedor_alterar = reader.GetBoolean("pr_permissao_fornecedor_alterar");
                dto.permissao_estoque_alterar = reader.GetBoolean("pr_permissao_estoque_alterar");
                dto.permissao_pedido_cadastrar = reader.GetBoolean("pr_permissao_pedido_cadastrar");
                dto.permissao_pedido_consultar = reader.GetBoolean("pr_permissao_pedido_consultar");
                dto.permissao_produto_alterar = reader.GetBoolean("pr_permissao_produto_alterar");
                dto.permissao_cardapio_alterar = reader.GetBoolean("pr_permissao_cardapio_alterar");
                dto.permissao_pedido = reader.GetBoolean("pr_permissao_pedido");
                dto.Sexo = reader.GetString("sx_sexo");
                dto.Telefone = reader.GetString("tl_telefone");
                dto.Celular = reader.GetString("cl_celular");
                dto.CEP = reader.GetString("cp_cep");
                dto.Rua = reader.GetString("rua");
                dto.Bairro = reader.GetString("br_bairro");
                dto.numerocasa = reader.GetString("nr_numero_da_casa");
                dto.Cidade = reader.GetString("cd_cidade");
                dto.Estado = reader.GetString("st_estado_uf");
                lista.Add(dto);
            }
            reader.Close();
            return lista;

        }

        public FuncionarioDTO Login(string usuario, string senha)
        {
            string script = @"select * from tb_cadastro_funcionario where
                            user_usuario = @user_usuario and
                            sn_senha = @sn_senha";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("user_usuario", usuario));
            parms.Add(new MySqlParameter("sn_senha", senha));

            DataBase db = new DataBase();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            FuncionarioDTO funcionario = null;

            if (reader.Read())
            {
                funcionario = new FuncionarioDTO();
                funcionario.Id_funcionario = reader.GetInt32("id_cadastro_funcionario");
                funcionario.nomecompleto = reader.GetString("nm_nome_completo");
                funcionario.usuario = reader.GetString("user_usuario");
                funcionario.senha = reader.GetString("sn_senha");
                funcionario.cargo = reader.GetString("cg_cargo");
                funcionario.RG = reader.GetString("rg");
                funcionario.Nascimento = reader.GetDateTime("dt_data_nascimento");
                funcionario.cpf = reader.GetString("cf_cpf");
                funcionario.cadastro = reader.GetDateTime("dt_data_cadastro");
                funcionario.permissao_funcionarios = reader.GetBoolean("pr_permissao_funcionarios");
                funcionario.permissao_produtos = reader.GetBoolean("pr_permissao_produto");
                funcionario.permissao_cardapio = reader.GetBoolean("pr_permissao_cardapio");
                funcionario.permissao_fornecedor = reader.GetBoolean("pr_permissao_fornecedor");
                funcionario.permissao_vinculardadosdecardapio_e_produto = reader.GetBoolean("pr_permissao_cadastrar");
                funcionario.permissao_estoque = reader.GetBoolean("pr_permissao_estoque");
                funcionario.permissao_atualizar_estoque = reader.GetBoolean("pr_permissao_atualizar_estoque");
                funcionario.permissao_consultar_estoque = reader.GetBoolean("pr_permissao_consultar_estoque");
                funcionario.permissao_pedido_fornecedor = reader.GetBoolean("pr_permissao_pedido_fornecedor");
                funcionario.permissao_cliente = reader.GetBoolean("pr_permissao_cliente");
                funcionario.permissao_controle_cliente = reader.GetBoolean("pr_permissao_controle_cliente");
                funcionario.permissao_consultar_cliente = reader.GetBoolean("pr_permissao_consultar_cliente");
                funcionario.permissao_cadastrar_funcionarios = reader.GetBoolean("pr_permissao_cadastrar_funcionarios");
                funcionario.permissao_consultar_funcionarios = reader.GetBoolean("pr_permissao_consultar_funcionarios");
                funcionario.permissao_cadastrar_novo_produto = reader.GetBoolean("pr_permissao_cadastrar_novo_produto");
                funcionario.consultar_produtos = reader.GetBoolean("pr_permissao_consultar_produtos");
                funcionario.permissao_consultar_cardapio = reader.GetBoolean("pr_permissao_consultar_cardapio");
                funcionario.permissao_cadastro_novo_cardapio = reader.GetBoolean("pr_permissao_cadastro_novo_cardapio");
                funcionario.permissao_novo_fornecedor = reader.GetBoolean("pr_permissao_novo_fornecedor");
                funcionario.permissao_consultar_fornecedor = reader.GetBoolean("pr_permissao_consultar_fornecedor");
                funcionario.permissao_fluxo_de_caixa = reader.GetBoolean("pr_permissao_fluxo_de_caixa");
                funcionario.permissao_fluxo_de_caixa_bt = reader.GetBoolean("pr_permissao_fluxo_de_caixa_bt");
                funcionario.permissao_fluxo_de_caixa_consultar = reader.GetBoolean("pr_permissao_fluxo_de_caixa_consultar");
                funcionario.permissao_folha_de_pagamento = reader.GetBoolean("pr_permissao_folha_de_pagamento");
                funcionario.permissao_folha_de_pagamento_gerar = reader.GetBoolean("permissao_folha_de_pagamento_gerar");
                funcionario.permissao_folha_de_pagamento_consultar = reader.GetBoolean("permissao_folha_de_pagamento_consultar");
                funcionario.permissao_folha_de_pagamento_alterar = reader.GetBoolean("permissao_folha_de_pagamento_alterar");
                funcionario.permissao_funcionario_alterar = reader.GetBoolean("pr_permissao_funcionario_alterar");
                funcionario.permissao_fornecedor_alterar = reader.GetBoolean("pr_permissao_fornecedor_alterar");
                funcionario.permissao_estoque_alterar = reader.GetBoolean("pr_permissao_estoque_alterar");
                funcionario.permissao_pedido_cadastrar = reader.GetBoolean("pr_permissao_pedido_cadastrar");
                funcionario.permissao_pedido_consultar = reader.GetBoolean("pr_permissao_pedido_consultar");
                funcionario.permissao_produto_alterar = reader.GetBoolean("pr_permissao_produto_alterar");
                funcionario.permissao_cardapio_alterar = reader.GetBoolean("pr_permissao_cardapio_alterar");
                funcionario.permissao_pedido = reader.GetBoolean("pr_permissao_pedido");
                funcionario.Sexo = reader.GetString("sx_sexo");
                funcionario.Telefone = reader.GetString("tl_telefone");
                funcionario.Celular = reader.GetString("cl_celular");
                funcionario.CEP = reader.GetString("cp_cep");
                funcionario.Rua = reader.GetString("rua");
                funcionario.Bairro = reader.GetString("br_bairro");
                funcionario.numerocasa = reader.GetString("nr_numero_da_casa");
                funcionario.Cidade = reader.GetString("cd_cidade");
                funcionario.Estado = reader.GetString("st_estado_uf");

            }
            reader.Close();
            return funcionario;
        }

        public List<FuncionarioDTO> Consultar(string nome)
        {
            string script = @"select * from tb_cadastro_funcionario where nm_nome_completo like @nm_nome_completo";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_nome_completo", nome + "%"));
            DataBase db = new DataBase();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);
            List<FuncionarioDTO> lista = new List<FuncionarioDTO>();
            while (reader.Read())
            {
                FuncionarioDTO dto = new FuncionarioDTO();
                dto.Id_funcionario = reader.GetInt32("id_cadastro_funcionario");
                dto.nomecompleto = reader.GetString("nm_nome_completo");
                dto.usuario = reader.GetString("user_usuario");
                dto.senha = reader.GetString("sn_senha");
                dto.cargo = reader.GetString("cg_cargo");
                dto.RG = reader.GetString("rg");
                dto.Nascimento = reader.GetDateTime("dt_data_nascimento");
                dto.cpf = reader.GetString("cf_cpf");
                dto.cadastro = reader.GetDateTime("dt_data_cadastro");
                dto.permissao_funcionarios = reader.GetBoolean("pr_permissao_funcionarios");
                dto.permissao_produtos = reader.GetBoolean("pr_permissao_produto");
                dto.permissao_cardapio = reader.GetBoolean("pr_permissao_cardapio");
                dto.permissao_fornecedor = reader.GetBoolean("pr_permissao_fornecedor");
                dto.permissao_vinculardadosdecardapio_e_produto = reader.GetBoolean("pr_permissao_cadastrar");
                dto.permissao_estoque = reader.GetBoolean("pr_permissao_estoque");
                dto.permissao_atualizar_estoque = reader.GetBoolean("pr_permissao_atualizar_estoque");
                dto.permissao_consultar_estoque = reader.GetBoolean("pr_permissao_consultar_estoque");
                dto.permissao_pedido_fornecedor = reader.GetBoolean("pr_permissao_pedido_fornecedor");
                dto.permissao_cliente = reader.GetBoolean("pr_permissao_cliente");
                dto.permissao_controle_cliente = reader.GetBoolean("pr_permissao_controle_cliente");
                dto.permissao_consultar_cliente = reader.GetBoolean("pr_permissao_consultar_cliente");
                dto.permissao_cadastrar_funcionarios = reader.GetBoolean("pr_permissao_cadastrar_funcionarios");
                dto.permissao_consultar_funcionarios = reader.GetBoolean("pr_permissao_consultar_funcionarios");
                dto.permissao_cadastrar_novo_produto = reader.GetBoolean("pr_permissao_cadastrar_novo_produto");
                dto.consultar_produtos = reader.GetBoolean("pr_permissao_consultar_produtos");
                dto.permissao_consultar_cardapio = reader.GetBoolean("pr_permissao_consultar_cardapio");
                dto.permissao_cadastro_novo_cardapio = reader.GetBoolean("pr_permissao_cadastro_novo_cardapio");
                dto.permissao_novo_fornecedor = reader.GetBoolean("pr_permissao_novo_fornecedor");
                dto.permissao_consultar_fornecedor = reader.GetBoolean("pr_permissao_consultar_fornecedor");
                dto.permissao_fluxo_de_caixa = reader.GetBoolean("pr_permissao_fluxo_de_caixa");
                dto.permissao_fluxo_de_caixa_bt = reader.GetBoolean("pr_permissao_fluxo_de_caixa_bt");
                dto.permissao_fluxo_de_caixa_consultar = reader.GetBoolean("pr_permissao_fluxo_de_caixa_consultar");
                dto.permissao_folha_de_pagamento = reader.GetBoolean("pr_permissao_folha_de_pagamento");
                dto.permissao_folha_de_pagamento_gerar = reader.GetBoolean("permissao_folha_de_pagamento_gerar");
                dto.permissao_folha_de_pagamento_consultar = reader.GetBoolean("permissao_folha_de_pagamento_consultar");
                dto.permissao_folha_de_pagamento_alterar = reader.GetBoolean("permissao_folha_de_pagamento_alterar");
                dto.permissao_funcionario_alterar = reader.GetBoolean("pr_permissao_funcionario_alterar");
                dto.permissao_fornecedor_alterar = reader.GetBoolean("pr_permissao_fornecedor_alterar");
                dto.permissao_estoque_alterar = reader.GetBoolean("pr_permissao_estoque_alterar");
                dto.permissao_pedido_cadastrar = reader.GetBoolean("pr_permissao_pedido_cadastrar");
                dto.permissao_pedido_consultar = reader.GetBoolean("pr_permissao_pedido_consultar");
                dto.permissao_produto_alterar = reader.GetBoolean("pr_permissao_produto_alterar");
                dto.permissao_cardapio_alterar = reader.GetBoolean("pr_permissao_cardapio_alterar");
                dto.permissao_pedido = reader.GetBoolean("pr_permissao_pedido");
                dto.Sexo = reader.GetString("sx_sexo");
                dto.Telefone = reader.GetString("tl_telefone");
                dto.Celular = reader.GetString("cl_celular");
                dto.CEP = reader.GetString("cp_cep");
                dto.Rua = reader.GetString("rua");
                dto.Bairro = reader.GetString("br_bairro");
                dto.numerocasa = reader.GetString("nr_numero_da_casa");
                dto.Cidade = reader.GetString("cd_cidade");
                dto.Estado = reader.GetString("st_estado_uf");

                lista.Add(dto);
            }
            
            reader.Close();
            return lista;

        }

        public List<FuncionarioDTO> Buscar(int id)
        {
            string script = @"select * from tb_cadastro_funcionario where id_cadastro_funcionario like @id_cadastro_funcionario";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_cadastro_funcionario", id + "%"));
            DataBase db = new DataBase();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);
            List<FuncionarioDTO> lista = new List<FuncionarioDTO>();
            while (reader.Read())
            {
                FuncionarioDTO dto = new FuncionarioDTO();
                dto.Id_funcionario = reader.GetInt32("id_cadastro_funcionario");
                dto.nomecompleto = reader.GetString("nm_nome_completo");
                dto.usuario = reader.GetString("user_usuario");
                dto.senha = reader.GetString("sn_senha");
                dto.cargo = reader.GetString("cg_cargo");
                dto.RG = reader.GetString("rg");
                dto.Nascimento = reader.GetDateTime("dt_data_nascimento");
                dto.cpf = reader.GetString("cf_cpf");
                dto.cadastro = reader.GetDateTime("dt_data_cadastro");
                dto.permissao_funcionarios = reader.GetBoolean("pr_permissao_funcionarios");
                dto.permissao_produtos = reader.GetBoolean("pr_permissao_produto");
                dto.permissao_cardapio = reader.GetBoolean("pr_permissao_cardapio");
                dto.permissao_fornecedor = reader.GetBoolean("pr_permissao_fornecedor");
                dto.permissao_vinculardadosdecardapio_e_produto = reader.GetBoolean("pr_permissao_cadastrar");
                dto.permissao_estoque = reader.GetBoolean("pr_permissao_estoque");
                dto.permissao_atualizar_estoque = reader.GetBoolean("pr_permissao_atualizar_estoque");
                dto.permissao_consultar_estoque = reader.GetBoolean("pr_permissao_consultar_estoque");
                dto.permissao_pedido_fornecedor = reader.GetBoolean("pr_permissao_pedido_fornecedor");
                dto.permissao_cliente = reader.GetBoolean("pr_permissao_cliente");
                dto.permissao_controle_cliente = reader.GetBoolean("pr_permissao_controle_cliente");
                dto.permissao_consultar_cliente = reader.GetBoolean("pr_permissao_consultar_cliente");
                dto.permissao_cadastrar_funcionarios = reader.GetBoolean("pr_permissao_cadastrar_funcionarios");
                dto.permissao_consultar_funcionarios = reader.GetBoolean("pr_permissao_consultar_funcionarios");
                dto.permissao_cadastrar_novo_produto = reader.GetBoolean("pr_permissao_cadastrar_novo_produto");
                dto.consultar_produtos = reader.GetBoolean("pr_permissao_consultar_produtos");
                dto.permissao_consultar_cardapio = reader.GetBoolean("pr_permissao_consultar_cardapio");
                dto.permissao_cadastro_novo_cardapio = reader.GetBoolean("pr_permissao_cadastro_novo_cardapio");
                dto.permissao_novo_fornecedor = reader.GetBoolean("pr_permissao_novo_fornecedor");
                dto.permissao_consultar_fornecedor = reader.GetBoolean("pr_permissao_consultar_fornecedor");
                dto.permissao_fluxo_de_caixa = reader.GetBoolean("pr_permissao_fluxo_de_caixa");
                dto.permissao_fluxo_de_caixa_bt = reader.GetBoolean("pr_permissao_fluxo_de_caixa_bt");
                dto.permissao_fluxo_de_caixa_consultar = reader.GetBoolean("pr_permissao_fluxo_de_caixa_consultar");
                dto.permissao_folha_de_pagamento = reader.GetBoolean("pr_permissao_folha_de_pagamento");
                dto.permissao_folha_de_pagamento_gerar = reader.GetBoolean("permissao_folha_de_pagamento_gerar");
                dto.permissao_folha_de_pagamento_consultar = reader.GetBoolean("permissao_folha_de_pagamento_consultar");
                dto.permissao_folha_de_pagamento_alterar = reader.GetBoolean("permissao_folha_de_pagamento_alterar");
                dto.permissao_funcionario_alterar = reader.GetBoolean("pr_permissao_funcionario_alterar");
                dto.permissao_fornecedor_alterar = reader.GetBoolean("pr_permissao_fornecedor_alterar");
                dto.permissao_estoque_alterar = reader.GetBoolean("pr_permissao_estoque_alterar");
                dto.permissao_pedido_cadastrar = reader.GetBoolean("pr_permissao_pedido_cadastrar");
                dto.permissao_pedido_consultar = reader.GetBoolean("pr_permissao_pedido_consultar");
                dto.permissao_produto_alterar = reader.GetBoolean("pr_permissao_produto_alterar");
                dto.permissao_cardapio_alterar = reader.GetBoolean("pr_permissao_cardapio_alterar");
                dto.permissao_pedido = reader.GetBoolean("pr_permissao_pedido");
                dto.Sexo = reader.GetString("sx_sexo");
                dto.Telefone = reader.GetString("tl_telefone");
                dto.Celular = reader.GetString("cl_celular");
                dto.CEP = reader.GetString("cp_cep");
                dto.Rua = reader.GetString("rua");
                dto.Bairro = reader.GetString("br_bairro");
                dto.numerocasa = reader.GetString("nr_numero_da_casa");
                dto.Cidade = reader.GetString("cd_cidade");
                dto.Estado = reader.GetString("st_estado_uf");
                lista.Add(dto);
            }
            reader.Close();
            return lista;
        }

        public List<FuncionarioDTO> ConsultarporUsuario(string usuario)
        {
            string script = @"select * from tb_cadastro_funcionario where user_usuario like @user_usuario";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("user_usuario", usuario + "%"));
            DataBase db = new DataBase();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);
            List<FuncionarioDTO> lista = new List<FuncionarioDTO>();
            while (reader.Read())
            {
                FuncionarioDTO dto = new FuncionarioDTO();
                dto.Id_funcionario = reader.GetInt32("id_cadastro_funcionario");
                dto.nomecompleto = reader.GetString("nm_nome_completo");
                dto.usuario = reader.GetString("user_usuario");
                dto.senha = reader.GetString("sn_senha");
                dto.cargo = reader.GetString("cg_cargo");
                dto.RG = reader.GetString("rg");
                dto.Nascimento = reader.GetDateTime("dt_data_nascimento");
                dto.cpf = reader.GetString("cf_cpf");
                dto.cadastro = reader.GetDateTime("dt_data_cadastro");
                dto.permissao_funcionarios = reader.GetBoolean("pr_permissao_funcionarios");
                dto.permissao_produtos = reader.GetBoolean("pr_permissao_produto");
                dto.permissao_cardapio = reader.GetBoolean("pr_permissao_cardapio");
                dto.permissao_fornecedor = reader.GetBoolean("pr_permissao_fornecedor");
                dto.permissao_vinculardadosdecardapio_e_produto = reader.GetBoolean("pr_permissao_cadastrar");
                dto.permissao_estoque = reader.GetBoolean("pr_permissao_estoque");
                dto.permissao_atualizar_estoque = reader.GetBoolean("pr_permissao_atualizar_estoque");
                dto.permissao_consultar_estoque = reader.GetBoolean("pr_permissao_consultar_estoque");
                dto.permissao_pedido_fornecedor = reader.GetBoolean("pr_permissao_pedido_fornecedor");
                dto.permissao_cliente = reader.GetBoolean("pr_permissao_cliente");
                dto.permissao_controle_cliente = reader.GetBoolean("pr_permissao_controle_cliente");
                dto.permissao_consultar_cliente = reader.GetBoolean("pr_permissao_consultar_cliente");
                dto.permissao_cadastrar_funcionarios = reader.GetBoolean("pr_permissao_cadastrar_funcionarios");
                dto.permissao_consultar_funcionarios = reader.GetBoolean("pr_permissao_consultar_funcionarios");
                dto.permissao_cadastrar_novo_produto = reader.GetBoolean("pr_permissao_cadastrar_novo_produto");
                dto.consultar_produtos = reader.GetBoolean("pr_permissao_consultar_produtos");
                dto.permissao_consultar_cardapio = reader.GetBoolean("pr_permissao_consultar_cardapio");
                dto.permissao_cadastro_novo_cardapio = reader.GetBoolean("pr_permissao_cadastro_novo_cardapio");
                dto.permissao_novo_fornecedor = reader.GetBoolean("pr_permissao_novo_fornecedor");
                dto.permissao_consultar_fornecedor = reader.GetBoolean("pr_permissao_consultar_fornecedor");
                dto.permissao_fluxo_de_caixa = reader.GetBoolean("pr_permissao_fluxo_de_caixa");
                dto.permissao_fluxo_de_caixa_bt = reader.GetBoolean("pr_permissao_fluxo_de_caixa_bt");
                dto.permissao_fluxo_de_caixa_consultar = reader.GetBoolean("pr_permissao_fluxo_de_caixa_consultar");
                dto.permissao_folha_de_pagamento = reader.GetBoolean("pr_permissao_folha_de_pagamento");
                dto.permissao_folha_de_pagamento_gerar = reader.GetBoolean("permissao_folha_de_pagamento_gerar");
                dto.permissao_folha_de_pagamento_consultar = reader.GetBoolean("permissao_folha_de_pagamento_consultar");
                dto.permissao_folha_de_pagamento_alterar = reader.GetBoolean("permissao_folha_de_pagamento_alterar");
                dto.permissao_funcionario_alterar = reader.GetBoolean("pr_permissao_funcionario_alterar");
                dto.permissao_fornecedor_alterar = reader.GetBoolean("pr_permissao_fornecedor_alterar");
                dto.permissao_estoque_alterar = reader.GetBoolean("pr_permissao_estoque_alterar");
                dto.permissao_pedido_cadastrar = reader.GetBoolean("pr_permissao_pedido_cadastrar");
                dto.permissao_pedido_consultar = reader.GetBoolean("pr_permissao_pedido_consultar");
                dto.permissao_produto_alterar = reader.GetBoolean("pr_permissao_produto_alterar");
                dto.permissao_cardapio_alterar = reader.GetBoolean("pr_permissao_cardapio_alterar");
                dto.permissao_pedido = reader.GetBoolean("pr_permissao_pedido");
                dto.Sexo = reader.GetString("sx_sexo");
                dto.Telefone = reader.GetString("tl_telefone");
                dto.Celular = reader.GetString("cl_celular");
                dto.CEP = reader.GetString("cp_cep");
                dto.Rua = reader.GetString("rua");
                dto.Bairro = reader.GetString("br_bairro");
                dto.numerocasa = reader.GetString("nr_numero_da_casa");
                dto.Cidade = reader.GetString("cd_cidade");
                dto.Estado = reader.GetString("st_estado_uf");

                lista.Add(dto);
            }

            reader.Close();
            return lista;

        }

        public void AlterarSenha(int id, string senha)
        {
            string script = @"update tb_cadastro_funcionario set sn_senha = @sn_senha WHERE id_cadastro_funcionario = @id_cadastro_funcionario";
            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_cadastro_funcionario", id));
            parms.Add(new MySqlParameter("sn_senha", senha));
            DataBase db = new DataBase();
            db.ExecuteInsertScript(script, parms);
        }

        public List<FuncionarioDTO> ConsultarCPF(string CPF)
        {
            string script = @"select * from tb_cadastro_funcionario where cf_cpf like @cf_cpf";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("cf_cpf", CPF + "%"));
            DataBase db = new DataBase();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);
            List<FuncionarioDTO> lista = new List<FuncionarioDTO>();
            while (reader.Read())
            {
                FuncionarioDTO dto = new FuncionarioDTO();
                dto.Id_funcionario = reader.GetInt32("id_cadastro_funcionario");
                dto.nomecompleto = reader.GetString("nm_nome_completo");
                dto.usuario = reader.GetString("user_usuario");
                dto.senha = reader.GetString("sn_senha");
                dto.cargo = reader.GetString("cg_cargo");
                dto.RG = reader.GetString("rg");
                dto.Nascimento = reader.GetDateTime("dt_data_nascimento");
                dto.cpf = reader.GetString("cf_cpf");
                dto.cadastro = reader.GetDateTime("dt_data_cadastro");
                dto.permissao_funcionarios = reader.GetBoolean("pr_permissao_funcionarios");
                dto.permissao_produtos = reader.GetBoolean("pr_permissao_produto");
                dto.permissao_cardapio = reader.GetBoolean("pr_permissao_cardapio");
                dto.permissao_fornecedor = reader.GetBoolean("pr_permissao_fornecedor");
                dto.permissao_vinculardadosdecardapio_e_produto = reader.GetBoolean("pr_permissao_cadastrar");
                dto.permissao_estoque = reader.GetBoolean("pr_permissao_estoque");
                dto.permissao_atualizar_estoque = reader.GetBoolean("pr_permissao_atualizar_estoque");
                dto.permissao_consultar_estoque = reader.GetBoolean("pr_permissao_consultar_estoque");
                dto.permissao_pedido_fornecedor = reader.GetBoolean("pr_permissao_pedido_fornecedor");
                dto.permissao_cliente = reader.GetBoolean("pr_permissao_cliente");
                dto.permissao_controle_cliente = reader.GetBoolean("pr_permissao_controle_cliente");
                dto.permissao_consultar_cliente = reader.GetBoolean("pr_permissao_consultar_cliente");
                dto.permissao_cadastrar_funcionarios = reader.GetBoolean("pr_permissao_cadastrar_funcionarios");
                dto.permissao_consultar_funcionarios = reader.GetBoolean("pr_permissao_consultar_funcionarios");
                dto.permissao_cadastrar_novo_produto = reader.GetBoolean("pr_permissao_cadastrar_novo_produto");
                dto.consultar_produtos = reader.GetBoolean("pr_permissao_consultar_produtos");
                dto.permissao_consultar_cardapio = reader.GetBoolean("pr_permissao_consultar_cardapio");
                dto.permissao_cadastro_novo_cardapio = reader.GetBoolean("pr_permissao_cadastro_novo_cardapio");
                dto.permissao_novo_fornecedor = reader.GetBoolean("pr_permissao_novo_fornecedor");
                dto.permissao_consultar_fornecedor = reader.GetBoolean("pr_permissao_consultar_fornecedor");
                dto.permissao_fluxo_de_caixa = reader.GetBoolean("pr_permissao_fluxo_de_caixa");
                dto.permissao_fluxo_de_caixa_bt = reader.GetBoolean("pr_permissao_fluxo_de_caixa_bt");
                dto.permissao_fluxo_de_caixa_consultar = reader.GetBoolean("pr_permissao_fluxo_de_caixa_consultar");
                dto.permissao_folha_de_pagamento = reader.GetBoolean("pr_permissao_folha_de_pagamento");
                dto.permissao_folha_de_pagamento_gerar = reader.GetBoolean("permissao_folha_de_pagamento_gerar");
                dto.permissao_folha_de_pagamento_consultar = reader.GetBoolean("permissao_folha_de_pagamento_consultar");
                dto.permissao_folha_de_pagamento_alterar = reader.GetBoolean("permissao_folha_de_pagamento_alterar");
                dto.permissao_funcionario_alterar = reader.GetBoolean("pr_permissao_funcionario_alterar");
                dto.permissao_fornecedor_alterar = reader.GetBoolean("pr_permissao_fornecedor_alterar");
                dto.permissao_estoque_alterar = reader.GetBoolean("pr_permissao_estoque_alterar");
                dto.permissao_pedido_cadastrar = reader.GetBoolean("pr_permissao_pedido_cadastrar");
                dto.permissao_pedido_consultar = reader.GetBoolean("pr_permissao_pedido_consultar");
                dto.permissao_produto_alterar = reader.GetBoolean("pr_permissao_produto_alterar");
                dto.permissao_cardapio_alterar = reader.GetBoolean("pr_permissao_cardapio_alterar");
                dto.permissao_pedido = reader.GetBoolean("pr_permissao_pedido");
                dto.Sexo = reader.GetString("sx_sexo");
                dto.Telefone = reader.GetString("tl_telefone");
                dto.Celular = reader.GetString("cl_celular");
                dto.CEP = reader.GetString("cp_cep");
                dto.Rua = reader.GetString("rua");
                dto.Bairro = reader.GetString("br_bairro");
                dto.numerocasa = reader.GetString("nr_numero_da_casa");
                dto.Cidade = reader.GetString("cd_cidade");
                dto.Estado = reader.GetString("st_estado_uf");

                lista.Add(dto);
            }

            reader.Close();
            return lista;

        }
    }
}
