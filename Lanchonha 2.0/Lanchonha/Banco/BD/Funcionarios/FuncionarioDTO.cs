﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lanchonha.Banco.BD.Funcionarios
{
    public class FuncionarioDTO
    {
        public int Id_funcionario { get; set; }
        public string nomecompleto { get; set; }
        public string usuario { get; set; }
        public string senha { get; set; }
        public string cargo { get; set; }
        public string RG { get; set; }
        public DateTime Nascimento { get; set; }
        public string cpf { get; set; }
        public DateTime cadastro { get; set; }
        public string Sexo { get; set; }
        public string Telefone { get; set; }
        public string Celular { get; set; }
        public string CEP { get; set; }
        public string Rua { get; set; }
        public string Bairro { get; set; }
        public string Estado { get; set; }
        public string Cidade { get; set; }
        public string numerocasa { get; set; }

        public bool permissao_funcionarios { get; set; }
        public bool permissao_cadastrar_funcionarios { get; set; }
        public bool permissao_consultar_funcionarios { get; set; }
        public bool permissao_funcionario_alterar { get; set; }

        public bool permissao_produtos { get; set; }
        public bool permissao_cadastrar_novo_produto { get; set; }
        public bool consultar_produtos { get; set; }
        public bool permissao_produto_alterar { get; set; }

        
        public bool permissao_fornecedor { get; set; }
        public bool permissao_fornecedor_alterar { get; set; }
        public bool permissao_pedido_fornecedor { get; set; }
        public bool permissao_novo_fornecedor { get; set; }
        public bool permissao_consultar_fornecedor { get; set; }

        public bool permissao_estoque { get; set; }
        public bool permissao_atualizar_estoque { get; set; }
        public bool permissao_consultar_estoque { get; set; }
        public bool permissao_estoque_alterar { get; set; }

        public bool permissao_cliente { get; set; }
        public bool permissao_controle_cliente { get; set; }
        public bool permissao_consultar_cliente { get; set; }

        public bool permissao_cardapio { get; set; }
        public bool permissao_consultar_cardapio { get; set; }
        public bool permissao_cadastro_novo_cardapio { get; set; }
        public bool permissao_cardapio_alterar { get; set; }
        public bool permissao_vinculardadosdecardapio_e_produto { get; set; }

        public bool permissao_fluxo_de_caixa { get; set; }
        public bool permissao_fluxo_de_caixa_bt { get; set; }
        public bool permissao_fluxo_de_caixa_consultar { get; set; }

        public bool permissao_folha_de_pagamento { get; set; }
        public bool permissao_folha_de_pagamento_gerar { get; set; }
        public bool permissao_folha_de_pagamento_consultar { get; set; }
        public bool permissao_folha_de_pagamento_alterar { get; set; }




        public bool permissao_pedido { get; set; }
        public bool permissao_pedido_cadastrar { get; set; }
        public bool permissao_pedido_consultar { get; set; }       
       

        

    }
    
}
