﻿using Lanchonha.Banco.Banco;
using Lanchonha.Utilitarios;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lanchonha.Banco.BD.Funcionarios
{
    public class Business_Funcionarios
    {

        public int Salvar(FuncionarioDTO dto)
        {
            
            CPF cpf = new CPF();
            SHA256Cript crip = new SHA256Cript();
            exReg reg = new exReg();
            reg.ValidarNome(dto.nomecompleto);
            reg.ValidarSenha(dto.senha);
            reg.ValidarTelefoneCelular(dto.Celular);
            reg.ValidarTelefoneFixo(dto.Telefone);
            cpf.ValidarCPF(dto.cpf);

            if (String.IsNullOrEmpty(dto.nomecompleto) || dto.nomecompleto.Trim().Length == 0)
            {
                throw new ArgumentException("O campo Nome só tem espaços.");
            }
            if (String.IsNullOrEmpty(dto.cargo) || dto.cargo.Trim().Length == 0)
            {
                throw new ArgumentException("O campo Cargo só tem espaços.");
            }
            if (String.IsNullOrEmpty(dto.usuario) || dto.usuario.Trim().Length == 0)
            {
                throw new ArgumentException("O campo Usuário só tem espaços.");
            }
            if (String.IsNullOrEmpty(dto.senha) || dto.senha.Trim().Length == 0)
            {
                throw new ArgumentException("O campo Senha só tem espaços.");
            }
            if (String.IsNullOrEmpty(dto.Estado) || dto.Estado.Trim().Length == 0)
            {
                throw new ArgumentException("O campo Estado só tem espaços.");
            }
            if (String.IsNullOrEmpty(dto.Cidade) || dto.Cidade.Trim().Length == 0)
            {
                throw new ArgumentException("O campo Cidade só tem espaços.");
            }
            if (String.IsNullOrEmpty(dto.Bairro) || dto.Bairro.Trim().Length == 0)
            {
                throw new ArgumentException("O campo Bairro só tem espaços.");
            }
            if (String.IsNullOrEmpty(dto.Rua) || dto.Rua.Trim().Length == 0)
            {
                throw new ArgumentException("O campo Rua só tem espaços.");
            }
            if (String.IsNullOrEmpty(dto.numerocasa) || dto.numerocasa.Trim().Length == 0)
            {
                throw new ArgumentException("O campo Número da casa só tem espaços.");
            }
            if (dto.numerocasa == string.Empty)
                throw new ArgumentException("O campo n° da casa está em branco, por favor preencher.");
            if (dto.usuario == string.Empty)
                throw new ArgumentException("O campo usuário está em branco, por favor preencher.");
            if (dto.senha == string.Empty)
                throw new ArgumentException("O campo senha está em branco, por favor preencher.");
            if (dto.Sexo == string.Empty)
                throw new ArgumentException("O campo sexo está em branco, por favor preencher.");
            if (dto.cpf == string.Empty)
                throw new ArgumentException("O campo CEP está em branco, por favor preencher.");
            
           if (     dto.permissao_funcionarios == false &&
                    dto.permissao_cadastrar_funcionarios == false &&
                    dto.permissao_consultar_funcionarios == false &&
                    dto.permissao_funcionario_alterar == false &&

                    dto.permissao_produtos == false &&
                    dto.permissao_cadastrar_novo_produto == false &&
                    dto.consultar_produtos == false &&
                    dto.permissao_produto_alterar == false &&

                    dto.permissao_fornecedor == false &&
                    dto.permissao_fornecedor_alterar == false &&
                    dto.permissao_pedido_fornecedor == false &&
                    dto.permissao_novo_fornecedor == false &&
                    dto.permissao_consultar_fornecedor == false &&

                    dto.permissao_estoque == false &&
                    dto.permissao_atualizar_estoque == false &&
                    dto.permissao_consultar_estoque == false &&
                    dto.permissao_estoque_alterar == false &&

                    dto.permissao_cliente == false &&
                    dto.permissao_controle_cliente == false &&
                    dto.permissao_consultar_cliente == false &&

                    dto.permissao_cardapio == false &&
                    dto.permissao_consultar_cardapio == false &&
                    dto.permissao_cadastro_novo_cardapio == false &&
                    dto.permissao_cardapio_alterar == false &&
                    dto.permissao_vinculardadosdecardapio_e_produto == false &&

                    dto.permissao_fluxo_de_caixa == false &&
                    dto.permissao_fluxo_de_caixa_bt == false &&
                    dto.permissao_fluxo_de_caixa_consultar == false &&

                    dto.permissao_folha_de_pagamento == false &&
                    dto.permissao_folha_de_pagamento_gerar == false &&
                    dto.permissao_folha_de_pagamento_consultar == false &&
                    dto.permissao_folha_de_pagamento_alterar == false &&

                    dto.permissao_pedido == false &&
                    dto.permissao_pedido_cadastrar == false &&
                    dto.permissao_pedido_consultar == false)
            {
                throw new ArgumentException("Os campos permissões estão em branco, por favor preencher.");
            }

           
            if (dto.senha != null)
            {
                dto.senha= crip.Criptografar(dto.senha);
            }
            
            Database__Funcionarios db = new Database__Funcionarios();
           
            
            return db.Salvar(dto);
        }

        public void Alterar(FuncionarioDTO dto)
        {
            CPF cpf = new CPF();

            exReg reg = new exReg();
            reg.ValidarNome(dto.nomecompleto);
            reg.ValidarSenha(dto.senha);
            reg.ValidarTelefoneCelular(dto.Celular);
            reg.ValidarTelefoneFixo(dto.Telefone);
            cpf.ValidarCPF(dto.cpf);

            if (String.IsNullOrEmpty(dto.nomecompleto) || dto.nomecompleto.Trim().Length == 0)
            {
                throw new ArgumentException("O campo Nome só tem espaços.");
            }
            if (String.IsNullOrEmpty(dto.cargo) || dto.cargo.Trim().Length == 0)
            {
                throw new ArgumentException("O campo Cargo só tem espaços.");
            }
            if (String.IsNullOrEmpty(dto.usuario) || dto.usuario.Trim().Length == 0)
            {
                throw new ArgumentException("O campo Usuário só tem espaços.");
            }
            if (String.IsNullOrEmpty(dto.senha) || dto.senha.Trim().Length == 0)
            {
                throw new ArgumentException("O campo Senha só tem espaços.");
            }
            if (String.IsNullOrEmpty(dto.Estado) || dto.Estado.Trim().Length == 0)
            {
                throw new ArgumentException("O campo Estado só tem espaços.");
            }
            if (String.IsNullOrEmpty(dto.Cidade) || dto.Cidade.Trim().Length == 0)
            {
                throw new ArgumentException("O campo Cidade só tem espaços.");
            }
            if (String.IsNullOrEmpty(dto.Bairro) || dto.Bairro.Trim().Length == 0)
            {
                throw new ArgumentException("O campo Bairro só tem espaços.");
            }
            if (String.IsNullOrEmpty(dto.Rua) || dto.Rua.Trim().Length == 0)
            {
                throw new ArgumentException("O campo Rua só tem espaços.");
            }
            if (String.IsNullOrEmpty(dto.numerocasa) || dto.numerocasa.Trim().Length == 0)
            {
                throw new ArgumentException("O campo Numero da casa só tem espaços.");
            }
            if (dto.numerocasa == string.Empty)
                throw new ArgumentException("O campo n° da casa está em branco, por favor preencher.");
            if (dto.usuario == string.Empty)
                throw new ArgumentException("O campo usuário está em branco, por favor preencher.");
            if (dto.senha == string.Empty)
                throw new ArgumentException("O campo senha está em branco, por favor preencher.");
            if (dto.Sexo == string.Empty)
                throw new ArgumentException("O campo sexo está em branco, por favor preencher.");
            if (dto.cpf == string.Empty)
                throw new ArgumentException("O campo CEP está em branco, por favor preencher.");
            if (dto.permissao_funcionarios == false &&
                   dto.permissao_cadastrar_funcionarios == false &&
                   dto.permissao_consultar_funcionarios == false &&
                   dto.permissao_funcionario_alterar == false &&

                   dto.permissao_produtos == false &&
                   dto.permissao_cadastrar_novo_produto == false &&
                   dto.consultar_produtos == false &&
                   dto.permissao_produto_alterar == false &&

                   dto.permissao_fornecedor == false &&
                   dto.permissao_fornecedor_alterar == false &&
                   dto.permissao_pedido_fornecedor == false &&
                   dto.permissao_novo_fornecedor == false &&
                   dto.permissao_consultar_fornecedor == false &&

                   dto.permissao_estoque == false &&
                   dto.permissao_atualizar_estoque == false &&
                   dto.permissao_consultar_estoque == false &&
                   dto.permissao_estoque_alterar == false &&

                   dto.permissao_cliente == false &&
                   dto.permissao_controle_cliente == false &&
                   dto.permissao_consultar_cliente == false &&

                   dto.permissao_cardapio == false &&
                   dto.permissao_consultar_cardapio == false &&
                   dto.permissao_cadastro_novo_cardapio == false &&
                   dto.permissao_cardapio_alterar == false &&
                   dto.permissao_vinculardadosdecardapio_e_produto == false &&

                   dto.permissao_fluxo_de_caixa == false &&
                   dto.permissao_fluxo_de_caixa_bt == false &&
                   dto.permissao_fluxo_de_caixa_consultar == false &&

                   dto.permissao_folha_de_pagamento == false &&
                   dto.permissao_folha_de_pagamento_gerar == false &&
                   dto.permissao_folha_de_pagamento_consultar == false &&
                   dto.permissao_folha_de_pagamento_alterar == false &&

                   dto.permissao_pedido == false &&
                   dto.permissao_pedido_cadastrar == false &&
                   dto.permissao_pedido_consultar == false)
            {
                throw new ArgumentException("Os campos permissões estão em branco, por favor preencher.");
            }


            Database__Funcionarios db = new Database__Funcionarios();
            db.Alterar(dto);
        }

        public void Remover(int id)
        {
            Database__Funcionarios db = new Database__Funcionarios();
            db.Remover(id);
        }

        public List<FuncionarioDTO> Listar()
        {
            Database__Funcionarios db = new Database__Funcionarios();
            return db.Listar();
        }

        public FuncionarioDTO Logar(string usuario, string senha)
        {
            Database__Funcionarios db = new Database__Funcionarios();
            return db.Login(usuario, senha);
        }

        public List<FuncionarioDTO> Consultar(string nome)
        {
            Database__Funcionarios db = new Database__Funcionarios();
            return db.Consultar(nome);
        }


        public List<FuncionarioDTO> Buscar(int id)
        {
            Database__Funcionarios db = new Database__Funcionarios();
            return db.Buscar(id);
        }

        public List<FuncionarioDTO> ConsultarporUsuario(string usuario)
        {
            Database__Funcionarios db = new Database__Funcionarios();
            return db.ConsultarporUsuario(usuario);
           
        }

        public void AlterarSenha(int id, string senha)
        {
            SHA256Cript crip = new SHA256Cript();
            exReg reg = new exReg();
            reg.ValidarSenha(senha);
            if(senha==string.Empty)
            {
                throw new ArgumentException("Senha não pode ser vazia");
            }
            if (String.IsNullOrEmpty(senha) || senha.Trim().Length == 0)
            {
                throw new ArgumentException("O campo Nome só tem espaços.");
            }
            Database__Funcionarios db = new Database__Funcionarios();
            db.AlterarSenha(id, crip.Criptografar(senha));
        }

        public List<FuncionarioDTO> ConsultarCPF(string CPF)
        {
            Database__Funcionarios db = new Database__Funcionarios();
            return db.ConsultarCPF(CPF);
        }
    }
}
