﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lanchonha.Banco.BD.ProdutoFornecedor
{
    class DTONomedeProdutoeFornecedor
    {
        public int IDprodutoFornecedor { get; set; }
        public int IdProduto { get; set; }
        public string NomeProduto { get; set; }
        public decimal PrecoProduto { get; set; }
        public int IdFornecedor { get; set; }
        public string NomeFornecedor { get; set; }
    }
}
