﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lanchonha.Banco.BD.ProdutoFornecedor
{
    public class ProdutoFornecedorDTO
    {
        
        public int ID { get; set; }
        public int FKproduto { get; set; }
        public int FKFornecedor { get; set; }
    }
}
