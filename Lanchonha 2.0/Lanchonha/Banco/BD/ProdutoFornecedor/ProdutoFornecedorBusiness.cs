﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lanchonha.Banco.BD.ProdutoFornecedor
{
    class ProdutoFornecedorBusiness
    {
        int chave;
        public int Salvar(List<DTONomedeProdutoeFornecedor> lista)
        {
            
            foreach (DTONomedeProdutoeFornecedor item in lista)
            {
                DTONomedeProdutoeFornecedor dto = new DTONomedeProdutoeFornecedor();
                dto.IdFornecedor = item.IdFornecedor;
                dto.IdProduto = item.IdProduto;
                ProdutoFornecedorDataBase db = new ProdutoFornecedorDataBase();
                chave = db.Salvar(dto);
            }

            return chave;
        }

        public List<DTONomedeProdutoeFornecedor> Consultar (int ID)
        {
            ProdutoFornecedorDataBase db = new ProdutoFornecedorDataBase();
            return db.Consultar(ID);
        }
    }
}
