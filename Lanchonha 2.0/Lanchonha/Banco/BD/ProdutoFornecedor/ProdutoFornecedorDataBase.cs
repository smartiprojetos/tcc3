﻿using Lanchonha.Banco.Banco;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lanchonha.Banco.BD.ProdutoFornecedor
{
     class ProdutoFornecedorDataBase
    {
        public int Salvar (DTONomedeProdutoeFornecedor dto)
        {
            string script = @"insert into tb_produto_fornecedor (fk_id_fornecedor_para_pf,fk_id_produto_para_pf)
                                                 values         (@fk_id_fornecedor_para_pf,@fk_id_produto_para_pf)";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("fk_id_fornecedor_para_pf", dto.IdFornecedor));
            parms.Add(new MySqlParameter("fk_id_produto_para_pf", dto.IdProduto));

            DataBase db = new DataBase();
            return db.ExecuteInsertScriptWithPk(script, parms);
        }


        public List<DTONomedeProdutoeFornecedor> Consultar(int id)
        {
            string script = @"select id_cadastro_produtos,pr_preco, nm_produto,id_produto_fornecedor_pf from tb_cadastro_produtos
                             join tb_produto_fornecedor
                             on tb_cadastro_produtos.id_cadastro_produtos = tb_produto_fornecedor.fk_id_produto_para_pf
                             WHERE tb_produto_fornecedor.fk_id_fornecedor_para_pf =" + id;
;
            List<MySqlParameter> parms = new List<MySqlParameter>();
            DataBase db = new DataBase();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);
            List<DTONomedeProdutoeFornecedor> lista = new List<DTONomedeProdutoeFornecedor>();
            while(reader.Read())
            {
                DTONomedeProdutoeFornecedor dto = new DTONomedeProdutoeFornecedor();
                dto.IdProduto = reader.GetInt32("id_cadastro_produtos");
                dto.NomeProduto = reader.GetString("nm_produto");
                dto.IDprodutoFornecedor= reader.GetInt32("id_produto_fornecedor_pf");
                dto.PrecoProduto = reader.GetDecimal("pr_preco");
                lista.Add(dto);
            }
            reader.Close();
            return lista;


        }
    }
}
