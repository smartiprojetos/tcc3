﻿using Lanchonha.Banco.Banco;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lanchonha.Banco.BD.FolhaPgmt
{
    class FolhaPgmtDatabase
    {
        public int SalvarFolha(FolhaPgmtDTO dto)
        {
            string script = @"INSERT tb_folha_pgmt(dt_Registro, vl_SalarioBruto, ds_DiasTrabalhados, ds_HoraE50, ds_HoraE100, ds_Mensagem, fk_FolhaPgmt_Func, fk_id_empresa_fp)
                                            VALUES(@dt_Registro, @vl_SalarioBruto, @ds_DiasTrabalhados, @ds_HoraE50, @ds_HoraE100, @ds_Mensagem, @fk_FolhaPgmt_Func, @fk_id_empresa_fp)";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("dt_Registro", dto.dt_Registro));
            parms.Add(new MySqlParameter("vl_SalarioBruto", dto.vl_SalarioBruto));
            parms.Add(new MySqlParameter("ds_DiasTrabalhados", dto.ds_DiasTrabalhados));
            parms.Add(new MySqlParameter("ds_HoraE50", dto.ds_HoraE50));
            parms.Add(new MySqlParameter("ds_HoraE100", dto.ds_HoraE100));
            parms.Add(new MySqlParameter("ds_Mensagem", dto.ds_Mensagem));
            parms.Add(new MySqlParameter("fk_FolhaPgmt_Func", dto.fk_FolhaPgmt_Func));
            parms.Add(new MySqlParameter("fk_id_empresa_fp", dto.FKEmpresa));

            DataBase db = new DataBase();
            return db.ExecuteInsertScriptWithPk(script, parms);
        }

        public int AlterarFolha(FolhaPgmtDTO dto)
        {
            string script = @"UPDATE tb_folha_pgmt SET dt_Registro = @dt_Registro,
                                                   vl_SalarioBruto = @vl_SalarioBruto, 
                                                ds_DiasTrabalhados = @ds_DiasTrabalhados, 
                                                        ds_HoraE50 = @ds_HoraE50, 
                                                       ds_HoraE100 = @ds_HoraE100,
                                                       ds_Mensagem = @ds_Mensagem,
                                                             vl_VT = @vl_VT,
                                                             vl_VR = @vl_VR,
                                                   vl_Adiantamento = @vl_Adiantamento,
                                                           vl_INSS = @vl_INSS,
                                                        vl_HoraE50 = @vl_HoraE50,
                                                       vl_HoraE100 = @vl_HoraE100,
                                                      vl_Proventos = @vl_Proventos,
                                                      vl_Descontos = @vl_Descontos,
                                                        vl_Liquido = @vl_Liquido,
                                                       vl_BaseINSS = @vl_BaseINSS,
                                                       vl_BaseFGTS = @vl_BaseFGTS,
                                                        vl_FGTSmes = @vl_FGTSmes,
                                                       vl_BaseIRRF = @vl_BaseIRRF,
                                                      vl_FaixaIRRF = @vl_FaixaIRRF,
                                                          vl_DedIR = @vl_DedIR,
                                                             vl_IR = @vl_IR,
                                                        vl_ValorIR = @vl_ValorIR,
                                                      vl_ValorFGTS = @vl_ValorFGTS,
                                                     vl_mesSalario = @vl_mesSalario

                                               WHERE id_Folha_Pgmt = @id_Folha_Pgmt";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_Folha_Pgmt", dto.id_Folha_Pgmt));
            parms.Add(new MySqlParameter("dt_Registro", dto.dt_Registro));
            parms.Add(new MySqlParameter("vl_DedIR", dto.vl_DedIR));
            parms.Add(new MySqlParameter("vl_IR", dto.vl_IR));
            parms.Add(new MySqlParameter("vl_ValorIR", dto.vl_ValorIR));
            parms.Add(new MySqlParameter("vl_ValorFGTS", dto.vl_ValorFGTS));
            parms.Add(new MySqlParameter("vl_SalarioBruto", dto.vl_SalarioBruto));
            parms.Add(new MySqlParameter("ds_DiasTrabalhados", dto.ds_DiasTrabalhados));
            parms.Add(new MySqlParameter("ds_HoraE50", dto.ds_HoraE50));
            parms.Add(new MySqlParameter("ds_HoraE100", dto.ds_HoraE100));
            parms.Add(new MySqlParameter("ds_Mensagem", dto.ds_Mensagem));
            parms.Add(new MySqlParameter("vl_VT", dto.vl_VT));
            parms.Add(new MySqlParameter("vl_VR", dto.vl_VR));
            parms.Add(new MySqlParameter("vl_Adiantamento", dto.vl_Adiantamento));
            parms.Add(new MySqlParameter("vl_INSS", dto.vl_INSS));
            parms.Add(new MySqlParameter("vl_HoraE50", dto.vl_HoraE50));
            parms.Add(new MySqlParameter("vl_HoraE100", dto.vl_HoraE100));
            parms.Add(new MySqlParameter("vl_Proventos", dto.vl_Proventos));
            parms.Add(new MySqlParameter("vl_Descontos", dto.vl_Descontos));
            parms.Add(new MySqlParameter("vl_Liquido", dto.vl_Liquido));
            parms.Add(new MySqlParameter("vl_BaseINSS", dto.vl_BaseINSS));
            parms.Add(new MySqlParameter("vl_BaseFGTS", dto.vl_BaseFGTS));
            parms.Add(new MySqlParameter("vl_mesSalario", dto.vl_mesSalario));
            parms.Add(new MySqlParameter("vl_FGTSmes", dto.vl_FGTSmes));
            parms.Add(new MySqlParameter("vl_BaseIRRF", dto.vl_BaseIRRF));
            parms.Add(new MySqlParameter("vl_FaixaIRRF", dto.vl_FaixaIRRF));

            DataBase db = new DataBase();
            return db.ExecuteInsertScriptWithPk(script, parms);
        }

        public int RemoverFolha(int idFolha)
        {
            string script = @"DELETE FROM tb_folha_pgmt WHERE id_Folha_Pgmt = @id_Folha_Pgmt";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_Folha_Pgmt", idFolha));

            DataBase db = new DataBase();
            return db.ExecuteInsertScriptWithPk(script, parms);
        }
        // Fazer os selects por view
        public decimal TaxaIR(decimal SalarioBase)
        {
            string script = $@"Select ds_Aliquota FROM tb_ir
                                                 WHERE ds_Salario <= '{SalarioBase}'
                                              ORDER BY ds_Salario 
                                            DESC LIMIT 1";

            List<MySqlParameter> parms = new List<MySqlParameter>();

            DataBase db = new DataBase();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            if (reader.Read())
            {
                decimal taxa = reader.GetDecimal("ds_Aliquota");
                return taxa;
            }
            else
                return 0;
        }

        public decimal DeducaoIR(decimal SalarioBase)
        {
            string script = $@"Select ds_Deduzir FROM tb_ir
                                                 WHERE ds_Salario <= '{SalarioBase}'
                                              ORDER BY ds_Salario 
                                            DESC LIMIT 1";

            List<MySqlParameter> parms = new List<MySqlParameter>();

            DataBase db = new DataBase();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            if (reader.Read())
            {
                decimal taxa = reader.GetDecimal("ds_Deduzir");
                return taxa;
            }
            else
                return 0;
        }

        public decimal TaxaINSS(decimal SalarioBase)
        {
            string script = $@"Select ds_Aliquota FROM tb_inss
                                                 WHERE ds_Salario <= '{SalarioBase}'
                                              ORDER BY ds_Salario 
                                            DESC LIMIT 1";

            List<MySqlParameter> parms = new List<MySqlParameter>();

            DataBase db = new DataBase();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            if (reader.Read())
            {
                decimal taxa = reader.GetDecimal("ds_Aliquota");
                return taxa;
            }
            else
                return 0;
        }

        public List<Descontos_DTO> Descontos()
        {
            string script = @"SELECT * FROM tb_descontos";
            List<MySqlParameter> parms = new List<MySqlParameter>();

            DataBase db = new DataBase();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<Descontos_DTO> descontos = new List<Descontos_DTO>();
            while (reader.Read())
            {
                Descontos_DTO desconto = new Descontos_DTO();
                desconto.id_Descontos = reader.GetInt32("id_Descontos");
                desconto.ds_Tipo = reader.GetString("ds_Tipo");
                desconto.ds_Porcentagem = reader.GetDecimal("ds_Porcentagem");

                descontos.Add(desconto);
            }
            reader.Close();
            return descontos;
        }

        public List<view_func_folha> ConsultarPorCPF(string cpf)
        {
            string script = $"SELECT * FROM view_func_folha WHERE cf_cpf like @cf_cpf";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("cf_cpf", cpf + "%"));
            DataBase db = new DataBase();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);
            List<view_func_folha> lista = new List<view_func_folha>();
            
            while (reader.Read())
            {
                view_func_folha folha = new view_func_folha();
                folha.id_Folha_Pgmt = reader.GetInt32("id_Folha_Pgmt");
                folha.dt_Registro = reader.GetDateTime("dt_Registro");
                folha.vl_SalarioBruto = reader.GetDecimal("vl_SalarioBruto");
                folha.ds_DiasTrabalhados = reader.GetInt32("ds_DiasTrabalhados");
                folha.ds_HoraE50 = reader.GetInt32("ds_HoraE50");
                folha.ds_HoraE100 = reader.GetInt32("ds_HoraE100");
                folha.ds_Mensagem = reader.GetString("ds_Mensagem");
                folha.vl_VT= reader.GetDecimal("vl_VT");
                folha.vl_VR= reader.GetDecimal("vl_VR");
                folha.vl_Adiantamento= reader.GetDecimal("vl_Adiantamento");
                folha.vl_INSS= reader.GetDecimal("vl_INSS");
                folha.vl_HoraE50= reader.GetDecimal("vl_HoraE50");
                folha.vl_HoraE100= reader.GetDecimal("vl_HoraE100");
                folha.vl_mesSalario= reader.GetDecimal("vl_mesSalario");
                folha.vl_Proventos= reader.GetDecimal("vl_Proventos");
                folha.vl_Descontos= reader.GetDecimal("vl_Descontos");
                folha.vl_Liquido= reader.GetDecimal("vl_Liquido");
                folha.vl_BaseINSS= reader.GetDecimal("vl_BaseINSS");
                folha.vl_BaseFGTS= reader.GetDecimal("vl_BaseFGTS");
                folha.vl_FGTSmes= reader.GetDecimal("vl_FGTSmes");
                folha.vl_BaseIRRF= reader.GetDecimal("vl_BaseIRRF");
                folha.vl_FaixaIRRF= reader.GetDecimal("vl_FaixaIRRF");
                folha.vl_DedIR= reader.GetDecimal("vl_DedIR");
                folha.vl_IR= reader.GetDecimal("vl_IR");
                folha.vl_ValorIR = reader.GetDecimal("vl_ValorIR");
                folha.vl_ValorFGTS = reader.GetDecimal("vl_ValorFGTS");

                folha.fk_FolhaPgmt_Func = reader.GetInt32("fk_FolhaPgmt_Func");
                folha.nm_NomeFunc = reader.GetString("nm_nome_completo");
                folha.ds_Cargo = reader.GetString("cg_cargo");
                folha.ds_CPF = reader.GetString("cf_cpf");

                folha.nm_real= reader.GetString("nm_real");
                folha.cn_cnpj= reader.GetString("cn_cnpj");
                lista.Add(folha);

            }
            reader.Close();
            return lista;
        }

        public view_func_folha ConsultarPorIdCalc(string id)
        {
            string script = $"SELECT * FROM view_func_folha WHERE id_Folha_Pgmt = '{id}'";

            List<MySqlParameter> parms = new List<MySqlParameter>();

            DataBase db = new DataBase();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            view_func_folha folha = new view_func_folha();
            while (reader.Read())
            {
                folha.id_Folha_Pgmt = reader.GetInt32("id_Folha_Pgmt");
                folha.dt_Registro = reader.GetDateTime("dt_Registro");
                folha.vl_SalarioBruto = reader.GetDecimal("vl_SalarioBruto");
                folha.ds_DiasTrabalhados = reader.GetInt32("ds_DiasTrabalhados");
                folha.ds_HoraE50 = reader.GetInt32("ds_HoraE50");
                folha.ds_HoraE100 = reader.GetInt32("ds_HoraE100");
                folha.ds_Mensagem = reader.GetString("ds_Mensagem");
                folha.fk_FolhaPgmt_Func = reader.GetInt32("fk_FolhaPgmt_Func");
                folha.ds_Cargo = reader.GetString("cg_cargo");
                folha.nm_NomeFunc = reader.GetString("nm_nome_completo");
                folha.ds_CPF = reader.GetString("cf_cpf");
            }
            reader.Close();
            return folha;
        }

        public List<view_func_folha> ConsultarPorID(int ID)
        {
            string script = $"SELECT * FROM view_func_folha WHERE id_Folha_Pgmt = '{ID}'";

            List<MySqlParameter> parms = new List<MySqlParameter>();

            DataBase db = new DataBase();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);
            List<view_func_folha> lista = new List<view_func_folha>();

            while (reader.Read())
            {
                view_func_folha folha = new view_func_folha();
                folha.id_Folha_Pgmt = reader.GetInt32("id_Folha_Pgmt");
                folha.dt_Registro = reader.GetDateTime("dt_Registro");
                folha.vl_SalarioBruto = reader.GetDecimal("vl_SalarioBruto");
                folha.ds_DiasTrabalhados = reader.GetInt32("ds_DiasTrabalhados");
                folha.ds_HoraE50 = reader.GetInt32("ds_HoraE50");
                folha.ds_HoraE100 = reader.GetInt32("ds_HoraE100");
                folha.ds_Mensagem = reader.GetString("ds_Mensagem");
                folha.vl_VT = reader.GetDecimal("vl_VT");
                folha.vl_VR = reader.GetDecimal("vl_VR");
                folha.vl_Adiantamento = reader.GetDecimal("vl_Adiantamento");
                folha.vl_INSS = reader.GetDecimal("vl_INSS");
                folha.vl_HoraE50 = reader.GetDecimal("vl_HoraE50");
                folha.vl_HoraE100 = reader.GetDecimal("vl_HoraE100");
                folha.vl_mesSalario = reader.GetDecimal("vl_mesSalario");
                folha.vl_Proventos = reader.GetDecimal("vl_Proventos");
                folha.vl_Descontos = reader.GetDecimal("vl_Descontos");
                folha.vl_Liquido = reader.GetDecimal("vl_Liquido");
                folha.vl_BaseINSS = reader.GetDecimal("vl_BaseINSS");
                folha.vl_BaseFGTS = reader.GetDecimal("vl_BaseFGTS");
                folha.vl_FGTSmes = reader.GetDecimal("vl_FGTSmes");
                folha.vl_BaseIRRF = reader.GetDecimal("vl_BaseIRRF");
                folha.vl_FaixaIRRF = reader.GetDecimal("vl_FaixaIRRF");
                folha.vl_DedIR = reader.GetDecimal("vl_DedIR");
                folha.vl_IR = reader.GetDecimal("vl_IR");
                folha.vl_ValorIR = reader.GetDecimal("vl_ValorIR");
                folha.vl_ValorFGTS = reader.GetDecimal("vl_ValorFGTS");

                folha.fk_FolhaPgmt_Func = reader.GetInt32("fk_FolhaPgmt_Func");
                folha.nm_NomeFunc = reader.GetString("nm_nome_completo");
                folha.ds_Cargo = reader.GetString("cg_cargo");
                folha.ds_CPF = reader.GetString("cf_cpf");

                folha.nm_real = reader.GetString("nm_real");
                folha.cn_cnpj = reader.GetString("cn_cnpj");
                lista.Add(folha);

            }
            reader.Close();
            return lista;
        }

        public List<view_func_folha> Listar()
        {
            string script = @"select * from view_func_folha;";

            List<MySqlParameter> parms = new List<MySqlParameter>();
        
            DataBase db = new DataBase();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);
            List<view_func_folha> lista = new List<view_func_folha>();

            while (reader.Read())
            {
                view_func_folha folha = new view_func_folha();
                folha.id_Folha_Pgmt = reader.GetInt32("id_Folha_Pgmt");
                folha.dt_Registro = reader.GetDateTime("dt_Registro");
                folha.vl_SalarioBruto = reader.GetDecimal("vl_SalarioBruto");
                folha.ds_DiasTrabalhados = reader.GetInt32("ds_DiasTrabalhados");
                folha.ds_HoraE50 = reader.GetInt32("ds_HoraE50");
                folha.ds_HoraE100 = reader.GetInt32("ds_HoraE100");
                folha.ds_Mensagem = reader.GetString("ds_Mensagem");
                folha.vl_VT = reader.GetDecimal("vl_VT");
                folha.vl_VR = reader.GetDecimal("vl_VR");
                folha.vl_Adiantamento = reader.GetDecimal("vl_Adiantamento");
                folha.vl_INSS = reader.GetDecimal("vl_INSS");
                folha.vl_HoraE50 = reader.GetDecimal("vl_HoraE50");
                folha.vl_HoraE100 = reader.GetDecimal("vl_HoraE100");
                folha.vl_mesSalario = reader.GetDecimal("vl_mesSalario");
                folha.vl_Proventos = reader.GetDecimal("vl_Proventos");
                folha.vl_Descontos = reader.GetDecimal("vl_Descontos");
                folha.vl_Liquido = reader.GetDecimal("vl_Liquido");
                folha.vl_BaseINSS = reader.GetDecimal("vl_BaseINSS");
                folha.vl_BaseFGTS = reader.GetDecimal("vl_BaseFGTS");
                folha.vl_FGTSmes = reader.GetDecimal("vl_FGTSmes");
                folha.vl_BaseIRRF = reader.GetDecimal("vl_BaseIRRF");
                folha.vl_FaixaIRRF = reader.GetDecimal("vl_FaixaIRRF");
                folha.vl_DedIR = reader.GetDecimal("vl_DedIR");
                folha.vl_IR = reader.GetDecimal("vl_IR");
                folha.vl_ValorIR = reader.GetDecimal("vl_ValorIR");
                folha.vl_ValorFGTS = reader.GetDecimal("vl_ValorFGTS");

                folha.fk_FolhaPgmt_Func = reader.GetInt32("fk_FolhaPgmt_Func");
                folha.nm_NomeFunc = reader.GetString("nm_nome_completo");
                folha.ds_Cargo = reader.GetString("cg_cargo");
                folha.ds_CPF = reader.GetString("cf_cpf");

                folha.nm_real = reader.GetString("nm_real");
                folha.cn_cnpj = reader.GetString("cn_cnpj");
                lista.Add(folha);

            }
            reader.Close();
            return lista;
        }
    }
}
