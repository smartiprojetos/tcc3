﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lanchonha.Banco.BD.FolhaPgmt
{
    class INSS_DTO
    {
        public int id_INSS { get; set; }
        public decimal ds_Salario { get; set; }
        public decimal ds_Aliquota { get; set; }
    }
}
