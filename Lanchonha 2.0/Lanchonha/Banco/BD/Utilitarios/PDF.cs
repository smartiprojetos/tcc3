﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lanchonha.Banco.BD.Utilitarios
{
    public class PDF
    {
        public void pdf(string conteudo)
        {
            //acessar a biblioteca , dentro do using para eliminar as referencias de memoria quando terminar
            using (var doc = new PdfSharp.Pdf.PdfDocument())
            {
                //criar pagina
                var page = doc.AddPage();
                //criar um grafico 
                var graphics = PdfSharp.Drawing.XGraphics.FromPdfPage(page);
                //para escrever texto 
                var textFormatter = new PdfSharp.Drawing.Layout.XTextFormatter(graphics);
                //criar fonte - nome da fonte e o tamanho
                var font = new PdfSharp.Drawing.XFont("Arial", 14);

                textFormatter.Alignment = PdfSharp.Drawing.Layout.XParagraphAlignment.Justify;

                //como colocar imagem 
                // graphics.DrawImage(PdfSharp.Drawing.XImage.FromFile(img , 0,0));
                //Escrevendo
                // Pamametros - texto - fonte - cor da fonte - retangulo onde o texto será inserido (coordenadas)
                textFormatter.DrawString(conteudo,
                                         font,
                                         PdfSharp.Drawing.XBrushes.Black,
                                         new PdfSharp.Drawing.XRect(10, 50, page.Width, page.Height));






                doc.Save("Orcamento.pdf");
                System.Diagnostics.Process.Start("Orcamento.pdf");
            }
        }
    }
}
