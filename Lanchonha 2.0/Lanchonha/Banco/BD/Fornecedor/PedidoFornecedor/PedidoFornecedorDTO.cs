﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lanchonha.Banco.BD.Fornecedor.PedidoFornecedor
{
    class PedidoFornecedorDTO
    {
        public int IDPedidoFornecedor { get; set; }
        public int Quantidade { get; set; }
        public decimal PrecoTotal { get; set; }
        public DateTime DataCompra { get; set; }
        public int FKFuncionario  { get; set; }
        public int FkProdutoFornecedor { get; set; }
    }
}
