﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lanchonha.Banco.BD.Fornecedor.PedidoFornecedor
{
    class PedidoparaFornecedorGridDTO
    {
        public int FkProdutoFornecedor { get; set; }
        public decimal PrecoTotal { get; set; }
        public int Quantidade { get; set; }
        public string NomeProduto { get; set; }
        public string NomeFornecedor { get; set; }
        public int FKFuncionario { get; set; }
        public DateTime DataCompra { get; set; }
        public int IDProduto { get; set; }
    }
}
