﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lanchonha.Banco.BD.Fornecedor.PedidoFornecedor
{
    class ConsultarPedidoFornecedorDTO
    {

        public string nm_Produto { get; set; }

        public string nm_empresa { get; set; }

        public decimal pre_valor_total { get; set; }

        public int qrt_quantidade_de_produto { get; set; }

        public DateTime dt_compra { get; set; }

        public int fk_id_produto_fornecedor_para_prf { get; set; }

        public int IdFornecedor { get; set; }

    }    
}
