﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lanchonha.Banco.BD.Fornecedor.PedidoFornecedor
{
    class PedidoparaFornecedorView
    {
        public int IdProdudtoFornecedor { get; set; }
        public string  NomeFornecedor { get; set; }
        public int IdFornecedor { get; set; }
        public string NomeProduto { get; set; }
        public int IdProduto { get; set; }
        public string CNPJ { get; set; }
        public string CEP { get; set; }
        public decimal PrecoUnitario { get; set; }

    }
}
