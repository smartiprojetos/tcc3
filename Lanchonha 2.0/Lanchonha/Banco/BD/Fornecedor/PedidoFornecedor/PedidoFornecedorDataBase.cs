﻿using Lanchonha.Banco.Banco;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lanchonha.Banco.BD.Fornecedor.PedidoFornecedor
{
    class PedidoFornecedorDataBase
    {
        public int Salvar (PedidoFornecedorDTO dto)
        {
            string script = @"insert into tb_pedido_para_fornecedor (qrt_quantidade_de_produto,pre_valor_total,dt_compra,fk_id_funcionario_para_prf,fk_id_produto_fornecedor_para_prf)
                                values                    (@qrt_quantidade_de_produto,@pre_valor_total,@dt_compra,@fk_id_funcionario_para_prf,@fk_id_produto_fornecedor_para_prf)";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("qrt_quantidade_de_produto", dto.Quantidade));
            parms.Add(new MySqlParameter("pre_valor_total", dto.PrecoTotal));
            parms.Add(new MySqlParameter("fk_id_funcionario_para_prf", dto.FKFuncionario));
            parms.Add(new MySqlParameter("dt_compra", dto.DataCompra));
            parms.Add(new MySqlParameter("fk_id_produto_fornecedor_para_prf", dto.FkProdutoFornecedor));


            DataBase db = new DataBase();
            return db.ExecuteInsertScriptWithPk(script, parms);

        }

        public List<PedidoparaFornecedorView> Listar()
        {
            string script = @"select * from pedido_para_fornecedo";
            DataBase db = new DataBase();
            List<MySqlParameter> parms = new List<MySqlParameter>();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);
            List<PedidoparaFornecedorView> lista = new List<PedidoparaFornecedorView>();
            while(reader.Read())
            {
                PedidoparaFornecedorView dto = new PedidoparaFornecedorView();
                dto.IdProdudtoFornecedor = reader.GetInt32("id_produto_fornecedor_pf");
                dto.NomeFornecedor = reader.GetString("nm_empresa");
                dto.NomeProduto = reader.GetString("nm_produto");
                dto.CNPJ = reader.GetString("cj_cnpj");
                dto.CEP = reader.GetString("cp_cep");
                dto.PrecoUnitario = reader.GetDecimal("pr_unitario");

                lista.Add(dto);
            }
            reader.Close();
            return lista;
        }


        public List<PedidoparaFornecedorView> ConsultarView(int ID)
        {
            string script = @"select * from pedido_para_fornecedo where id_cadastro_fornecedor like @id_cadastro_fornecedor";
            DataBase db = new DataBase();
            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_cadastro_fornecedor", ID + "%"));
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);
            List<PedidoparaFornecedorView> lista = new List<PedidoparaFornecedorView>();
            while (reader.Read())
            {
                PedidoparaFornecedorView dto = new PedidoparaFornecedorView();
                dto.IdProdudtoFornecedor = reader.GetInt32("id_produto_fornecedor_pf");
                dto.NomeFornecedor = reader.GetString("nm_empresa");
                dto.NomeProduto = reader.GetString("nm_produto");
                dto.CNPJ = reader.GetString("cj_cnpj");
                dto.CEP = reader.GetString("cp_cep");
                dto.PrecoUnitario = reader.GetDecimal("pr_unitario");
                dto.IdFornecedor = reader.GetInt32("id_cadastro_fornecedor");
                dto.IdProduto= reader.GetInt32("id_cadastro_produtos");
                lista.Add(dto);
            }
            reader.Close();
            return lista;
        }
        // Checar a Necessidade Disso
        //public List<PedidoFornecedorDTO> ConsultarporNome(string nome)
        //{
        //    string script = @"select * from tb_pedido_para_fornecedor where nm_empresa like @nm_empresa";
        //    DataBase db = new DataBase();
        //    List<MySqlParameter> parms = new List<MySqlParameter>();
        //    parms.Add(new MySqlParameter("nm_empresa", nome + "%"));
        //    MySqlDataReader reader = db.ExecuteSelectScript(script, parms);
        //    List<PedidoFornecedorDTO> lista = new List<PedidoFornecedorDTO>();
        //    while (reader.Read())
        //    {
        //        PedidoFornecedorDTO dto = new PedidoFornecedorDTO();
        //        dto.IDPedidoFornecedor = reader.GetInt32("id_pedido_para_fornecedor_prf");
        //        dto.PrecoTotal = reader.GetDecimal("pre_valor_total");
        //        dto.Quantidade = reader.GetInt32("qrt_quantidade_de_produto");
        //        dto.FkProdutoFornecedor = reader.GetInt32("fk_id_produto_fornecedor_para_prf");

        //        lista.Add(dto);
        //    }
        //    reader.Close();
        //    return lista;
        //}
        public List<ConsultarPedidoFornecedorDTO> ConsultarporNomeempresa(string nomeempresa)
        {
            string script = @"select * from consultar_pedido_fornecedor where nm_empresa like @nm_empresa";


            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_empresa", nomeempresa + "%"));
            DataBase db = new DataBase();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms); 
            List<ConsultarPedidoFornecedorDTO> lista = new List<ConsultarPedidoFornecedorDTO>();
            while (reader.Read())
            {
                ConsultarPedidoFornecedorDTO dto = new ConsultarPedidoFornecedorDTO();
                dto.dt_compra = reader.GetDateTime("dt_compra")                                                                                                                 ; // achou
                dto.nm_Produto = reader.GetString("nm_Produto")                                                                                                                   ;
                dto.nm_empresa = reader.GetString("nm_empresa");
                dto.pre_valor_total = reader.GetInt32("pre_valor_total");
                dto.qrt_quantidade_de_produto = reader.GetInt32("qrt_quantidade_de_produto");
                dto.fk_id_produto_fornecedor_para_prf = reader.GetInt32("fk_id_produto_fornecedor_para_prf");
                dto.IdFornecedor= reader.GetInt32("id_cadastro_fornecedor");

                lista.Add(dto);
            }
            reader.Close();
            return lista;

        }

        public List<ConsultarPedidoFornecedorDTO> ConsultarporID(int ID)
        {
            string script = @"select * from consultar_pedido_fornecedor where id_cadastro_fornecedor like @id_cadastro_fornecedor";


            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_cadastro_fornecedor", ID + "%"));
            DataBase db = new DataBase();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);
            List<ConsultarPedidoFornecedorDTO> lista = new List<ConsultarPedidoFornecedorDTO>();
            while (reader.Read())
            {
                ConsultarPedidoFornecedorDTO dto = new ConsultarPedidoFornecedorDTO();
                dto.dt_compra = reader.GetDateTime("dt_compra");
                dto.nm_Produto = reader.GetString("nm_Produto");
                dto.nm_empresa = reader.GetString("nm_empresa");
                dto.pre_valor_total = reader.GetInt32("pre_valor_total");
                dto.qrt_quantidade_de_produto = reader.GetInt32("qrt_quantidade_de_produto");
                dto.fk_id_produto_fornecedor_para_prf = reader.GetInt32("fk_id_produto_fornecedor_para_prf");
                dto.IdFornecedor = reader.GetInt32("id_cadastro_fornecedor");

                lista.Add(dto);
            }
            reader.Close();
            return lista;

        }



    }
}
