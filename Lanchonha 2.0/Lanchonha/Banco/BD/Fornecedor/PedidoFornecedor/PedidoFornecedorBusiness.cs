﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lanchonha.Banco.BD.Fornecedor.PedidoFornecedor
{
    class PedidoFornecedorBusiness
    {
        public int Salvar(List<PedidoparaFornecedorGridDTO> dto)
        {
            
            
            int pk=0;
            PedidoFornecedorDataBase db = new PedidoFornecedorDataBase();
            PedidoFornecedorDTO pedido = new PedidoFornecedorDTO();
            foreach (PedidoparaFornecedorGridDTO item in dto)
            {
                pedido.FkProdutoFornecedor = item.FkProdutoFornecedor;
                pedido.PrecoTotal = item.PrecoTotal;
                pedido.Quantidade = item.Quantidade;
                pedido.FKFuncionario = item.FKFuncionario;
                pedido.DataCompra = item.DataCompra;
                pk=db.Salvar(pedido);
            }

            return pk;
        }

        public List<PedidoparaFornecedorView> Listar()
        {
            PedidoFornecedorDataBase db = new PedidoFornecedorDataBase();
            return db.Listar();
        }

        

        public List<ConsultarPedidoFornecedorDTO> ConsultarpornomeEmpresa(string nomeempresa)
        {
            PedidoFornecedorDataBase db = new PedidoFornecedorDataBase();
            return db.ConsultarporNomeempresa(nomeempresa);
        }

        public List<ConsultarPedidoFornecedorDTO> ConsultarporID(int ID)
        {
            PedidoFornecedorDataBase db = new PedidoFornecedorDataBase();
            return db.ConsultarporID(ID);
        }
    }
}
