﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lanchonha.Banco.BD.Fornecedor
{
    class DTO_Fornecedores
    {

        public int IdFornecedor { get; set; }
        public string NomeFornecedor { get; set; }
        public string CNPJ { get; set; }
        public string Rua { get; set; }
        public string Numerofornecedor { get; set; }
        public string Complemento { get; set; }
        public string Bairro { get; set; }
        public string Cidade { get; set; }
        public string UF { get; set; }
        public string CEP { get; set; }
        public string Telefone { get; set; }
        public string Email { get; set; }
        public decimal PrecoUnitario { get; set; }
        public DateTime DataCadastro { get; set; }
        public string Observacao { get; set; }
        public int FKFuncionario { get; set; }

    }
}
