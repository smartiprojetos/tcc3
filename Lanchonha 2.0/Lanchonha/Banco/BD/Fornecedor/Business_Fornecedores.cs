﻿using Lanchonha.Utilitarios;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lanchonha.Banco.BD.Fornecedor
{
    class Business_Fornecedores
    {
        public int Salvar (DTO_Fornecedores dto)

        {
            exReg reg = new exReg();
            reg.ValidarNome(dto.NomeFornecedor);
            reg.ValidarTelefoneFixo(dto.Telefone);
            reg.ValidarEmail(dto.Email);
            //if (String.IsNullOrEmpty(dto.Complemento) || dto.Complemento.Trim().Length == 0)
            //{
            //    throw new ArgumentException("O campo Complemento só tem espaços.");
            //}
            if (String.IsNullOrEmpty(dto.NomeFornecedor) || dto.NomeFornecedor.Trim().Length == 0)
            {
                throw new ArgumentException("O campo Nome do Fornecedor só tem espaços.");
            }
            if (String.IsNullOrEmpty(dto.Email) || dto.Email.Trim().Length == 0)
            {
                throw new ArgumentException("O campo E-mail só tem espaços.");
            }
            if (String.IsNullOrEmpty(dto.UF) || dto.UF.Trim().Length == 0)
            {
                throw new ArgumentException("O campo Estado só tem espaços.");
            }
            if (String.IsNullOrEmpty(dto.Cidade) || dto.Cidade.Trim().Length == 0)
            {
                throw new ArgumentException("O campo Cidade só tem espaços.");
            }
            if (String.IsNullOrEmpty(dto.Bairro) || dto.Bairro.Trim().Length == 0)
            {
                throw new ArgumentException("O campo Bairro só tem espaços.");
            }
            if (String.IsNullOrEmpty(dto.Rua) || dto.Rua.Trim().Length == 0)
            {
                throw new ArgumentException("O campo Rua só tem espaços.");
            }
            if (String.IsNullOrEmpty(dto.Numerofornecedor) || dto.Numerofornecedor.Trim().Length == 0)
            {
                throw new ArgumentException("O campo Numero do Fornecedor só tem espaços.");
            }
            if (dto.UF == null)
            {
                throw new ArgumentException("O campo UF está em branco, por favor preencher.");
            }
            if (dto.Bairro == null)
            {
                throw new ArgumentException("O campo Bairro está em branco, por favor preencher.");
            }
            if (dto.Rua == null)
            {
                throw new ArgumentException("O campo Rua está em branco, por favor preencher.");
            }
            if (dto.Cidade == null)
            {
                throw new ArgumentException("O campo Cidade está em branco, por favor preencher.");
            }
            if (dto.Numerofornecedor == null)
            {
                throw new ArgumentException("O campo Número está em branco, por favor preencher.");
            }
            if (dto.PrecoUnitario == 0)
            {
                throw new ArgumentException("O campo Preço está em branco, por favor preencher.");
            }
            CNPJ NHA = new CNPJ();
            NHA.ValidarCNPJ(dto.CNPJ);
            Database_Fornecedores db = new Database_Fornecedores();
            return db.Salvar(dto);
        }

        public void Alterar(DTO_Fornecedores dto)
        {
            exReg reg = new exReg();
            reg.ValidarNome(dto.NomeFornecedor);
            reg.ValidarTelefoneFixo(dto.Telefone);
            reg.ValidarValor(Convert.ToString(dto.PrecoUnitario));
            reg.ValidarEmail(dto.Email);

            if (String.IsNullOrEmpty(dto.NomeFornecedor) || dto.NomeFornecedor.Trim().Length == 0)
            {
                throw new ArgumentException("O campo Nome do Fornecedor só tem espaços.");
            }
            if (String.IsNullOrEmpty(dto.Email) || dto.Email.Trim().Length == 0)
            {
                throw new ArgumentException("O campo E-mail só tem espaços.");
            }
            if (String.IsNullOrEmpty(dto.UF) || dto.UF.Trim().Length == 0)
            {
                throw new ArgumentException("O campo Estado só tem espaços.");
            }
            if (String.IsNullOrEmpty(dto.Cidade) || dto.Cidade.Trim().Length == 0)
            {
                throw new ArgumentException("O campo Cidade só tem espaços.");
            }
            if (String.IsNullOrEmpty(dto.Bairro) || dto.Bairro.Trim().Length == 0)
            {
                throw new ArgumentException("O campo Bairro só tem espaços.");
            }
            if (String.IsNullOrEmpty(dto.Rua) || dto.Rua.Trim().Length == 0)
            {
                throw new ArgumentException("O campo Rua só tem espaços.");
            }
            if (String.IsNullOrEmpty(dto.Numerofornecedor) || dto.Numerofornecedor.Trim().Length == 0)
            {
                throw new ArgumentException("O campo Numero do Fornecedor só tem espaços.");
            }
            if (dto.UF == null)
            {
                throw new ArgumentException("O campo UF está em branco, por favor preencher.");
            }
            if (dto.Bairro == null)
            {
                throw new ArgumentException("O campo Bairro está em branco, por favor preencher.");
            }
            if (dto.Rua == null)
            {
                throw new ArgumentException("O campo Rua está em branco, por favor preencher.");
            }
            if (dto.Cidade == null)
            {
                throw new ArgumentException("O campo Cidade está em branco, por favor preencher.");
            }
            if (dto.Numerofornecedor == null)
            {
                throw new ArgumentException("O campo Número está em branco, por favor preencher.");
            }
            if (dto.PrecoUnitario == 0)
            {
                throw new ArgumentException("O campo Preço está em branco, por favor preencher.");
            }
            CNPJ NHA = new CNPJ();
            NHA.ValidarCNPJ(dto.CNPJ);
            Database_Fornecedores db = new Database_Fornecedores();
            db.Alterar(dto);
        }

        public void Remover(int ID)
        {
            Database_Fornecedores db = new Database_Fornecedores();
            db.Remover(ID);
        }

        public List<DTO_Fornecedores> ConsultarporNome (string fornecedor)
        {
            Database_Fornecedores db = new Database_Fornecedores();
            return db.ConsultarporNome(fornecedor);
        }

        public List<DTO_Fornecedores> ConsultarporId(int Id)
        {
            Database_Fornecedores db = new Database_Fornecedores();
            return db.ConsultarporId(Id);
        }

        public List<DTO_Fornecedores> Listar()
        {
            Database_Fornecedores db = new Database_Fornecedores();
            return db.Listar();
        }
    }
}
