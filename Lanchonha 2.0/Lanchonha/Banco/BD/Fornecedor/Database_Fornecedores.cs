﻿using Lanchonha.Banco.Banco;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lanchonha.Banco.BD.Fornecedor
{
    class Database_Fornecedores
    {
        public int Salvar (DTO_Fornecedores dto)
        {
            string script= @"insert into tb_cadastro_fornecedor (nm_empresa,cj_cnpj,rn_rua,nr_numero,ct_complemento,br_bairro,cd_cidade,uf,cp_cep,
                                                                   tl_telefone,email,pr_unitario,obs_observacao,dt_data_cadastro,fk_id_cadastro_funcionario_para_fo)
                                                              values
                                                                (@nm_empresa,@cj_cnpj,@rn_rua,@nr_numero,@ct_complemento,@br_bairro,@cd_cidade,@uf,@cp_cep,
                                                                  @tl_telefone,@email,@pr_unitario,@obs_observacao,@dt_data_cadastro,@fk_id_cadastro_funcionario_para_fo)";
            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_empresa", dto.NomeFornecedor));
            parms.Add(new MySqlParameter("cj_cnpj",dto.CNPJ));
            parms.Add(new MySqlParameter("rn_rua",dto.Rua));
            parms.Add(new MySqlParameter("nr_numero",dto.Numerofornecedor));
            parms.Add(new MySqlParameter("ct_complemento",dto.Complemento));
            parms.Add(new MySqlParameter("br_bairro",dto.Bairro));
            parms.Add(new MySqlParameter("cd_cidade", dto.Cidade));
            parms.Add(new MySqlParameter("uf",dto.UF));
            parms.Add(new MySqlParameter("cp_cep",dto.CEP));
            parms.Add(new MySqlParameter("tl_telefone",dto.Telefone));
            parms.Add(new MySqlParameter("email",dto.Email));
            parms.Add(new MySqlParameter("pr_unitario",dto.PrecoUnitario));
            parms.Add(new MySqlParameter("obs_observacao",dto.Observacao));
            parms.Add(new MySqlParameter("dt_data_cadastro", dto.DataCadastro));
            parms.Add(new MySqlParameter("fk_id_cadastro_funcionario_para_fo",dto.FKFuncionario));

            DataBase db = new DataBase();
            return db.ExecuteInsertScriptWithPk(script, parms);

        }

        public void Alterar (DTO_Fornecedores dto)
        {
            string script = @"update tb_cadastro_fornecedor set nm_empresa=@nm_empresa,                                    
                                                                cj_cnpj=@cj_cnpj,
                                                                rn_rua=@rn_rua,
                                                                nr_numero=@nr_numero,
                                                                ct_complemento=@ct_complemento,
                                                                br_bairro=@br_bairro,
                                                                cd_cidade=@cd_cidade,
                                                                uf=@uf,
                                                                cp_cep=@cp_cep,
                                                                tl_telefone=@tl_telefone,
                                                                email=@email,
                                                                pr_unitario=@pr_unitario,
                                                                obs_observacao=@obs_observacao,
                                                                dt_data_cadastro=@dt_data_cadastro,
                                                                fk_id_cadastro_funcionario_para_fo=@fk_id_cadastro_funcionario_para_fo
                                                                where id_cadastro_fornecedor=@id_cadastro_fornecedor";
            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_cadastro_fornecedor", dto.IdFornecedor));
            parms.Add(new MySqlParameter("nm_empresa", dto.NomeFornecedor));
            parms.Add(new MySqlParameter("cj_cnpj", dto.CNPJ));
            parms.Add(new MySqlParameter("rn_rua", dto.Rua));
            parms.Add(new MySqlParameter("nr_numero", dto.Numerofornecedor));
            parms.Add(new MySqlParameter("ct_complemento", dto.Complemento));
            parms.Add(new MySqlParameter("br_bairro", dto.Bairro));
            parms.Add(new MySqlParameter("cd_cidade", dto.Cidade));
            parms.Add(new MySqlParameter("uf", dto.UF));
            parms.Add(new MySqlParameter("cp_cep", dto.CEP));
            parms.Add(new MySqlParameter("tl_telefone", dto.Telefone));
            parms.Add(new MySqlParameter("email", dto.Email));
            parms.Add(new MySqlParameter("pr_unitario", dto.PrecoUnitario));
            parms.Add(new MySqlParameter("obs_observacao", dto.Observacao));
            parms.Add(new MySqlParameter("dt_data_cadastro", dto.DataCadastro));
            parms.Add(new MySqlParameter("fk_id_cadastro_funcionario_para_fo", dto.FKFuncionario));

            DataBase db = new DataBase();
            db.ExecuteInsertScript(script, parms);
        }

        public void Remover(int ID)
        {
            string script=@"delete from tb_cadastro_fornecedor where id_cadastro_fornecedor = @id_cadastro_fornecedor";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_cadastro_fornecedor", ID));

            DataBase db = new DataBase();
            db.ExecuteInsertScript(script, parms);
        }

        public List<DTO_Fornecedores> ConsultarporNome(string fornecedor)
        {
            string script = @"select * from tb_cadastro_fornecedor where nm_empresa like @nm_empresa";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_empresa", fornecedor + "%"));
            DataBase db = new DataBase();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);
            List<DTO_Fornecedores> lista = new List<DTO_Fornecedores>();
            DTO_Fornecedores dto = null;
            while(reader.Read())
            {
                dto = new DTO_Fornecedores();
                dto.IdFornecedor = reader.GetInt32("id_cadastro_fornecedor");
                dto.NomeFornecedor = reader.GetString("nm_empresa");
                dto.CNPJ = reader.GetString("cj_cnpj");
                dto.Rua = reader.GetString("rn_rua");
                dto.Numerofornecedor = reader.GetString("nr_numero");
                dto.Complemento = reader.GetString("ct_complemento");
                dto.Bairro = reader.GetString("br_bairro");
                dto.Cidade = reader.GetString("cd_cidade");
                dto.UF = reader.GetString("uf");
                dto.CEP = reader.GetString("cp_cep");
                dto.Telefone = reader.GetString("tl_telefone");
                dto.Email = reader.GetString("email");
                dto.PrecoUnitario = reader.GetDecimal("pr_unitario");
                dto.DataCadastro = reader.GetDateTime("dt_data_cadastro");
                dto.Observacao = reader.GetString("obs_observacao");
                dto.FKFuncionario = reader.GetInt32("fk_id_cadastro_funcionario_para_fo");

                lista.Add(dto);
            }
            reader.Close();
            return lista;


        }

        public List<DTO_Fornecedores> ConsultarporId(int Id)
        {
            string script = @"select * from tb_cadastro_fornecedor where id_cadastro_fornecedor like @id_cadastro_fornecedor";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_cadastro_fornecedor", Id + "%"));
            DataBase db = new DataBase();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);
            List<DTO_Fornecedores> lista = new List<DTO_Fornecedores>();
            DTO_Fornecedores dto = null;
            while (reader.Read())
            {
                dto = new DTO_Fornecedores();
                dto.IdFornecedor = reader.GetInt32("id_cadastro_fornecedor");
                dto.NomeFornecedor = reader.GetString("nm_empresa");
                dto.CNPJ = reader.GetString("cj_cnpj");
                dto.Rua = reader.GetString("rn_rua");
                dto.Numerofornecedor = reader.GetString("nr_numero");
                dto.Complemento = reader.GetString("ct_complemento");
                dto.Bairro = reader.GetString("br_bairro");
                dto.Cidade = reader.GetString("cd_cidade");
                dto.UF = reader.GetString("uf");
                dto.CEP = reader.GetString("cp_cep");
                dto.Telefone = reader.GetString("tl_telefone");
                dto.Email = reader.GetString("email");
                dto.PrecoUnitario = reader.GetDecimal("pr_unitario");
                dto.DataCadastro = reader.GetDateTime("dt_data_cadastro");
                dto.Observacao = reader.GetString("obs_observacao");
                dto.FKFuncionario = reader.GetInt32("fk_id_cadastro_funcionario_para_fo");

                lista.Add(dto);
            }
            reader.Close();
            return lista;
        }

        public List<DTO_Fornecedores> Listar()
        {
            string script = @"select * from tb_cadastro_fornecedor";

                List<MySqlParameter> parms = new List<MySqlParameter>();
                DataBase db = new DataBase();
                MySqlDataReader reader = db.ExecuteSelectScript(script, parms);
                List<DTO_Fornecedores> lista = new List<DTO_Fornecedores>();
                DTO_Fornecedores dto = null;

                while (reader.Read())
                {
                dto = new DTO_Fornecedores();
                dto.IdFornecedor = reader.GetInt32("id_cadastro_fornecedor");
                dto.NomeFornecedor = reader.GetString("nm_empresa");
                dto.CNPJ = reader.GetString("cj_cnpj");
                dto.Rua = reader.GetString("rn_rua");
                dto.Numerofornecedor = reader.GetString("nr_numero");
                dto.Complemento = reader.GetString("ct_complemento");
                dto.Bairro = reader.GetString("br_bairro");
                dto.Cidade = reader.GetString("cd_cidade");
                dto.UF = reader.GetString("uf");
                dto.CEP = reader.GetString("cp_cep");
                dto.Telefone = reader.GetString("tl_telefone");
                dto.Email = reader.GetString("email");
                dto.PrecoUnitario = reader.GetDecimal("pr_unitario");
                dto.DataCadastro = reader.GetDateTime("dt_data_cadastro");
                dto.Observacao = reader.GetString("obs_observacao");
                dto.FKFuncionario = reader.GetInt32("fk_id_cadastro_funcionario_para_fo");

                lista.Add(dto);
            }
            reader.Close();
            return lista;


        }


    }
}
