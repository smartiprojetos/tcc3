﻿using Lanchonha.Banco.Banco;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lanchonha.Banco.BD.Estoque
{
    class Database_Estoque
    {
        public int Salvar (DTO_Estoque dto)
        {
            string script = @"insert into tb_estoque (qnt_quantidade_entrada,dt_entrada,vl_valortotal,qnt_saida,dt_saida,vl_retornovalor,cd_condicao,fk_id_produto_para_es,fk_id_cadastro_funcionario_para_es)
                                      values (@qnt_quantidade_entrada,@dt_entrada,@vl_valortotal,@qnt_saida,@dt_saida,@vl_retornovalor,@cd_condicao,@fk_id_produto_para_es,@fk_id_cadastro_funcionario_para_es)";
            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("qnt_quantidade_entrada", dto.Quantidadeentrada));
            parms.Add(new MySqlParameter("dt_entrada", dto.DataEntrada));
            parms.Add(new MySqlParameter("vl_valortotal", dto.ValorTotal));
            parms.Add(new MySqlParameter("qnt_saida", dto.QuantidadeSaida));
            parms.Add(new MySqlParameter("dt_saida", dto.DataSaida));
            parms.Add(new MySqlParameter("vl_retornovalor", dto.RetornoValor));
            parms.Add(new MySqlParameter("cd_condicao", dto.Condicao));
            parms.Add(new MySqlParameter("fk_id_produto_para_es", dto.FKIdProduto));
            parms.Add(new MySqlParameter("fk_id_cadastro_funcionario_para_es", dto.IdFuncionarioParaEs));

            DataBase db = new DataBase();
            return db.ExecuteInsertScriptWithPk(script, parms);
            
        }

        public void ALterar (DTO_Estoque dto)
        {
            string script = @"update tb_estoque set qnt_quantidade_entrada=@qnt_quantidade_entrada,
                                                    dt_entrada=@dt_entrada,
                                                    vl_valortotal=@vl_valortotal,
                                                    qnt_saida=@qnt_saida,
                                                    dt_saida=@dt_saida,
                                                    vl_retornovalor=@vl_retornovalor,
                                                    cd_condicao=@cd_condicao,
                                                    fk_id_produto_para_es=@fk_id_produto_para_es,
                                                    fk_id_cadastro_funcionario_para_es=@fk_id_cadastro_funcionario_para_es
                                                    where id_estoque =@id_estoque";
            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_estoque", dto.IDestoque));
            parms.Add(new MySqlParameter("qnt_quantidade_entrada", dto.Quantidadeentrada));
            parms.Add(new MySqlParameter("dt_entrada", dto.DataEntrada));
            parms.Add(new MySqlParameter("vl_valortotal", dto.ValorTotal));
            parms.Add(new MySqlParameter("qnt_saida", dto.QuantidadeSaida));
            parms.Add(new MySqlParameter("dt_saida", dto.DataSaida));
            parms.Add(new MySqlParameter("vl_retornovalor", dto.RetornoValor));
            parms.Add(new MySqlParameter("cd_condicao", dto.Condicao));
            parms.Add(new MySqlParameter("fk_id_produto_para_es", dto.FKIdProduto));
            parms.Add(new MySqlParameter("fk_id_cadastro_funcionario_para_es", dto.IdFuncionarioParaEs));

            DataBase db = new DataBase();
            db.ExecuteInsertScript(script, parms);

        }

        public void Remover (int pk)
        {
            string script = @"delete from tb_estoque where id_estoque=@id_estoque";
            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_estoque", pk));

            DataBase db = new DataBase();
            db.ExecuteInsertScript(script, parms);
        }

        public List<DTO_Estoque> Consultarponome (string nome)
        {
            string script = @" select * from tb_estoque where nm_produto like @nm_produto";
            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_produto", nome + "%"));

            DataBase db = new DataBase();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);
            List<DTO_Estoque> lista = new List<DTO_Estoque>();
            while(reader.Read())
            {
                DTO_Estoque dto = new DTO_Estoque();
                dto.IDestoque = reader.GetInt32("id_estoque");
                dto.Quantidadeentrada = reader.GetInt32("qnt_quantidade_entrada");
                dto.DataEntrada = reader.GetDateTime("dt_entrada");
                dto.ValorTotal =reader.GetDecimal ("vl_valortotal");
                dto.QuantidadeSaida = reader.GetInt32("qnt_saida");
                dto.DataSaida = reader.GetDateTime("dt_saida");
                dto.Condicao = reader.GetString("cd_condicao");
                dto.RetornoValor = reader.GetDecimal("vl_retornovalor");
                dto.FKIdProduto = reader.GetInt32("fk_id_produto_para_es");
                dto.IdFuncionarioParaEs = reader.GetInt32("fk_id_cadastro_funcionario_para_es");

                lista.Add(dto);
            }
            reader.Close();
            return lista;
        }

        public List<DTO_Estoque> Listar()
        {
            string script = @" select * from tb_estoque";
            List<MySqlParameter> parms = new List<MySqlParameter>();

            DataBase db = new DataBase();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);
            List<DTO_Estoque> lista = new List<DTO_Estoque>();
            while (reader.Read())
            {
                DTO_Estoque dto = new DTO_Estoque();
                dto.IDestoque = reader.GetInt32("id_estoque");
                dto.Quantidadeentrada = reader.GetInt32("qnt_quantidade_entrada");
                dto.DataEntrada = reader.GetDateTime("dt_entrada");
                dto.ValorTotal = reader.GetDecimal("vl_valortotal");
                dto.QuantidadeSaida = reader.GetInt32("qnt_saida");
                dto.DataSaida = reader.GetDateTime("dt_saida");
                dto.RetornoValor = reader.GetDecimal("vl_retornovalor");
                dto.Condicao = reader.GetString("cd_condicao");
                dto.FKIdProduto = reader.GetInt32("fk_id_produto_para_es");
                dto.IdFuncionarioParaEs = reader.GetInt32("fk_id_cadastro_funcionario_para_es");

                lista.Add(dto);
            }
            reader.Close();
            return lista;
        }

        public List<DTO_Estoque> ConsultarporID(int ID)
        {
            string script = @" select * from tb_estoque where id_estoque like @id_estoque";
            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_estoque", ID + "%"));

            DataBase db = new DataBase();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);
            List<DTO_Estoque> lista = new List<DTO_Estoque>();
            while (reader.Read())
            {
                DTO_Estoque dto = new DTO_Estoque();
                dto.IDestoque = reader.GetInt32("id_estoque");
                dto.Quantidadeentrada = reader.GetInt32("qnt_quantidade_entrada");
                dto.DataEntrada = reader.GetDateTime("dt_entrada");
                dto.ValorTotal = reader.GetDecimal("vl_valortotal");
                dto.QuantidadeSaida = reader.GetInt32("qnt_saida");
                dto.DataSaida = reader.GetDateTime("dt_saida");
                dto.RetornoValor = reader.GetDecimal("vl_retornovalor");
                dto.Condicao = reader.GetString("cd_condicao");
                dto.FKIdProduto = reader.GetInt32("fk_id_produto_para_es");
                dto.IdFuncionarioParaEs = reader.GetInt32("fk_id_cadastro_funcionario_para_es");

                lista.Add(dto);
            }
            reader.Close();
            return lista;
        }

        public List<ViewConsultarEstoqueDTO> ConsultarEstoqueView(string produto)
        {
            string script = @" select * from consultar_estoque where nm_produto like @nm_produto";
            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_produto", produto + "%"));
            DataBase db = new DataBase();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);
            List<ViewConsultarEstoqueDTO> lista = new List<ViewConsultarEstoqueDTO>();
            while (reader.Read())
            {
                ViewConsultarEstoqueDTO dto = new ViewConsultarEstoqueDTO();
                dto.IDestoque = reader.GetInt32("id_estoque");
                dto.Quantidadeentrada = reader.GetInt32("qnt_quantidade_entrada");
                dto.DataEntrada = reader.GetDateTime("dt_entrada");
                dto.ValorTotal = reader.GetDecimal("vl_valortotal");
                dto.QuantidadeSaida = reader.GetInt32("qnt_saida");
                dto.DataSaida = reader.GetDateTime("dt_saida");
                dto.Condicao = reader.GetString("cd_condicao");
                dto.RetornoValor = reader.GetDecimal("vl_retornovalor");
                dto.NomeProduto = reader.GetString("nm_produto");
                dto.IDproduto = reader.GetInt32("id_cadastro_produtos");

                lista.Add(dto);
            }
            reader.Close();
            return lista;
        }

        public List<ViewConsultarEstoqueDTO> ListarEstoqueView()
        {
            string script = @" select * from consultar_estoque";
            List<MySqlParameter> parms = new List<MySqlParameter>();
            DataBase db = new DataBase();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);
            List<ViewConsultarEstoqueDTO> lista = new List<ViewConsultarEstoqueDTO>();
            while (reader.Read())
            {
                ViewConsultarEstoqueDTO dto = new ViewConsultarEstoqueDTO();
                dto.IDestoque = reader.GetInt32("id_estoque");
                dto.Quantidadeentrada = reader.GetInt32("qnt_quantidade_entrada");
                dto.DataEntrada = reader.GetDateTime("dt_entrada");
                dto.ValorTotal = reader.GetDecimal("vl_valortotal");
                dto.QuantidadeSaida = reader.GetInt32("qnt_saida");
                dto.DataSaida = reader.GetDateTime("dt_saida");
                dto.RetornoValor = reader.GetDecimal("vl_retornovalor");
                dto.NomeProduto = reader.GetString("nm_produto");
                dto.Condicao = reader.GetString("cd_condicao");
                dto.IDproduto = reader.GetInt32("id_cadastro_produtos");

                lista.Add(dto);
            }
            reader.Close();
            return lista;
        }
    }
}
