﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lanchonha.Banco.BD.Estoque
{
    class ViewConsultarEstoqueDTO
    {
        public int IDestoque { get; set; }
        public string NomeProduto { get; set; }
        public int Quantidadeentrada { get; set; }
        public DateTime DataEntrada { get; set; }
        public decimal ValorTotal { get; set; }
        public int QuantidadeSaida { get; set; }
        public DateTime DataSaida { get; set; }
        public string Condicao { get; set; }
        public decimal RetornoValor { get; set; }
        public int IDproduto { get; set; }

    }
}
