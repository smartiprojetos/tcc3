﻿using Lanchonha.Banco.BD.Fornecedor.PedidoFornecedor;
using Lanchonha.Banco.BD.Funcionarios;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lanchonha.Banco.BD.Estoque
{
    class Business_Estoque
    {
        public int Salvar(DTO_Estoque dto)
        {
            if (dto.FKIdProduto == 0 && dto.FKIdProduto == 0)
            {
                throw new ArgumentException("O campo produto está vazio por favor cadastre produtos para inserir no estoque.");
            }

            if (dto.Quantidadeentrada == null && dto.QuantidadeSaida == null)
            {
                throw new ArgumentException("O campo Quantidade De entrada ou a Quantidade de saida  estao em branco, por favor preencher.");
            }
            if (dto.Condicao ==string.Empty && dto.Condicao == string.Empty)
            {
                throw new ArgumentException("O campo Condição está em branco, por favor preencher.");
            }

            Database_Estoque db = new Database_Estoque();
            return db.Salvar(dto);
        }

        public void Remover(int id)
        {
            Database_Estoque db = new Database_Estoque();
            db.Remover(id);
        }

        public void Alterar(DTO_Estoque dto)
        {
            if (dto.Quantidadeentrada == null && dto.QuantidadeSaida == null)
            {
                throw new ArgumentException("O campo Quantidade De entrada ou a Quantidade de saida  estao em branco, por favor preencher.");
            }
            Database_Estoque db = new Database_Estoque();
            db.ALterar(dto);
        }

        public List<DTO_Estoque> Listar()
        {
            Database_Estoque db = new Database_Estoque();
            return db.Listar();
        }

        public List<DTO_Estoque>ConsultarporNome(string nome)
        {
            Database_Estoque db = new Database_Estoque();
            return db.Consultarponome(nome);
        }

        public List<DTO_Estoque> Consultarporid(int id)
        {
            Database_Estoque db = new Database_Estoque();
            return db.ConsultarporID(id);
        }

        public List<ViewConsultarEstoqueDTO> ConsultarEstoqueView(string produto)
        {
            Database_Estoque db = new Database_Estoque();
            return db.ConsultarEstoqueView(produto);
        }

        public List<ViewConsultarEstoqueDTO> ListarEstoqueView()
        {
            Database_Estoque db = new Database_Estoque();
            return db.ListarEstoqueView();
        }

        public int SalvarporLista(List<PedidoparaFornecedorGridDTO> Lista)
        {
            int pk =UserSession.UsuarioLogado.Id_funcionario;
            int idestoque=0;
            DTO_Estoque estoque = new DTO_Estoque();
            Database_Estoque db = new Database_Estoque();
            foreach (PedidoparaFornecedorGridDTO item in Lista)
            {
                estoque.Quantidadeentrada =item.Quantidade ;
                estoque.DataEntrada = item.DataCompra;
                estoque.ValorTotal = item.PrecoTotal;
                estoque.QuantidadeSaida = 0;
                estoque.RetornoValor = 0;
                estoque.Condicao = "Em espera";
                estoque.FKIdProduto = item.IDProduto;
                estoque.IdFuncionarioParaEs =pk ;
                idestoque=db.Salvar(estoque);
            }
            return idestoque;


        }
    }
}
