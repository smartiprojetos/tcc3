﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lanchonha.Banco.BD.AJUDA_
{
    class DTO_Pedido_Item
    {
        public int ID { get; set; }
        public int Quantidade { get; set; }
        public string Observacoes { get; set; }
        public int IDPedido { get; set; }
        public int IDCardapio { get; set; }

    }
}
